<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));

$post->idprofile = $data->idprofile;
$post->idimage = $data->idimage;
$post->imagename = $data->imagename;
$post->imagelocation = $data->imagelocation;
if($data->imageurl)
{
    $post->imageurl = $data->imageurl;
}
$newresult = $post->updateimages();
$no = $newresult->rowCount();
if($no>0)
{
        $error_arr = array();
        $error_arr['data']= array();
        $post_item = array("message"=>"Image Updated Successfully");
        array_push($error_arr['data'],$post_item);
        echo json_encode($error_arr);
        return true;
    
}

else
{
        $error_arr = array();
        $error_arr['data']= array();
        $error_item = array("message"=>"Failed To Update Image");
        array_push($error_arr['data'],$error_item);
        echo json_encode($error_arr);
        return true;
   
}

//selected services


?>