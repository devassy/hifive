<?php
session_start();
if(isset($_SESSION["username"]))
{
$username = $_SESSION["username"];
}
else
{
    header("location:userlogin.php");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
         <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
       
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    
                </link>
            </link>
        </link>
    </head>
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            
            <div class="logo">
                   <a class="simple-text logo-normal" href="welcomeuser.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
                <?php include 'usermenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title">
                                            Change Password
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <form method= "post" action ="changecurlloguserpassword.php">

                                            <div class ="row">

                                                    <div class="form-group">
                                                        <div class="col-md-12 col-md-offset-3">
                                                             <div id="messages">
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            current password
                                                        </label>
                                                        <input class="form-control" type="password" name="currentpassword" id="currentpassword">
                                                        </input>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            new password
                                                        </label>
                                                        <input class="form-control" type="password" name="newpassword" id="newpassword">
                                                        </input>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            retype newpassword
                                                        </label>
                                                        <input class="form-control" type="password" name="retypenewpassword" id="retypenewpassword">
                                                        </input>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                       
                                                        <input class="form-control" type="hidden" name="username"  value = "<?php echo $username;?>" id="username">
                                                        </input>
                                                    </div>
                                                </div>
                                            </div>
                                          
                                            
                                            <button class="btn btn-primary pull-right" type="submit" id="submit" name="submit">
                                            Change Password
                                            </button>
                                           
                                            <div class="clearfix">
                                            </div>
                                        </form>
                                        <?php
                                        if(isset($_GET["message"]))
                                        {
                                          $newresult = $_GET["message"];
                                          
                                          if($newresult == "Password Changed Successfully")
                                          {
                                          ?>
                                            <div style="color:green;" id = "testdiv"><?php echo $newresult;?></div>
                                            <script type="text/javascript">
                                               $(function()
                                               {
                                                       $("#testdiv").delay(7000).fadeOut();
                                               });
                                           </script>
                                        <?php
                                          }
                                          else if($newresult == "invalid username or password")
                                          {
                                            ?>
                                            <div style="color:red;" id = "testdiv"><?php echo $newresult;?></div>
                                            <script type="text/javascript">
                                               $(function()
                                               {
                                                       $("#testdiv").delay(7000).fadeOut();
                                               });
                                           </script>
                                        <?php
                                           
                                          }
                                          else
                                          {
                                             
                                          }

                                        }
                                        ?>
                                        <div id = "username_result"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>
                    
                   <?php include 'footer.php';?>

                </div>
            </div>
            <!--   Core JS Files   -->
<script src="assets/js/core/jquery.min.js" type="text/javascript">
</script>
<script src="assets/js/core/popper.min.js" type="text/javascript">
</script>
<script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
</script>
<script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
</script>
<!--Add user form validation-->
<script src="assets/js/adduser.js">
</script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
</script>
<!-- Chartist JS -->
<script src="assets/js/plugins/chartist.min.js">
</script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js">
</script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
</script>



        </div>
       
    </body>
    <!--  -->
</html>     

