<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="colorlib.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="s01">
      <div class="nav">
        <img class="logo" src="./hifiveimages/logo1.png" alt="hifivelogo">
        <div class="login">
          <?php include 'header.php';?>
        </div>
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog modal-login">
            <div class="modal-content">
              <form action="#" method="post">
                <div class="modal-header">        
                  <h4 class="modal-title">Login</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">        
                  <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control" required="required">
                  </div>
                  <div class="form-group">
                    <div class="clearfix">
                      <label>Password</label>
                      <a href="#" class="pull-right text-muted"><small>Forgot?</small></a>
                    </div>
                    
                    <input type="password" class="form-control" required="required">
                  </div>
                </div>
                <div class="modal-footer">
                  <label class="checkbox-inline pull-left"><input type="checkbox"> Remember me</label>
                  <input type="submit" class="btn pull-right" value="Login">
                </div>
              </form>
            </div>
          </div>
        </div>     







      </div>
    <?php include 'usermenu.php';?>
      
      <form>
        <fieldset>
          <legend>The Complete Service Marketplace</legend>
        </fieldset>
        <div class="inner-form">
          <div class="input-field first-wrap">
            <input id="search" type="text" placeholder="What service are you looking for?" />
          </div>
          <div class="input-field second-wrap">
            <input id="location" type="text" placeholder="location" />
          </div>
          <div class="input-field third-wrap">
            <button class="btn-search" type="button">Search</button>
          </div>
        </div>
      </form>
    </div>
  
  </body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
