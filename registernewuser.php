<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>  
         <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

       
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    
                </link>
            </link>
        </link>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    </head>
    <body class="">
       
            <div class="main-panel">
                <!-- Navbar -->
               
                
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title">
                                            Register New User
                                        </h4>
                                        <p class="card-category">
                                           
                                        </p>
                                    </div>
                                    <div class="card-body">
                                        <form id="add_user" method= "post" action ="curluserinsert.php">

                                            <div class ="row">

                                                    <div class="form-group">
                                                        <div class="col-md-12 col-md-offset-3">
                                                             <div id="messages">
                                                             <div id = "username_result"></div>
                                                             <div id = "user_email"></div>
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Username
                                                        </label>
                                                        <input class="form-control" type="text" name="username" id="username">
                                                        </input>
                                                    </div>
                                                </div>
                                               
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Password
                                                        </label>
                                                        <input class="form-control" id="password" type="password" name="password">
                                                        </input>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                           Confirm Password
                                                        </label>
                                                        <input class="form-control" type="password" name="confirmpassword" id="confirmpassword">
                                                        </input>
                                                    </div>
                                                </div>
                                               
                                                
                                            </div>
                                          
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Email
                                                        </label>
                                                        <input class="form-control" id="useremail" type="email" name="useremail">
                                                        </input>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Phone
                                                        </label>
                                                        <input class="form-control" type="number" id="phone" name="phone">
                                                        </input>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                          
                                           
                                           
                                           
                                            
                                           
                                              
                                               
                                           
                                           
                                            
                                            <button class="btn btn-primary pull-right" type="submit" id="submit" name="submit">
                                                Add
                                            </button>
                                           
                                            <div class="clearfix">
                                            </div>
                                        </form>
                                        <?php
                                        if(isset($_GET["message"]))
                                        {
                                           $resultmessage = $_GET["message"];
                                           if($resultmessage=="Passwords Mismatch")
                                           {
                                           echo "<span style=color:red>$resultmessage</span>";
                                           }
                                           else if($resultmessage=="Failed to insert")
                                           {
                                           echo "<span style=color:red>$resultmessage</span>";
                                           }
                                           else
                                           {
                                             ?>
                                             <div style="color:green;" id = "testdiv"><?php echo $resultmessage;?></div>
                                             <script type="text/javascript">
                                                $(function()
                                                {
                                                        $("#testdiv").delay(7000).fadeOut();
                                                });
                                            </script>
                                             <?php
                                            
                                             unset($result);
                                            
                                           }
                                           

                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>
                    
                   

                </div>
            </div>
            <!--   Core JS Files   -->
<script src="assets/js/core/jquery.min.js" type="text/javascript">
</script>
<script src="assets/js/core/popper.min.js" type="text/javascript">
</script>
<script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
</script>
<script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
</script>
<script src="assets/js/newuser.js">
</script>
<!--Add user form validation-->


<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
</script>
<!-- Chartist JS -->
<script src="assets/js/plugins/chartist.min.js">
</script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js">
</script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
</script>
</div>
<script type="text/javascript">
 $(document).ready(function(){
  $('#username').blur(function(){
    var username = $('#username').val();
    var jsonData = {username:username};
    $.ajax({
     type: "POST",
     url: "checkusername.php",
     data: JSON.stringify(jsonData),
     dataType: "json",
     success: function(data)
     {
        $('#username').val("");
        $('#username_result').html('This username has already taken');
        $('#username_result').css('color','red');
        $('#username_result').fadeIn().delay(10000).fadeOut();
     }
      
    }); //ajax ends
   
  });//user name change function ends
 }); //document ready function ends
</script> 
<script type="text/javascript">
 $(document).ready(function(){
  $('#useremail').keyup(function(){
    var email = $('#useremail').val();
    var jsonData = {email:email};
    $.ajax({
     type: "POST",
     url: "checkemail.php",
     data: JSON.stringify(jsonData),
     dataType: "json",
     success: function(data)
     {
        
        $('#useremail').val("");
        $('#user_email').html('This email has already registered');
        $('#user_email').css('color','red');
        $('#user_email').fadeIn().delay(10000).fadeOut();
        
     }
      
    }); //ajax ends
  });//user email change function ends
 }); //document ready function ends
</script> 

    </body>
    
</html>     

