<?php
session_start();
include 'urlrewrite.php';

$type = $_SESSION["type"];
$userid = $_SESSION["idprofiles"];
if(($type == 'U')&&($_SESSION["idprofiles"]))
{
$data = array();
//Option 1: Convert data array to json if you want to send data as json


//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/readuserrequest.php?id=$userid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$newdata = curl_exec($ch);
$result = json_decode($newdata);

//close curl connection
curl_close($ch);
//print result
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
           
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
                
                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <link href="assets/css/mobileview.css" rel="stylesheet">
            </link>
        </link>
    </head>
   
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
               
            <div class="logo">
                   <a class="simple-text logo-normal" href="welcomeuser.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
                <?php include 'usermenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title ">
                                            Service Requests
                                        </h4>
                                        
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class=" text-primary">
                                                    <th>Sl No:</th>
                                                    <th>Message</th>
                                                    <th>Service Name</th>
                                                    <th>Service Location</th>
                                                    <th>Servicedate</th>
                                                    <th>Servicetime</th>  
                                                    <th>Service Requested on </th>
                                                    <th>Service Status</th>
                                                    <th>Action</th>
                                                </thead>
                                                <tbody>
												<?php
                                               
												if(!empty($result))
												{
                                                    $i=1;
												foreach($result as $value)
												{
													foreach($value as $data)
													{
                                                    $servicetime = rtrim("$data->servicetime",0);
												?>
                                                   <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $data->usermessage;?></td>
                                                   <td><?php echo $data->servicename;?></td>
                                                   <td><?php echo $data->service_location;?></td>
                                                   <td><?php $requesteddate = date("d-M-Y",strtotime($data->servicedate));echo $requesteddate;?></td>
                                                   <td><?php echo $servicetime;?></td>
                                                   <td><?php $createddate = date("d-M-Y",strtotime($data->createdon));echo $createddate;?></td>
                                                   <td>
                                                   <?php 
                                                   if($data->service_status=="N")
                                                   {
                                                     echo "Service Not Started";
                                                   }
                                                   else if($data->service_status=="S")
                                                   {
                                                     echo "Service Has Started";
                                                   }
                                                   else if($data->service_status=="C")
                                                   {
                                                     echo "Service Has Completed";
                                                   }
                                                   else if($data->service_status=="P")
                                                   {
                                                     echo "Service in Progress";
                                                   }
                                                   else if($data->service_status=="Ca")
                                                   {
                                                     echo "Service has Cancelled";
                                                   }
                                                   else
                                                   {

                                                   }
                                                   ?>
                                                   </td>
                                                   <td>
                                                          
                                                          
                                                          <a href="editcurlservicerequestbyuser.php?id=<?php echo $data->idservice_request;?>">
                                                              <button class="btn btn-sm btn-success" rel="tooltip" type="button" title="edit request">
                                                                  <i class="material-icons">
                                                                      edit
                                                                  </i>
                                                              </button>
                                                          </a>
                                                          <!--<a href="curldelete.php?idprofiles=<?php echo $data->idservice_request;?>">
                                                              <button class="btn btn-sm btn-danger" rel="tooltip" type="button" title="delete request">
                                                                  <i class="material-icons">
                                                                      close
                                                                  </i>
                                                              </button>
                                                          </a>-->
                                                          <a href="addusermessage.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-success" rel="tooltip" type="button" title="add message">
                                                                    <i class="material-icons">
                                                                    +message
                                                                    </i>
                                                                </button>
                                                            </a>
                                                            <a href="viewservicemessage.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-success" rel="tooltip" type="button" title="view message">
                                                                    <i class="material-icons">
                                                                    message
                                                                    </i>
                                                                </button>
                                                            </a>
                                                      </td>                                                    </tr>
                                                </tbody>
												<?php
                                                $i++;
                                                    }
                                                    
                                                }
                                                
												}
												else
												{
													echo "No records Found";
                                                }
												?>
                                            </table>
                                            <?php
                                            if(isset($_GET["result"]))
                                            {
                                                $success = $_GET["result"];
                                                echo "<span style=color:green;>$success</span>";
                                            }
                                            // if(empty($success))
                                            // {
                                            //     echo "<span style=color:red;></span>";
                                            // }
                                           ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include 'footer.php';?>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
                
            </div>
        </div>
    </body>
</html>
<?php
}
else
{
    header("location:userlogin.php");

}
