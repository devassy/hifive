<?php
session_start();
include 'urlrewrite.php';


$userid = $_POST["userid"];
$catid = $_POST["catid"];
$usermessage = $_POST["usermessage"];
$service = $_POST["servicename"];
$service_location = $_POST["service_location"];
$servicedistrict = $_POST["servicedistrict"];
$servicedate = $_POST["servicedate"];
$servicetime = $_POST["servicetime"];
$payment_type = $_POST["payment_type"];
$servicerequestdata = array("userid"=>"$userid","categoryid"=>"$catid","idservices"=>$service, "usermessage" =>"$usermessage", "service_location" =>"$service_location","payment_type"=>"$payment_type","servicedate"=>"$servicedate","servicetime"=>$servicetime,"createdby"=>$userid,"servicedistrict"=>$servicedistrict,"service_location"=>"$service_location");

//Option 1: Convert data array to json if you want to send data as json
$data = json_encode($servicerequestdata);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "$url/addservicerequest.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$newresult = curl_exec($ch);
$result = json_decode($newresult);
$resultmessage = $result->message;
//close curl connection
curl_close($ch);
//print result
// print_r($result);
if(isset($result))
{
   
    if($_POST["singleworkerid"]!='')
    {
        
        $singleworkerid = $_POST["singleworkerid"];
        $_SESSION["message"] = $result->message;
        $_SESSION["singleworkerid"] = $singleworkerid;
        $_SESSION["userid"] = $userid;
        $_SESSION["usermessage"] = $usermessage;
        $_SESSION["catid"] = $catid;
        $_SESSION["servicedistrict"] = $servicedistrict;
        $_SESSION["servicetime"] = $servicetime;
        $_SESSION["service_location"] = $service_location;
        $_SESSION["payment_type"]= $payment_type;
        $_SESSION["servicedate"] = $servicedate;
        $_SESSION["servicename"] = $servicename;
        unset($singleworkerid);
        header("location:servicerequestbyuser.php");

    }
    else
    {
        if($resultmessage=="Please select a future date")
        {
            $_SESSION["usermessage"] = $usermessage;
            $_SESSION["catid"] = $catid;
            $_SESSION["servicedistrict"] = $servicedistrict;
            $_SESSION["servicetime"] = $servicetime;
            $_SESSION["service_location"] = $service_location;
            $_SESSION["payment_type"]= $payment_type;
            $_SESSION["servicedate"] = $servicedate;
            $_SESSION["servicename"] = $servicename;
            
            
           
        }
        $_SESSION["userid"] = $userid;
        $_SESSION["message"] = $result->message;
        header("location:servicerequestbyuser.php");

    }
    
}
else
{
    
    header("location:servicerequestbyuser.php?id=$userid");

}
?>