<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));


$post->servicename = $data->servicename;
$post->location = $data->location;


// Create post

$result = $post->searchworkers($post->location,$post->servicename);

        $post_arr = array();
        $post_arr['data']= array();
        while($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $post_item = array("idprofiles"=>$row["idprofiles"],"firstname"=>$row["firstname"],"location"=>$row["location"],"imagename"=>$row["imagename"],"type"=>$row["type"],"servicename"=>$row["servicename"],"gender"=>$row["gender"],"location"=>$row["location"],"city"=>$row["city"],"categoryname"=>$row["categoryname"],'message'=>"success",'district'=>$row["district"]);
            array_push($post_arr['data'],$post_item);
        }
        echo json_encode($post_arr);
        return true;
        
        
?>