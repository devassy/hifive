<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);

$post->idprofile = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');

//Blog post query
$result = $post->getimages($post->idprofile);
//Get row count

$num = $result->rowCount();

//Check if any posts
if($num > 0 ){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
            $post_item = array(
            'idimage'=>$row["idimage"],
            'idprofile' =>$row["idprofile"],
            'imagename'=>$row["imagename"],
            'imagelocation'=>$row["imagelocation"],
            'imageurl'=>$row["imageurl"]
        );
        array_push($post_arr['data'], $post_item);
    }
   
    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
    $error_arr = array();
    $error_arr['data']= array();
    $post_item = array("message"=>"No Images Found");
    array_push($error_arr['data'],$post_item);
    echo json_encode($error_arr);
    return true;
}



?>