<?php
session_start();
include 'urlrewrite.php';
$type = $_SESSION["type"];
if(isset($_SESSION["type"]))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Hifive
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>

<body class="">
<?php
$id = $_GET["id"];
$data = array("id" =>'$id');
//Option 1: Convert data array to json if you want to send data as json
//$data = json_encode($data);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/read_single.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$newdata = json_decode($result);

foreach($newdata as $data)
{
    foreach($data as $editdata)
    {
        $imagename = $editdata->imagename;
        $gender = $editdata->gender;
        $username = $editdata->username;
        $firstname = $editdata->firstname;
        $lastname = $editdata->lastname;
        $email = $editdata->email;
        $age = $editdata->age;
        $phone = $editdata->phone;
        $address1 = $editdata->address1;
        $address2 = $editdata->address2;
        $location = $editdata->location;
        $sublocality = $editdata->sublocality;
        $district = $editdata->district;
        $state = $editdata->state;
        $landmark = $editdata->landmark;
        $city = $editdata->city;
        $idimage = $editdata->idimage;
        $imagelocation = $editdata->imagelocation;
        $imageurl = $editdata->imageurl;
        $idprofiles = $editdata->idprofiles;

    }
}

//$newdata = json_decode($result);
//close curl connection
curl_close($ch);

?>

<script>

//document.write(x);
</script>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
     
    <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
      <?php
      $type = $_SESSION["type"];
      if($type=="U")
      {
      include 'usermenu.php';
      }
      else
      {
        include 'workermenu.php';
      }
      ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
     <?php include 'header.php';?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profile</h4>
                  <p class="card-category">Complete your profile</p>
                </div>       
                <div class="card-body">
                  <form method = "post" action = "curledit.php" enctype="multipart/form-data">
                  <?php
                    if(isset($imagename))
                    {
                        $chk="true";
                    ?>
                    <div class="row">
                    <div class="col-md-6">
                           <?php
                           if($imagelocation=="L")
                           {
                            ?>
                            <img src = "images/<?php echo $imagename;?>" width = "100" height="70" name = "chk">
                            <?php
                           }
                           else if($imagelocation=="S")
                           {
                               
                           ?>
                           <img src = "<?php echo $imageurl;?>" width = "100" height="70" name = "chk">
                           <?php
                           }
                           else
                           {

                           }
                           ?>
                             
                    </div>
                    <div class="col-md-6">
                           <div class="file-field">
                              <div class="btn btn-primary btn-md">
                                   <input type="file" name="userfile">
                              </div>
                            </div>
                        </div>
                    </div>
                   <?php
                    }
                    else
                    {
                        $chk = "false";
                       
                        if($gender=='F')
                        {
                        
                        ?>
                        <div class="row">
                    <div class="col-md-6">
                           <div class="file-field">
                           <img src="images/avathar1.jpg" alt="Avatar" class="avatar">
                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="file-field">
                              <div class="btn btn-primary btn-md">
                                   <input type="file" name="userfile">
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    
                        
                    </div>



                        <?php    
                        }
                        else
                        {
                           
                    ?>
                    <div class="row">
                    <div class="col-md-6">
                           <div class="file-field">
                           <img src="images/avatar.png" alt="Avatar" class="avatar">
                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="file-field">
                              <div class="btn btn-primary btn-md">
                                   <input type="file" name="userfile">
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    
                        
                    </div>
                    <?php
                        }//else ends
                    }//main else ends
                    ?>
                      <div class="row">
                          <div class="col-md-5">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Username
                                  </label>
                                  <input class="form-control" type="text" name ="username" value = "<?php echo $username;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Firstname
                                  </label>
                                  <input class="form-control" type="text" name="firstname" value = "<?php echo $firstname;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Lastname
                                  </label>
                                  <input class="form-control" type="text" name="lastname" value = "<?php echo $lastname;?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Email
                                  </label>
                                  <input class="form-control" type="email" name ="email" value = "<?php echo $email;?>">
                                  </input>
                              </div>
                          </div>
                          
                          
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Age
                                  </label>
                                  <input class="form-control" type="text" name="age" value = "<?php echo $age;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                         
                          <div class="form-check form-check-radio form-check-inline">
                        
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioOptions" value = "M" <?php if($gender=='M') echo 'checked="checked"';?>> Male
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                       
                                          
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioOptions" value = "F" <?php if($gender=='F') echo 'checked="checked"';?>>Female
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Phone
                                  </label>
                                  <input class="form-control" type="text" name="phone" value = "<?php echo $phone;?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Address1
                                  </label>
                                  <input class="form-control" type="text" name ="address1" value="<?php echo $address1;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Address2
                                  </label>
                                  <input class="form-control" type="text" name = "address2" value="<?php echo $address2;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      location
                                  </label>
                                  <input class="form-control" type="text" name="location" value = "<?php echo $location;?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          sub locality
                                      </label>
                                      <input class="form-control" type="text" name="sublocality" value = "<?php echo $sublocality;?>">
                                      </input>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          Landmark
                                      </label>
                                      <input class="form-control" type="text" name="landmark" value = "<?php echo $landmark;?>" >
                                      </input>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          city
                                      </label>
                                      <input class="form-control" type="text" name="city" value = "<?php echo $city;?>">
                                      </input>
                                  </div>
                              </div>
                          </div>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      District
                                  </label>
                                  <select name="district" id="payment_type" class="browser-default custom-select">
                                                      <option   <?php if ($district == "Thiruvanathapuram" ) echo 'selected';?> value="Thiruvanathapuram">Thiruvanathapuram</option>
                                                      <option   <?php if ($district == "Kollam" ) echo 'selected' ; ?> value="Kollam">Kollam</option>
                                                      <option   <?php if ($district == "Alappuzha" ) echo 'selected' ; ?> value="Alappuzha">Alappuzha</option>
                                                      <option   <?php if ($district == "Pathanamthitta" ) echo 'selected' ; ?> value="Pathanamthitta">Pathanamthitta</option>
                                                      <option   <?php if ($district == "Kottayam" ) echo 'selected' ; ?> value="Kottayam">Kottayam</option>
                                                      <option   <?php if ($district == "Idukki" ) echo 'selected' ; ?> value="Idukki">Idukki</option>
                                                      <option   <?php if ($district == "Ernakulam" ) echo 'selected' ; ?> value="Ernakulam">Ernakulam</option>
                                                      <option   <?php if ($district == "Thrissur" ) echo 'selected' ; ?> value="Thrissur">Thrissur</option>
                                                      <option   <?php if ($district == "Malappuram" ) echo 'selected' ; ?> value="Malappuram">Malappuram</option>
                                                      <option   <?php if ($district == "Kozhikode" ) echo 'selected' ; ?> value="Kozhikode">Kozhikode</option>
                                                      <option   <?php if ($district == "Palakkad" ) echo 'selected' ; ?> value="Palakkad">Palakkad</option>
                                                      <option   <?php if ($district == "Kannur" ) echo 'selected' ; ?> value="Kannur">Kannur</option>
                                                      <option   <?php if ($district == "Wayanad" ) echo 'selected' ; ?> value="Wayanad">Wayanad</option>
                                                      <option   <?php if ($district == "Kasaragod" ) echo 'selected' ; ?> value="Kasaragod">Kasaragod</option>
                                                      </select>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     State
                                  </label>
                                  <input class="form-control" type="text" name="state" value = "<?php echo $state;?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          Latitude
                                      </label>
                                      <input class="form-control" type="text">
                                      </input>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                         Longitude
                                      </label>
                                      <input class="form-control" type="text">
                                      </input>
                                  </div>
                              </div>
                          </div>
                    <input type = "hidden" name = "chk" value = "<?php echo $chk;?>">
                    <input type = "hidden" name = "imagelocation" value = "<?php if(isset($imagelocation)){echo $imagelocation;}?>">
                    <input type = "hidden" name = "oldimage" value = "<?php if(isset($imagename)){echo $imagename;}?>">
                    <input type = "hidden" name = "idimage" value = "<?php if(isset($idimage)){echo $idimage;}?>">
                    <input type="hidden" name ="idprofiles" value= "<?php echo $idprofiles;?>">
                    <button type="submit" class="btn btn-primary pull-right"  name="submit">Update Profile</button>
                    <div class="clearfix"></div>
                  </form>
                  <?php
                  if(isset($_SESSION["message"]))
                  {
                    $resultmessage = $_SESSION["message"];
                     
                if($_SESSION["message"]=="Successfully Updated")
                {
                $resultmessage = $_SESSION["message"];
                ?>
                    <div style="color:green;" id = "testdiv"><?php echo $resultmessage;?></div>
                    <script type="text/javascript">
                       $(function()
                       {
                               $("#testdiv").delay(7000).fadeOut();
                       });
                   </script>
              <?php
                unset($_SESSION["message"]);
                unset($message);
                }
                else if($resultmessage=="Post Not Updated")
                {
                    $resultmessage = $_SESSION["message"];
                    ?>
                        <div style="color:green;" id = "testdiv"><?php echo $resultmessage;?></div>
                        <script type="text/javascript">
                           $(function()
                           {
                                   $("#testdiv").delay(7000).fadeOut();
                           });
                       </script>
                  <?php
                    unset($_SESSION["message"]);
                    unset($message);

                }
                else
                {

                }
            
            }
        
            ?>
                </div>
              </div>
            </div>
           
            </div> 
          </div>

            <footer class="footer">
        <div class="container-fluid">
         
          </nav> 
          <div class="copyright">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script><!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
          </div>
        </div>
      </footer>


        </div>
      </div>
              <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
              <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
              <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
              <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
              <!--  Google Maps Plugin    -->
              <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
              <!-- Chartist JS -->
              <script src="assets/js/plugins/chartist.min.js"></script>
              <!--  Notifications Plugin    -->
              <script src="assets/js/plugins/bootstrap-notify.js"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
              
                </div>
              </div>
  <!--   Core JS Files   -->
  
 
</body>
<?php
}
else
{
    header("location:userlogin.php");
}
?>
</html>