<?php
include 'urlrewrite.php';
if(isset($_POST["getworker"]))
{
$category = $_POST["search"];
$location = $_POST["location"];
$search = array("servicename"=>
$category,"location"=>$location);

$ch = curl_init();
$data = json_encode($search);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getworkerslist.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$searchresult = curl_exec($ch);
$newresult = stripslashes($searchresult);
$workerresult = json_decode($newresult);

//close curl connection
curl_close($ch);
//print result
// print_r($result);

}
?>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="colorlib.com" name="author">
            <meta content="width=device-width, initial-scale=1.0" name="viewport">
                <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet"/>
                <link href="css/main.css" rel="stylesheet"/>
                <!-- <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet"> -->
                <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
                    </script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
                    </script>
                </link>
            </meta>
        </meta>
    </head>
    <body>
        <div class="s01">
            <div class="nav">
                <img alt="hifivelogo" class="logo" src="./hifiveimages/logo1.png">
                    <div class="login">
                        <a href="userlogin.php">
                            Login
                        </a>
                        <a href="registernewuser.php">
                            Signup
                        </a>
                    </div>
                    <!-- Modal HTML -->
                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog modal-login">
                            <div class="modal-content">
                                <form action="#" method="post">
                                    <div class="modal-header">
                                        <h4 class="modal-title">
                                            Login
                                        </h4>
                                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                                            ×
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>
                                                Email
                                            </label>
                                            <input class="form-control" required="required" type="email">
                                            </input>
                                        </div>
                                        <div class="form-group">
                                            <div class="clearfix">
                                                <label>
                                                    Password
                                                </label>
                                                <a class="pull-right text-muted" href="#">
                                                    <small>
                                                        Forgot?
                                                    </small>
                                                </a>
                                            </div>
                                            <input class="form-control" required="required" type="password">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <label class="checkbox-inline pull-left">
                                            <input type="checkbox">
                                                Remember me
                                            </input>
                                        </label>
                                        <input class="btn pull-right" type="submit" value="Login">
                                        </input>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </img>
            </div>
            
                <form action="<?=$_SERVER['PHP_SELF'];?>" method="post" name="f1">
                    <fieldset>
                        <legend>
                            The Complete Service Marketplace
                        </legend>
                    </fieldset>
                    <div class="inner-form">
                        <div class="input-field first-wrap">
                            <input id="search" name="search" placeholder="What service are you looking for?" type="text"/>
                        </div>
                        <div class="input-field second-wrap">
                            <input id="location" name="location" placeholder="location" type="text"/>
                        </div>
                        <div class="input-field third-wrap">
                            <button class="btn-search" name="getworker" type="submit">
                                Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="details">
                <div class="col" >
                    <img class="serach_res" height="150px" src="images/worker1.jpg" width="150px">
                    </img>
                </div>
                <div class="col">
                    <table class="search_details">
                        <tr>
                            <td>
                                Name
                            </td>
                            <td>
                                joey
                            </td>
                        </tr>
                        <tr>
                            <td>
                                location
                            </td>
                            <td>
                                abc
                            </td>
                        </tr>
                        <tr>
                            <td>
                                service
                            </td>
                            <td>
                                IT
                            </td>
                        </tr>
                        <tr>
                            <td>
                                category
                            </td>
                            <td>
                                DM
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                joey@gmail.com
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Registered on :
                            </td>
                            <td>
                                12/12/2018
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input onclick="window.location.href='#'" type="button" value="Profile"/>
                            </td>
                            <td>
                                <input onclick="window.location.href='#'" type="button" value="Remove"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
       
    </body>
    <!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
