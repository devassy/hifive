<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);


// Get ID
$data = json_decode(file_get_contents("php://input"));

// $post->senderid = $data->senderid;
// $post->recieverid = $data->recieverid;
$post->servicrequestid = $data->servicrequest;

//Get post


$result = $post->getworkerservicemessage();
//Get row count

$num = $result->rowCount();

//Check if any posts
if($num > 0 ){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        
        $post_item = array(
            'idmessage'=>$row["idmessage"],
            'senderid' =>$row["senderid"],
            'servicrequestid' =>$row["servicrequestid"],
            'recieverid'=>$row["recieverid"],
            'message'=>$row["usermessage"],
            'subject'=>$row["subject"],
            'servicename'=>$row["servicename"],
            'usermessage'=>$row["usermessage"],
            'message'=>$row["message"],
            'firstname'=>$row["firstname"],
            'lastname'=>$row["lastname"],
            'type'=>$row["type"],
            'created'=>$row["created"],
            'resultedmessage'=>'success'
        );
        array_push($post_arr['data'],$post_item);
    }
    
   
    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    $error_arr = array();
    $error_arr['data']= array();
    $error_item = array("resultedmessage"=>"No records");
    array_push($error_arr['data'],$error_item);
    echo json_encode($error_arr);
    return true;
}

?>
