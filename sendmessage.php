<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);


// Get ID
$post->servicerequestid = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');


//Get post

$result = $post->getmessages();
//Get row count

$num = $result->rowCount();

//Check if any posts
if($num > 0 ){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $senderid = $row["senderid"];
        $idprofiles = $row["idprofiles"];
        $receiverid = $row["recieverid"];
        if($senderid=="$idprofiles")
        {
                $fname = $row["firstname"];
                $lname = $row["lastname"];
                $space = "  ";
                $from = $fname.$space.$lname;
                $post_item = array(
                'resultedmessage'=>"success",
                'idmessage'=>$row["idmessage"],
                'senderid'=>$row["senderid"],
                'recieverid'=>$row["recieverid"],
                'message'=>$row["message"],
                'subject'=>$row["subject"],
                'servicename'=>$row["servicename"],
                'usermessage'=>$row["usermessage"],
                'created'=>$row["created"],
                "From"=>"$from",
                "To"=>"Admin",
                'firstname'=>$row["firstname"],
                'lastname'=>$row["lastname"],
                "type"=>$row["type"],
                "servicerequestid"=>$row["servicrequestid"],
                "idprofiles"=>$row["idprofiles"]
            );
            array_push($post_arr['data'],$post_item);
        }
        else if($senderid=="$receiverid")
        {
            $fname = $row["firstname"];
            $lname = $row["lastname"];
            $space = " ";
            $to = $fname.$space.$lname;
            $post_item = array(
            'resultedmessage'=>"success",
            'idmessage'=>$row["idmessage"],
            'senderid'=>$row["senderid"],
            'recieverid'=>$row["recieverid"],
            'message'=>$row["message"],
            'subject'=>$row["subject"],
            'servicename'=>$row["servicename"],
            'usermessage'=>$row["usermessage"],
            'created'=>$row["created"],
            'From'=>'Admin',
            'To'=>'$to',
            "type"=>$row["type"],
            "servicerequestid"=>$row["servicrequestid"],
            "idprofiles"=>$row["idprofiles"]
        );
        array_push($post_arr['data'],$post_item);
       }
       else
       {
            $post_item = array("message"=>"No records found");
            array_push($post_arr['data'],$post_item);
       }
    }
   
    
   
    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
    echo json_encode(
        array('resultedmessage' =>'No records')
    );
    return false;
}

?>
