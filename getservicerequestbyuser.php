<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);


// Get ID
$post->id = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');

//Get post

    $result = $post->editservicerequestbyuser();
    $num = $result->rowCount();
   
    if($num>0)
    {
        // set values to object properties
        $post_arr = array();
        $post_arr['data'] =  array();
           
            if($row = $result->fetch(PDO::FETCH_ASSOC))
            {
                    $post_item = array(
                    "workerid"=>$row["workerid"],
                    "userid"=>$row["userid"],
                    "userfirstname"=>$row["userfirstname"],
                    "userlastname"=>$row["userlastname"],
                    "userphone"=>$row["userphone"],
                    "useremail"=>$row["useremail"],
                    "servicename"=>$row["servicename"],
                    "usermessage"=>$row["usermessage"],
                    "firstname"=>$row["workerfirstname"],
                    "lastname"=>$row["workerlastname"],
                    "workerphone"=>$row["workerphone"],
                    "workeremail"=>$row["workeremail"],
                    "service_location"=>$row["service_location"],
                    "amount"=>$row["amount"],
                    "idservices"=>$row["service"],
                    "servicedate"=>$row["servicedate"],
                    "servicetime"=>$row["servicetime"],
                    "service_status"=>$row["service_status"],
                    "payment_status"=>$row["payment_status"],
                    "payment_type"=>$row["payment_type"],
                    "worker_status"=>$row["worker_status"],
                    "servicedistrict"=>$row["servicedistrict"],
                    "categoryid"=>$row["categoryid"],
                    "createdon"=>$row["createdon"],
                    "createdby"=>$row["createdby"],
                    "idservice_request"=>$row["idservice_request"]);
                     
            }
            
           array_push($post_arr['data'], $post_item);

    }
//Create array

//Json output

    if(count($post_item)>0)
    {
        
        echo json_encode($post_arr);
        return true;
    }
    else
    {
        $error_arr = array();
        $error_arr['data']= array();
        $post_item = array("message"=>"No data to display");
        array_push($error_arr['data'],$post_item);
        echo json_encode($error_arr);
        return true;
    }
?>


