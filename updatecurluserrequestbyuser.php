<?php
session_start();
include 'urlrewrite.php';
$userid = $_POST["userid"];
$idservice_request = $_POST["idservice_request"];
//$workerid = $_POST["workerid"];
$usermessage = $_POST["usermessage"];
$servicedate = $_POST["servicedate"];
$servicetime = $_POST["servicetime"];
//$amount = $_POST["amount"];
$payment_type = $_POST["payment_type"];
$service_location = $_POST["service_location"];
$servicedistrict = $_POST["servicedistrict"];
$payment_status = $_POST["payment_status"];
$catid = $_POST["catid"];
$servicename = $_POST["servicename"];


$editservicedata = array("idservice_request"=>$idservice_request,"userid"=>$userid,"payment_type"=>"$payment_type","servicedate"=>"$servicedate", "servicetime"=>"$servicetime","service_location" =>"$service_location","usermessage"=>"$usermessage","servicedistrict"=>"$servicedistrict","payment_status"=>"$payment_status","categoryid"=>$catid,"service"=>$servicename);


//Option 1: Convert data array to json if you want to send data as json
$data = json_encode($editservicedata);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/updateservicerequestbyuser.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$success = json_decode($result);
$message = $success->message;

//close curl connection
curl_close($ch);
//print result
if($message=="service request updated successfully")
{
    $_SESSION["result"] = $success->message;
    header("location:editcurlservicerequestbyuser.php?id=$idservice_request");

}
else
{
    $_SESSION["result"] = $success->message;
    header("location:editcurlservicerequestbyuser.php?id=$idservice_request");
}
?>