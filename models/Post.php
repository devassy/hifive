<?php 

date_default_timezone_set('Asia/Kolkata');
class Post {
    //DB 

    public $conn;
    private $table = 'profiles';

    //post properties

    public $amount;

    public $categoryid;
    public $confirmpassword;
    public $created;
    public $createdby;
    public $createdon;
    public $city;

    public $deleted_status;
    public $deleted;
    public $district;
    public $servicedistrict;

    public $email;

    public $idprofiles;
    public $idservice_request;
    public $is_email;
    public $idimage;
    public $imagename;
    public $imagelocation;
    public $imageurl;
    public $idprofile;
    public $idprofile_detail;
    public $idworker_category;
    public $idworkservice;
    public $idmessage;
    public $idtransaction;

    public $location;

    public $message;

    public $password;
    public $phone;
    public $payment_status;
    public $payment_type;
    public $path;
    public $profile_address;
    public $profileid;

    public $recieverid;
    public $requestid;

    public $service;
    public $service_status;
    public $servicetime;
    public $status;
    public $serviceid;
    public $servicedate;
    public $servicename;
    public $service_requestid;
    public $service_location;
    public $servicerequestid;
    public $servicrequestid;
    public $senderid;


    public $type;
    public $transaction_provider;
    public $transactionkey;
    public $transactionstatus;
    public $transactionresponse_message;

    public $usermessage;
    public $username;
    public $userid;
   

    public $updated;
    public $updatedby;

    public $workerid;
    public $worker_status;

    private $table1 = 'profile_address';
    private $table2 = 'profile_details';
    private $table3 = 'service_requests';
    private $table4 = 'profiles';
    private $table5 = 'categories';
    private $table6 = 'worker_categories';
    private $table7 = 'services';
    private $table8 = 'worker_services';
    private $table9 = 'images';
    private $table10 = 'messages';
    private $table11 = 'transactions';

    //constructor

    public function __construct($db)
    {
        $this->conn = $db;
    }


    //Get posts
   
    public function login()
    {
        $this->email = $this->username;
        $query = 'SELECT * FROM profiles WHERE (username=:username OR email=:email) AND password=:password';

        //prepare statement

        $stmt = $this->conn->prepare($query);

        //clean data
        $this->username = htmlspecialchars(strip_tags($this->username));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->email = htmlspecialchars(strip_tags($this->email));

        
        
        

        //Bind Data
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':email', $this->email);
        
        
        
        


         //Execute query

         if($stmt->execute())
         {
            $no=$stmt->rowCount();
            if($no>0)
            {
            return $stmt;
            }
            else
            {
            return false;
            }
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
       
    }//login function ends

    //login as appuser
    
    //login as app user function ends


    public function read_singlerequest()
	{
      
        $this->deleted_status = "0";
        $this->worker_status = "1";
		$readsinglerequest = 'SELECT *
            FROM 
                service_requests
                WHERE workerid = "'.$this->workerid.'" and deleted_status= "'.$this->deleted_status.'" and worker_status= "'.$this->worker_status.'" order by createdon desc';
         
            $stmt = $this->conn->prepare($readsinglerequest);
            // prepare query statement
            $stmt->bindParam(':workerid', $this->workerid);
            $stmt->bindParam(':deleted_status', $this->deleted_status);
            $stmt->bindParam(':worker_status', $this->worker_status);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            return $stmt;
           }
		
		
    }

    public function getappworkerrequest()
    {
        $this->deleted_status = "0";
        $this->worker_status = "1";
		$readsinglerequest = 'SELECT *
            FROM 
                service_requests LEFT JOIN profile_details on service_requests.workerid = profile_details.profileid LEFT JOIN services on service_requests.service = services.idservices 
                WHERE workerid = "'.$this->workerid.'" and deleted_status= "'.$this->deleted_status.'" and worker_status= "'.$this->worker_status.'" ';
         
            $stmt = $this->conn->prepare($readsinglerequest);
            // prepare query statement
            $stmt->bindParam(':workerid', $this->workerid);
            $stmt->bindParam(':deleted_status', $this->deleted_status);
            $stmt->bindParam(':worker_status', $this->worker_status);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            return $stmt;
           }



    }

    public function addimages()
    { 
        $this->createdby = $this->idprofile;
        $insertimages = 'INSERT INTO '.$this->table9.'
        SET 
        idimage = :idimage,
        idprofile = :idprofile,
        imagename = :imagename,
        imagelocation = :imagelocation,
        imageurl = :imageurl,
        created = :created,
        createdby = :createdby';
        $stmt = $this->conn->prepare($insertimages);
         
         
        //clean data
        $this->idimage = htmlspecialchars(strip_tags($this->idimage));
        $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
        $this->imagename = htmlspecialchars(strip_tags($this->imagename));
        $this->imagelocation = htmlspecialchars(strip_tags($this->imagelocation));
        if($this->imageurl!=null)
        {
            $this->imageurl = htmlspecialchars(strip_tags($this->imageurl));
        }
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':idimage', $this->idimage);
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':imagename', $this->imagename);
        $stmt->bindParam(':imagelocation', $this->imagelocation);
        $stmt->bindParam(':imageurl', $this->imageurl);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);

        if($stmt->execute()){
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }

    public function checkimage()
    {
        
        $getimages = "select * FROM images where idprofile = '$this->idprofile' and status ='1' and deleted = '0'";
        $stmt = $this->conn->prepare($getimages);
        $stmt->bindParam(':idprofile',$this->idprofile);
        
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));


    }

    public function getimages()
    {
        $getimages = "select * FROM images where idprofile = '$this->idprofile' and status ='1'";
        $stmt = $this->conn->prepare($getimages);
        $stmt->bindParam(':idprofile',$this->idprofile);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));


    }
    public function deleteimages()
    {
        $deleteimages = "DELETE  FROM images where idimage = '$this->idimage'";
        $stmt = $this->conn->prepare($deleteimages);
        $stmt->bindParam(':idimage',$this->idimage);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
    }

    public function updateimages()
    {
            
            $this->updated = date('Y-m-d H:i:s');
            $updateimage = 'UPDATE '.$this->table9.'
            SET 
            imagename =:imagename, 
            imagelocation=:imagelocation,
            imageurl=:imageurl,
            updated=:updated,
            updatedby =:updatedby
            WHERE 
            idimage = :idimage';
            $stmt = $this->conn->prepare($updateimage);

            $this->idimage = htmlspecialchars(strip_tags($this->idimage));
            $this->imagename = htmlspecialchars(strip_tags($this->imagename));
            $this->imagelocation = htmlspecialchars(strip_tags($this->imagelocation));
            if($this->imageurl!=null)
            {
                $this->imageurl = htmlspecialchars(strip_tags($this->imageurl));
            }
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->updatedby = htmlspecialchars(strip_tags($this->updatedby));

            $stmt->bindParam(':idimage',$this->idimage);
            $stmt->bindParam(':imagename',$this->imagename);
            $stmt->bindParam(':imagelocation',$this->imagelocation);
            $stmt->bindParam(':imageurl',$this->imageurl);
            $stmt->bindParam(':updated',$this->updated);
            $stmt->bindParam(':updatedby',$this->updatedby);
            if($stmt->execute())
            {
                return $stmt;
            }
            error_log("row => " . json_encode($row));

    }
    public function updateservicerequest()
	{
		$this->updated = date('Y-m-d H:i:s');
        $this->updatedby = $this->workerid;
            // $this->updated = $this->updateservices;
            $updateservicerequest = 'UPDATE ' . $this->table3. '
            SET 
            workerid =:workerid,
            usermessage =:usermessage,
            service_location =:service_location,
            payment_type = :payment_type,
            payment_status = :payment_status,
            amount =:amount,
            servicedate =:servicedate,
            servicetime =:servicetime,
            service_status =:service_status,
            worker_status =:worker_status,
            updated =:updated, 
            updatedby =:updatedby
            WHERE 
            idservice_request =:idservice_request and  workerid =:workerid';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updateservicerequest);
    
            //clean data
            $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));
            $this->workerid =  htmlspecialchars(strip_tags($this->workerid));
			$this->usermessage = htmlspecialchars(strip_tags($this->usermessage));
            $this->service_location = htmlspecialchars(strip_tags($this->service_location));
            $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
            if($this->payment_status!=null)
            {
            $this->payment_status = htmlspecialchars(strip_tags($this->payment_status));
            }
            $this->amount = htmlspecialchars(strip_tags($this->amount));
            $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
            $this->servicetime = htmlspecialchars(strip_tags($this->servicetime));
            $this->service_status = htmlspecialchars(strip_tags($this->service_status));
            $this->worker_status = htmlspecialchars(strip_tags($this->worker_status));
            
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->updatedby = htmlspecialchars(strip_tags($this->updatedby));
			
            $stmt->bindParam(':idservice_request', $this->idservice_request);
            $stmt->bindParam(':workerid', $this->workerid);
			$stmt->bindParam(':usermessage', $this->usermessage);
            $stmt->bindParam(':service_location', $this->service_location);
            $stmt->bindParam(':payment_type', $this->payment_type);
            $stmt->bindParam(':payment_status', $this->payment_status);
            $stmt->bindParam(':amount', $this->amount);
            $stmt->bindParam(':servicedate', $this->servicedate);
            $stmt->bindParam(':servicetime', $this->servicetime);
            $stmt->bindParam(':service_status', $this->service_status);
            $stmt->bindParam(':worker_status', $this->worker_status);       
            $stmt->bindParam(':updated', $this->updated);
            $stmt->bindParam(':updatedby', $this->updatedby);
			
			
			 if($stmt->execute()){

              return $stmt;

            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
            return false;
		
		
    }
    public function checkworkerpassword()
    {
        $this->type = "W";
        $checkquery = "SELECT * FROM  $this->table WHERE username =:username and password =:password and type=:type";
     
        // prepare query statement
        $stmt = $this->conn->prepare($checkquery);
     
        // bind id of product to be updated
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':type', $this->type);
        
        if($stmt->execute())
        {
            $no = $stmt->rowCount();
            if($no>0)
            {
                return $no;
            }
            else
            {
                return false;
            }
        }
        
    }
    public function updateworkerpassword()
    {
       
        if($this->newpassword=="$this->retypenewpassword")
        {
            $this->type = "W";
            $this->password = $this->newpassword;
            $updatepassword = 'UPDATE ' . $this->table . '
            SET 
            password = :password
            WHERE 
            username = :username and 
            type = :type';
        }
        $stmt = $this->conn->prepare($updatepassword);
                $stmt->bindParam(':username', $this->username);
                $stmt->bindParam(':password', $this->password);
                $stmt->bindParam(':type', $this->type);
        if($stmt->execute())
        {
            return true;
        }
    }
    public function checkuserpassword()
    {
        //$this->type = "U";
        $checkquery = "SELECT * FROM  $this->table WHERE username =:username and password =:password";
     
        // prepare query statement
        $stmt = $this->conn->prepare($checkquery);
     
        // bind id of product to be updated
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        //$stmt->bindParam(':type', $this->type);
        
        if($stmt->execute())
        {
            $no = $stmt->rowCount();
            if($no>0)
            {
                return $no;
            }
            else
            {
                return false;
            }
        }
        
    }
    public function updateuserpassword()
    {
       
        if($this->newpassword=="$this->retypenewpassword")
        {
            //$this->type = "U";
            $this->password = $this->newpassword;
            $updatepassword = 'UPDATE ' . $this->table . '
            SET 
            password = :password
            WHERE 
            username = :username';
        }
        $stmt = $this->conn->prepare($updatepassword);
                $stmt->bindParam(':username', $this->username);
                $stmt->bindParam(':password', $this->password);
                //$stmt->bindParam(':type', $this->type);
        if($stmt->execute())
        {
            return true;
        }
    }
    public function read_single()
    {
        //Create query
              
            // query to read single record
            $readsinglequery = 'SELECT *
            FROM 
                profiles
            LEFT  JOIN
                profile_address  ON profiles.idprofiles = profile_address.profileid LEFT JOIN profile_details on profiles.idprofiles =profile_details.profileid
                LEFT JOIN images ON  profiles.idprofiles = images.idprofile WHERE idprofiles = "'.$this->id.'" ';
         
         $stmt = $this->conn->prepare($readsinglequery);
            // prepare query statement
            $stmt->bindParam(':profileid', $this->id);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            // set values to object properties
            $this->idprofiles = $row['idprofiles'];
            $this->username = $row['username'];
            $this->type = $row['type'];
            $this->password = $row['password'];
            $this->firstname = $row['firstname'];
            $this->lastname = $row['lastname'];
            $this->age = $row['age'];
            $this->email = $row['email'];
            $this->address1 = $row['address1'];
            $this->address2 = $row['address2'];
            $this->phone = $row['phone'];
            $this->gender = $row['gender'];
            $this->location = $row['location'];
            $this->sublocality = $row['sublocality'];
            $this->landmark = $row['landmark'];
            $this->city = $row['city'];
            $this->district = $row['district'];
            $this->state = $row['state'];
            $this->created  = $row["created"];
            $this->status  = $row["status"];
            $this->imagename = $row["imagename"];
            $this->idimage = $row["idimage"];
            $this->imagelocation = $row["imagelocation"];
            $this->imageurl = $row["imageurl"];
            return true;
           }
            
        
    }



    public function read_singleworker()
    {
        //Create query
              
            // query to read single record
            $readsinglequery = 'SELECT * FROM worker_categories LEFT JOIN categories on worker_categories.categoryid = categories.idcategory LEFT JOIN profiles on worker_categories.profileid=profiles.idprofiles LEFT JOIN profile_address on worker_categories.profileid = profile_address.profileid LEFT JOIN profile_details on worker_categories.profileid = profile_details.profileid LEFT JOIN images on worker_categories.profileid=images.idprofile   WHERE idprofiles = "'.$this->id.'" ';
         
         $stmt = $this->conn->prepare($readsinglequery);
            // prepare query statement
            $stmt->bindParam(':profileid', $this->id);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
             return $stmt;
           }
            
        
    }




    public function update(){
        //Update query
         
        $query = 'UPDATE ' . $this->table . '
        SET 
        username = :username,
        email = :email,
        phone = :phone,
        updated = :updated 
        WHERE 
        idprofiles = :idprofiles';

        //Prepare statement
        
        $stmt = $this->conn->prepare($query);

        //clean data

        $this->username = htmlspecialchars(strip_tags($this->username));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->phone = htmlspecialchars(strip_tags($this->phone));
        $this->idprofiles = htmlspecialchars(strip_tags($this->idprofiles));
        $this->updated = htmlspecialchars(strip_tags($this->updated));


        //Bind data


        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':phone', $this->phone);
        $stmt->bindParam(':idprofiles', $this->idprofiles);
        $stmt->bindParam(':updated', $this->updated);

        //Execute query

        if($stmt->execute()){
          return $this->idprofiles;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }
    public function updateaddress($result)
    {
        $this->profileid = $result;
        $updatequery = 'UPDATE ' . $this->table1. '
        SET 
        address1 = :address1,
        address2 = :address2,
        location = :location,
        sublocality = :sublocality,
        landmark = :landmark,
        city = :city,
        district = :district,
        state = :state,
        updated = :updated
        WHERE 
        profileid = :profileid';

        //Prepare statement

        $stmt = $this->conn->prepare($updatequery);

        //clean data

        $this->address1 = htmlspecialchars(strip_tags($this->address1));
        $this->address2 = htmlspecialchars(strip_tags($this->address2));
        $this->location = htmlspecialchars(strip_tags($this->location));
        $this->sublocality = htmlspecialchars(strip_tags($this->sublocality));
        $this->landmark = htmlspecialchars(strip_tags($this->landmark));
        $this->city = htmlspecialchars(strip_tags($this->city));
        $this->district = htmlspecialchars(strip_tags($this->district));
        $this->state = htmlspecialchars(strip_tags($this->state));
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->updated = htmlspecialchars(strip_tags($this->updated));



        //Bind data


        $stmt->bindParam(':address1', $this->address1);
        $stmt->bindParam(':address2', $this->address2);
        $stmt->bindParam(':location', $this->location);
        $stmt->bindParam(':sublocality', $this->sublocality);
        $stmt->bindParam(':landmark', $this->landmark);
        $stmt->bindParam(':city', $this->city);
        $stmt->bindParam(':district', $this->district);
        $stmt->bindParam(':state', $this->state);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':updated', $this->updated);

        //Execute query

        if($stmt->execute()){
          return $this->profileid;
            
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }
    public function updateprofile($idprofile)
    {

        $this->profileid = $idprofile;
        $updatequery = 'UPDATE ' . $this->table2. '
        SET 
        firstname = :firstname,
        lastname = :lastname,
        gender = :gender,
        age = :age,
        updated = :updated
        WHERE 
        profileid = :profileid';

        //Prepare statement

        $stmt = $this->conn->prepare($updatequery);

        //clean data

        $this->firstname = htmlspecialchars(strip_tags($this->firstname));
        $this->lastname = htmlspecialchars(strip_tags($this->lastname));
        $this->gender = htmlspecialchars(strip_tags($this->gender));
        $this->age = htmlspecialchars(strip_tags($this->age));
        $this->updated = htmlspecialchars(strip_tags($this->updated));
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
       


        //Bind data


        $stmt->bindParam(':firstname', $this->firstname);
        $stmt->bindParam(':lastname', $this->lastname);
        $stmt->bindParam(':gender', $this->gender);
        $stmt->bindParam(':age', $this->age);
        $stmt->bindParam(':updated', $this->updated);
        $stmt->bindParam(':profileid', $this->profileid);

        //Execute query

        if($stmt->execute()){

          return  $stmt;
            
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

        


    }
    public function addservicerequest()
    {
        $this->worker_status = "0";
        $this->is_email = "0";
        $servicequery = 'INSERT INTO ' . $this->table3. '
        SET 
        idservice_request = :idservice_request,
        userid = :userid,
        service = :service,
        categoryid = :categoryid,
        usermessage = :usermessage,
        service_location = :service_location,
        servicedistrict =:servicedistrict,
        payment_type = :payment_type,
        payment_status = :payment_status,
        amount = :amount,
        servicedate = :servicedate,
        servicetime = :servicetime,
        worker_status = :worker_status,
        createdon = :createdon,
        createdby = :createdby';


        //Prepare statement

        $stmt = $this->conn->prepare($servicequery);

        //clean data
       
        $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));
       
        $this->userid = htmlspecialchars(strip_tags($this->userid));
        $this->service = htmlspecialchars(strip_tags($this->service));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->usermessage = htmlspecialchars(strip_tags($this->usermessage));
        
        $this->servicedistrict =  htmlspecialchars(strip_tags($this->servicedistrict)); 
        $this->service_location = htmlspecialchars(strip_tags($this->service_location));
        $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
        $this->payment_status = htmlspecialchars(strip_tags($this->payment_status));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
        $this->servicetime = htmlspecialchars(strip_tags($this->servicetime));
        $this->servicetime = htmlspecialchars(strip_tags($this->servicetime));
        $this->worker_status = htmlspecialchars(strip_tags($this->worker_status));
        $this->createdon = htmlspecialchars(strip_tags($this->createdon));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        // $this->service_location_address = htmlspecialchars(strip_tags($this->service_location_address));
        // $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
        // $this->amount = htmlspecialchars(strip_tags($this->amount));
        // $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
        // $this->servicetime = htmlspecialchars(strip_tags($this->servicetime));
        // $this->created = htmlspecialchars(strip_tags($this->created));

        


        //Bind data


        $stmt->bindParam(':idservice_request', $this->idservice_request);
        $stmt->bindParam(':userid', $this->userid);
       
        $stmt->bindParam(':service', $this->service);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':usermessage', $this->usermessage);
        $stmt->bindParam(':servicedistrict', $this->servicedistrict);
        $stmt->bindParam(':service_location', $this->service_location);
        $stmt->bindParam(':payment_type',$this->payment_type);
        $stmt->bindParam(':payment_status',$this->payment_status);
        $stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':servicedate', $this->servicedate);
        $stmt->bindParam(':servicetime', $this->servicetime);
        $stmt->bindParam(':worker_status', $this->worker_status);
        $stmt->bindParam(':createdon', $this->createdon);
        $stmt->bindParam(':createdby', $this->createdby);
        // $stmt->bindParam(':usermessage', $this->usermessage);
        // $stmt->bindParam(':service_location', $this->service_location);
        // $stmt->bindParam(':service_location_address', $this->service_location_address);
        //$stmt->bindParam(':payment_type',$this->payment_type);
        // $stmt->bindParam(':amount', $this->amount);
        // $stmt->bindParam(':servicedate', $this->servicedate);
        // $stmt->bindParam(':servicetime', $this->servicetime);
        // $stmt->bindParam(':created', $this->created);
       

       

        //Execute query

        if($stmt->execute())
        {
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
    public function getworker()
    {
        
        $selectworker = "select firstname,lastname,idprofiles from profiles INNER JOIN profile_details on profiles.idprofiles = profile_details.profileid INNER JOIN profile_address on profiles.idprofiles = profile_address.profileid where type = 'W' and district ='$this->district'";
        $stmt = $this->conn->prepare($selectworker);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }
    
    public function get_workerservicerequest()
    {
        $this->deleted_status=0;
		$readsinglerequest = 'SELECT  servicename,servicedistrict,email,phone,payment_type,address1,address2,location,amount,firstname,lastname,username,usermessage,idservice_request,workerid,service_location,servicedate,servicetime,service_status,payment_status,worker_status,is_email  from service_requests   LEFT JOIN profiles on service_requests.userid=profiles.idprofiles LEFT JOIN profile_address on service_requests.userid = profile_address.profileid  LEFT JOIN profile_details on service_requests.userid = profile_details.profileid LEFT join services on service_requests.service = services.idservices where idservice_request = :idservice_request and deleted_status = :deleted_status';
         
         $stmt = $this->conn->prepare($readsinglerequest);
            // prepare query statement
            $stmt->bindParam(':idservice_request', $this->idservice_request);
            $stmt->bindParam(':deleted_status', $this->deleted_status);
            $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));
            $this->deleted_status = htmlspecialchars(strip_tags($this->deleted_status));
           
            // bind id of product to be updated
         
            // execute query
            if($stmt->execute())
            {
            return $stmt;
            }

    }
    public function addnewuser()
    {
        $this->message;
        if($this->password=="$this->confirmpassword")
        {
        $this->type = 'U';
        $this->createdby = $this->idprofiles;
        $servicequery = 'INSERT INTO ' . $this->table4. '
        SET 
        idprofiles = :idprofiles,
        type =:type,
        username =:username,
        password =:password,
        email =:email,
        phone =:phone,
        status =:status, 
        created = :created,
        createdby = :createdby';


        //Prepare statement

        $stmt = $this->conn->prepare($servicequery);

        //clean data
       
        $this->idprofiles = htmlspecialchars(strip_tags($this->idprofiles));
        $this->type = htmlspecialchars(strip_tags($this->type));
        $this->username = htmlspecialchars(strip_tags($this->username));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->phone = htmlspecialchars(strip_tags($this->phone));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        // $this->service_location_address = htmlspecialchars(strip_tags($this->service_location_address));
        // $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
        // $this->amount = htmlspecialchars(strip_tags($this->amount));
        // $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
        // $this->servicetime = htmlspecialchars(strip_tags($this->servicetime));
        // $this->created = htmlspecialchars(strip_tags($this->created));

        


        //Bind data


        $stmt->bindParam(':idprofiles', $this->idprofiles);
        $stmt->bindParam(':type', $this->type);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':phone', $this->phone);
        $stmt->bindParam(':status', $this->status);
        $stmt->bindParam(':created',$this->created);
        $stmt->bindParam(':createdby', $this->createdby);
       
        //Execute query

        if($stmt->execute())
        {
            return true;
        }
        else
        {
            $this->message = "Failed to insert";
            return $this->message;
        }
        }
        else
        {
            $this->message = "Passwords Mismatch";
            return $this->message;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }

    
    public function addnewuseraddress($idprofiles,$profileaddress)
    {
      
        $this->profileid = $idprofiles;
        $this->idprofile_address = $profileaddress;
        $newuseraddress = 'INSERT INTO ' . $this->table1. '
        SET 
        idprofile_address = :idprofile_address,
        profileid = :profileid,
        created = :created,
        createdby = :createdby';

        
        $stmt = $this->conn->prepare($newuseraddress);

        $stmt->bindParam(':idprofile_address', $this->idprofile_address);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);

       
        if($stmt->execute())
        {
            return true;
        }

    }
    public function addnewuserprofile($idprofiles,$idprofile)
    {
        $this->idprofile_detail = $idprofile;
        $this->createdby = $idprofile;
        $this->profileid = $idprofiles;
        $newuserprofile = 'INSERT INTO ' . $this->table2. '
        SET 
        idprofile_detail = :idprofile_detail,
        profileid = :profileid,
        created = :created,
        createdby = :createdby';

        
        $stmt = $this->conn->prepare($newuserprofile);

        $stmt->bindParam(':idprofile_detail', $this->idprofile_detail);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);

       
        if($stmt->execute())
        {
            return true;
        }

    }
    public function checkusername()
    {
        $query3 = "SELECT * FROM  $this->table WHERE username =:username";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query3);
     
        // bind id of product to be updated
        $stmt->bindParam(':username', $this->username);
     
        if($stmt->execute())
        {
            return $stmt;
        }
       
    }
    public function checkemail()
    {
        $query3 = "SELECT * FROM  $this->table WHERE email =:email";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query3);
     
        // bind id of product to be updated
        $stmt->bindParam(':email', $this->email);
     
        if($stmt->execute())
        {
            return $stmt;
        }
       
    }
     
    public function readuserrequest()
    {
        $this->deleted_status = "0";
       
		$readsinglerequest = 'SELECT *
            FROM 
                service_requests LEFT JOIN profile_details on service_requests.userid = profile_details.profileid LEFT JOIN services on service_requests.service = services.idservices
                WHERE userid = "'.$this->userid.'" and deleted_status= "'.$this->deleted_status.'" order by createdon desc ';
         
            $stmt = $this->conn->prepare($readsinglerequest);
            // prepare query statement
            $stmt->bindParam(':userid', $this->userid);
            $stmt->bindParam(':deleted_status', $this->deleted_status);
            
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            return $stmt;
           }

    }
    public function editservicerequestbyuser()
    {
        $this->idservice_request = $this->id;
        $readsinglequery = 'SELECT idservice_request,userid,service,service_requests.categoryid,service_requests.createdby,servicename,usermessage,servicedistrict,service_location,amount,servicedate,service_status,servicetime,worker_status,
        payment_status,createdon,payment_type,pd1.profileid as workerid,pd1.firstname as workerfirstname,pd1.lastname as workerlastname,pr.email as workeremail,pr.phone as workerphone,
        pd.firstname as userfirstname,pd.lastname as userlastname,pr1.email as useremail,pr1.phone as userphone  FROM service_requests
        LEFT JOIN profile_details as pd on service_requests.userid = pd.profileid
        LEFT JOIN profiles as pr on service_requests.workerid = pr.idprofiles
        LEFT JOIN profiles as pr1 on service_requests.userid = pr1.idprofiles
        LEFT JOIN profile_details as pd1 on service_requests.workerid = pd1.profileid
        LEFT JOIN services on service_requests.service = services.idservices
        where idservice_request = "'.$this->idservice_request.'"';
                
         
         $stmt = $this->conn->prepare($readsinglequery);
            // prepare query statement
            $stmt->bindParam(':idservice_request',$this->idservice_request);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
               return $stmt;
           }

    }

    public function updateservicerequestbyuser()
	{
          
            $this->updated = date('Y-m-d H:i:s');
            $this->updatedby = $this->userid;
            // $this->updated = $this->updateservices;
            $updateservicerequest = 'UPDATE '.$this->table3.'
            SET 
            service =:service,
            categoryid = :categoryid,
            usermessage =:usermessage,
            servicedistrict =:servicedistrict,
            service_location =:service_location,
            payment_type = :payment_type,
            payment_status =:payment_status,
            servicedate =:servicedate,
            servicetime =:servicetime,
            updated =:updated, 
            updatedby =:updatedby
            WHERE 
            idservice_request =:idservice_request';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updateservicerequest);
    
            //clean data
            $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));
            $this->service = htmlspecialchars(strip_tags($this->service));
            $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
            //$this->workerid =  htmlspecialchars(strip_tags($this->workerid));
            $this->usermessage = htmlspecialchars(strip_tags($this->usermessage));
            $this->servicedistrict = htmlspecialchars(strip_tags($this->servicedistrict));
            $this->service_location = htmlspecialchars(strip_tags($this->service_location));
            $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
            $this->payment_status = htmlspecialchars(strip_tags($this->payment_status));
            // $this->amount = htmlspecialchars(strip_tags($this->amount));
            $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
            $this->servicetime = htmlspecialchars(strip_tags($this->servicetime));
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->updatedby = htmlspecialchars(strip_tags($this->updatedby));
			
            $stmt->bindParam(':idservice_request', $this->idservice_request);
            //$stmt->bindParam(':workerid', $this->workerid);
            $stmt->bindParam(':service', $this->service);
            $stmt->bindParam(':categoryid', $this->categoryid);
            $stmt->bindParam(':usermessage', $this->usermessage);
            $stmt->bindParam(':servicedistrict', $this->servicedistrict);
            $stmt->bindParam(':service_location', $this->service_location);
            $stmt->bindParam(':payment_type', $this->payment_type);
            $stmt->bindParam(':payment_status', $this->payment_status);

            // $stmt->bindParam(':amount', $this->amount);
            $stmt->bindParam(':servicedate', $this->servicedate);
            $stmt->bindParam(':servicetime', $this->servicetime);
            $stmt->bindParam(':updated', $this->updated);
            $stmt->bindParam(':updatedby', $this->updatedby);
			
			
			 if($stmt->execute()){

              return true;

            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
            return false;
		
		
    }
    public function updateuserimage($idimage,$idprofile,$imagelocation)
    {
        $this->updated = date('Y-m-d H:i:s');
        $this->idimage= $idimage;
        $this->idprofile = $idprofile;
        $this->updatedby = $idprofile;
        $this->imagelocation = $imagelocation;
         
            // $this->updated = $this->updateservices;
        $updateuserimage = 'UPDATE ' . $this->table9. '
        SET 
        imagename = :imagename,
        imagelocation = :imagelocation,
        updated = :updated, 
        updatedby = :updatedby
        WHERE 
        idimage = :idimage and idprofile = :idprofile';

        //Prepare statement

        $stmt = $this->conn->prepare($updateuserimage);

        //clean data
        $this->idimage = htmlspecialchars(strip_tags($this->idimage));
        $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
        $this->imagename = htmlspecialchars(strip_tags($this->imagename));
        $this->imagelocation = htmlspecialchars(strip_tags($this->imagelocation));
        $this->updated = htmlspecialchars(strip_tags($this->updated));
        $this->updatedby = htmlspecialchars(strip_tags($this->updatedby));
        
        $stmt->bindParam(':idimage', $this->idimage);
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':imagename', $this->imagename);
        $stmt->bindParam(':imagelocation', $this->imagelocation);
        $stmt->bindParam(':updated', $this->updated);
        $stmt->bindParam(':updatedby', $this->updatedby);
        
        
            if($stmt->execute()){

            return true;

        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);
        return false;


    }

    public function addworkerimage($idimage,$idprofiles)
    {
        $this->idprofile = $idprofiles;
        $this->idimage = $idimage;
        $this->created = date("Y-m-d H:i:s");
        $this->createdby = $idprofiles;
       
        $imagequery = 'INSERT INTO ' . $this->table9 . '
        SET 
        idimage =:idimage,
        idprofile =:idprofile,
        imagename=:imagename,
        created =:created,
        createdby =:createdby';


        //Prepare statement

        $stmt = $this->conn->prepare($imagequery);

        //clean data

        $this->idimage = htmlspecialchars(strip_tags($this->idimage));
        $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
        $this->imagename = htmlspecialchars(strip_tags($this->imagename));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        //Bind data
        $stmt->bindParam(':idimage', $this->idimage);
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':imagename', $this->imagename);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);
        
       

        //Execute query

        if($stmt->execute())
        {
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
    public function getworkercategory($idworkercategory)
	{
        
        $this->profileid = $idworkercategory;
		$readsinglerequest = 'SELECT *
            FROM 
                worker_categories
                WHERE profileid = "'.$this->profileid.'"';
         
            $stmt = $this->conn->prepare($readsinglerequest);
            // prepare query statement
            $stmt->bindParam(':idworker_category', $this->idworker_category);

            // bind id of product to be updated
         
            // execute query
          $stmt->execute();
        
        return $stmt;
		
		
    }
    // public function updateworkercategory($value,$idprofiles)
    // {
    //     $idworker_category = str_shuffle("0123456789");
    //     $this->idworker_category = $idworker_category;
    //     $this->profileid = $idprofiles;
    //     $this->categoryid= $value;
    //     $this->created = date('Y-m-d H:i:s');
    //     $this->createdby = $idprofiles;
    //     $updatecategory = 'INSERT INTO ' . $this->table5 . '
    //     SET 
    //     idworker_category = :idworker_category,
    //     profileid = :profileid,
    //     categoryid = :categoryid,
    //     created = :created,
    //     createdby = :createdby';
    //     $stmt = $this->conn->prepare($updatecategory);
         
         
    //     //clean data
    //     $this->idworker_category = htmlspecialchars(strip_tags($this->idworker_category));
    //     $this->profileid = htmlspecialchars(strip_tags($this->profileid));
    //     $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
    //     $this->created = htmlspecialchars(strip_tags($this->created));
    //     $this->createdby = htmlspecialchars(strip_tags($this->createdby));

    //     $stmt->bindParam(':idworker_category', $this->idworker_category);
    //     $stmt->bindParam(':profileid', $this->profileid);
    //     $stmt->bindParam(':categoryid', $this->categoryid);
    //     $stmt->bindParam(':created', $this->created);
    //     $stmt->bindParam(':createdby', $this->createdby);
       
        
    //     if($stmt->execute())
    //     {
           
    //         return $this->idworker_category;
    //     }
    //     echo 'Connection  Error' . $e->getMessage();
    //     //print error message if it has errors

    //     printf("Error: %s.\n", $stmt->error);


    //     return false;
    // }

    public function insertworkercategory($profileid,$value){
        //delete query
       
        $this->idworker_category = str_shuffle("0123456789");
        $this->profileid = $profileid;
        $this->categoryid = $value;
        $this->created = date('Y-m-d H:i:s');
        $this->createdby = $profileid;
        $updatecategory = 'INSERT INTO ' . $this->table6. '
        SET 
        idworker_category = :idworker_category,
        categoryid = :categoryid,
        profileid = :profileid,
        created = :created,
        createdby = :createdby';

        //prepare statement

        $stmt = $this->conn->prepare($updatecategory);

        //clean data
        $this->idworker_category = htmlspecialchars(strip_tags($this->idworker_category));
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        //Bind Data
        $stmt->bindParam(':idworker_category', $this->idworker_category);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);


         //Execute query

         if($stmt->execute())
         {
            return true;
         }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }
    public function selectedworker_categories()
    {
        //Create query
              
            // query to read single record
            $readselectedworker_category = 'SELECT *
            FROM worker_categories
            LEFT  JOIN
               categories ON worker_categories.categoryid = categories.idcategory where profileid = "'.$this->id.'" ';
         
         $stmt = $this->conn->prepare($readselectedworker_category);
            // prepare query statement
            $stmt->bindParam(':profileid', $this->id);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
             return $stmt;
           }
            
        
    }
    public function readservices($id)
    {
       
            $this->idprofile = $id;
            //Create query
            $readservices = 'SELECT * FROM services WHERE NOT EXISTS(SELECT * FROM worker_services WHERE services.idservices=worker_services.serviceid and idprofile=:idprofile)';
    
            // Prepare statement 
    
            $stmt = $this->conn->prepare($readservices);
    
            //Bind ID 
            $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
            $stmt->bindParam(':idprofile', $this->idprofile);
            // $stmt->bindParam(':username', $this->username);
            // $stmt->bindParam(':email', $this->email);
            // $stmt->bindParam(':phone', $this->phone);
            // $stmt->bindParam(':idprofile_address', $this->idprofile_address);
            // $stmt->bindParam(':profileid', $this->profileid);
            // $stmt->bindParam(':address1', $this->address1);
    
    
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));
            // Set properties
    
            // if($row){
            //     $this->title = $row['title'];
            //     $this->body = $row['body'];
            //     $this->author = $row['author'];
            //     $this->category_id = $row['category_id'];
            // }
            // else{
            //     return false;
            // }
    }
    // public function deletecategory($id){
    //     //delete query
    //     $this->profileid = $id;
    //     $deletecategory = 'DELETE FROM ' . $this->table6 . ' WHERE profileid = :profileid';

    //     //prepare statement

    //     $stmt = $this->conn->prepare($deletecategory);

    //     //clean data
    //     $this->profileid = htmlspecialchars(strip_tags($this->profileid));

    //     //Bind Data
    //     $stmt->bindParam(':profileid', $this->profileid);


    //      //Execute query

    //      if($stmt->execute()){
    //         return true;
    //     }
    //     echo 'Connection  Error' . $e->getMessage();
    //     //print error message if it has errors

    //     printf("Error: %s.\n", $stmt->error);


    //     return false;


    // }
    public function deleteworkercategory($id,$catid){
        //delete query
        $this->profileid = $id;
        $this->categoryid = $catid;
        
        $deletecategory = 'DELETE FROM ' . $this->table6 . ' WHERE categoryid = :categoryid and profileid=:profileid';

        //prepare statement

        $stmt = $this->conn->prepare($deletecategory);

        //clean data
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));

        //Bind Data
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':categoryid', $this->categoryid);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }
    public function readworkercategory()
    {
    
        $readcategory ='SELECT * FROM categories WHERE NOT EXISTS(SELECT * FROM worker_categories WHERE categories.idcategory = worker_categories.categoryid and profileid = :profileid)';
       
        $stmt = $this->conn->prepare($readcategory);
            // Prepare statement 
        $stmt->bindParam(':profileid', $this->profileid);
        
    
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));

    }
    public function getservices()
    {
       
        $getservices = 'SELECT idservices,servicename,categoryid,created from ' . $this->table7 . '  where categoryid = "'.$this->categoryid.'"';

        // Prepare statement 

        $stmt = $this->conn->prepare($getservices);
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));

        $stmt->bindParam(':categoryid', $this->categoryid);
         
        
        $stmt->execute();
        
        return $stmt;
    }
    
    //Filling 4th column in add worker services
    public function addworkerservices($profileid,$servicename,$categoryid,$amount,$value){
        
       
        $this->idworkservice = str_shuffle("0123456789");
        $this->idprofile = $profileid;
        $this->serviceid = $value;
        $this->amount = $amount;
        $this->categoryid = $categoryid;
        $this->servicename = $servicename;
        $this->created = date('Y-m-d H:i:s');
        $this->createdby = $profileid;
        
        $addworkerservices = 'INSERT INTO ' . $this->table8. '
        SET 
        idworkservice =:idworkservice,
        idprofile =:idprofile,
        serviceid =:serviceid,
        categoryid =:categoryid,
        amount =:amount,
        servicename =:servicename,
        created =:created,
        createdby =:createdby';

        //prepare statement

        $stmt = $this->conn->prepare($addworkerservices);

        //clean data
        $this->idworkservice = htmlspecialchars(strip_tags($this->idworkservice));
        $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
        $this->serviceid = htmlspecialchars(strip_tags($this->serviceid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->servicename = htmlspecialchars(strip_tags($this->servicename));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        //Bind Data
        $stmt->bindParam(':idworkservice', $this->idworkservice);
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':serviceid', $this->serviceid);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':servicename', $this->servicename);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);


         //Execute query

         if($stmt->execute())
         {
            return true;
         }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }
    public function getcategoryid($value)
    {
        $this->idservices = $value;
        $getservices = 'SELECT * from ' . $this->table7 . '  where idservices = "'.$this->idservices.'"';

        // Prepare statement 

        $stmt = $this->conn->prepare($getservices);
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        
        $stmt->bindParam(':idservices', $this->idservices);

        $stmt->execute();
        return $stmt;
    }
      //Filling 4th column in add worker services
    public function getworkerservices($id)
    {
     
        $this->idprofile = $id;
       
        $readworkerservices ='SELECT * FROM worker_services where idprofile=:idprofile';
       
        $stmt = $this->conn->prepare($readworkerservices);
            // Prepare statement 
        $stmt->bindParam(':idprofile',$this->idprofile);
        
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));

    }

    public function deleteworkerservices($id,$catid){
        //delete query
        $this->idprofile = $id;
        $this->categoryid = $catid;
        
        $deletecategory = 'DELETE FROM ' . $this->table8 . ' WHERE categoryid = :categoryid and idprofile=:idprofile';

        //prepare statement

        $stmt = $this->conn->prepare($deletecategory);

        //clean data
        $this->idprofile = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));

        //Bind Data
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':categoryid', $this->categoryid);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }

    public function deleteonlyworkerservices($id,$serviceid){
        //delete query
        $this->idprofile = $id;
        $this->serviceid = $serviceid;
        
        $deletecategory = 'DELETE FROM ' . $this->table8. ' WHERE serviceid = :serviceid and idprofile=:idprofile';

        //prepare statement

        $stmt = $this->conn->prepare($deletecategory);

        //clean data
        $this->idprofile = htmlspecialchars(strip_tags($this->profileid));
        $this->serviceid = htmlspecialchars(strip_tags($this->serviceid));

        //Bind Data
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':serviceid', $this->serviceid);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }


    public function searchworkers($location,$servicename){

        $this->location = "%$location%";
        $this->servicename = "%$servicename%";
        $this->categoryname = "%$servicename%";
        $selectworker = "select * from worker_services LEFT JOIN categories on worker_services.categoryid = categories.idcategory LEFT JOIN profiles on worker_services.idprofile = profiles.idprofiles LEFT JOIN  profile_address on worker_services.idprofile = profile_address.profileid LEFT JOIN images on worker_services.idprofile = images.idprofile LEFT JOIN profile_details on worker_services.idprofile = profile_details.profileid where location like :location and servicename like :servicename or categoryname like :categoryname";
        $stmt = $this->conn->prepare($selectworker);
        $this->location = htmlspecialchars(strip_tags($this->location));
        $this->servicename = htmlspecialchars(strip_tags($this->servicename));
        $this->categoryname = htmlspecialchars(strip_tags($this->categoryname));
        
        $stmt->bindParam(':location', $this->location);
        $stmt->bindParam(':servicename', $this->servicename);
        $stmt->bindParam(':categoryname', $this->categoryname);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }
    public function readcategory()
    {
       

            //Create query
            $readcategory = 'SELECT * FROM categories';
    
            // Prepare statement 
    
            $stmt = $this->conn->prepare($readcategory);
    
            //Bind ID 
            
            // $stmt->bindParam(':idprofiles', $this->idprofiles);
            // $stmt->bindParam(':username', $this->username);
            // $stmt->bindParam(':email', $this->email);
            // $stmt->bindParam(':phone', $this->phone);
            // $stmt->bindParam(':idprofile_address', $this->idprofile_address);
            // $stmt->bindParam(':profileid', $this->profileid);
            // $stmt->bindParam(':address1', $this->address1);
    
    
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));
            // Set properties
    
            // if($row){
            //     $this->title = $row['title'];
            //     $this->body = $row['body'];
            //     $this->author = $row['author'];
            //     $this->category_id = $row['category_id'];
            // }
            // else{
            //     return false;
            // }
    }
    public function readmessages()
    {
        //Create query
              
            // query to read single record
            $readsinglemessage = 'SELECT * FROM messages where recieverid = "'.$this->recieverid.'" order by created';
         
         $stmt = $this->conn->prepare($readsinglemessage);
            // prepare query statement
            $stmt->bindParam(':recieverid', $this->recieverid);
           
           
           if($stmt->execute())
           {
             return $stmt;
           }
            
        
    }

    public function readsingleuserrequest()
    {
        $readsinglemessage = 'SELECT * FROM messages where idmessage = "'.$this->idmessage.'" order by created';
         
        $stmt = $this->conn->prepare($readsinglemessage);
           // prepare query statement
           $stmt->bindParam(':idmessage', $this->idmessage);
          
          
          if($stmt->execute())
          {
            return $stmt;
          }

    }
    public function getadminid()
    {
        $selectadminid = "select * FROM profiles where type = 'A' LIMIT 0,1";
        $stmt = $this->conn->prepare($selectadminid);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }
    public function addusermessage()
    { 
     
        $insertmessage = 'INSERT INTO ' .$this->table10. '
        SET 
        idmessage =:idmessage,
        senderid =:senderid,
        servicrequestid =:servicrequestid,
        recieverid =:recieverid,
        message =:message,
        subject =:subject,
        status =:status,
        created =:created,
        createdby =:createdby';
        $stmt = $this->conn->prepare($insertmessage);
         
         
        //clean data
        $this->idmessage = htmlspecialchars(strip_tags($this->idmessage));
        $this->senderid = htmlspecialchars(strip_tags($this->senderid));
        $this->servicrequestid = htmlspecialchars(strip_tags($this->servicrequestid));
        $this->recieverid = htmlspecialchars(strip_tags($this->recieverid));
        $this->message = htmlspecialchars(strip_tags($this->message));
        $this->subject = htmlspecialchars(strip_tags($this->subject));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':idmessage', $this->idmessage);
        $stmt->bindParam(':senderid', $this->senderid);
        $stmt->bindParam(':servicrequestid', $this->servicrequestid);
        $stmt->bindParam(':recieverid',$this->recieverid);
        $stmt->bindParam(':message',$this->message);
        $stmt->bindParam(':subject',$this->subject);
        $stmt->bindParam(':status',$this->status);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);

        if($stmt->execute()){
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
    public function getservicemessage()
    {
          // query to read single record
          $readservicemessage = 'select ms.senderid,ms.recieverid,ms.idmessage,ms.servicrequestid,ms.subject,ms.created,ms.message,services.servicename,profile_details.firstname,profile_details.lastname,service_requests.usermessage,profiles.type FROM messages as ms LEFT join service_requests on ms.servicrequestid = service_requests.idservice_request LEFT JOIN services on service_requests.service = services.idservices LEFT join profile_details on service_requests.userid = profile_details.profileid LEFT JOIN profiles on ms.senderid = profiles.idprofiles where servicrequestid = "'.$this->servicerequestid.'" ORDER BY ms.created desc';         
          $stmt = $this->conn->prepare($readservicemessage);
             // prepare query statement
             $stmt->bindParam(':servicerequestid', $this->servicerequestid);
            
             // bind id of product to be updated
          
             // execute query
            if($stmt->execute())
            {
              return $stmt;
            }

    }

    public function getsendmessage()
    {
    
        $getservicemessage = 'SELECT ms.idmessage,ms.servicrequestid,ms.senderid,ms.recieverid,ms.message,ms.subject,ms.created,profiles.type,profiles.idprofiles,profile_details.firstname,profile_details.lastname,service_requests.usermessage FROM messages as ms LEFT JOIN profiles on ms.senderid = profiles.idprofiles LEFT JOIN profile_details on ms.senderid = profile_details.profileid LEFT JOIN service_requests on service_requests.idservice_request = ms.servicrequestid  where ms.senderid = "'.$this->senderid.'" ORDER BY ms.created desc';         
          $stmt = $this->conn->prepare($getservicemessage);
             // prepare query statement
             $stmt->bindParam(':senderid',$this->senderid);
            
             // bind id of product to be updated
          
             // execute query
            if($stmt->execute())
            {
              return $stmt;
            }

    }

    public function getreceivermessages()
    {
    
        $getreceivermessage = 'SELECT ms.idmessage,ms.senderid,ms.recieverid,ms.servicrequestid,ms.message,ms.subject,ms.created,profiles.type,profiles.idprofiles,profile_details.firstname,profile_details.lastname,service_requests.usermessage FROM messages as ms LEFT JOIN profiles on ms.recieverid = profiles.idprofiles LEFT JOIN profile_details on ms.recieverid = profile_details.profileid  LEFT JOIN service_requests on service_requests.idservice_request = ms.servicrequestid where ms.recieverid = "'.$this->recieverid.'" ORDER BY ms.created desc';         
          $stmt = $this->conn->prepare($getreceivermessage);
             // prepare query statement
             $stmt->bindParam(':recieverid', $this->recieverid);
            
             // bind id of product to be updated
          
             // execute query
            if($stmt->execute())
            {
              return $stmt;
            }

    }
    public function getworkerservicemessage()
    {
        $workerservicemessage = 'select ms.senderid,ms.recieverid,ms.idmessage,ms.servicrequestid,ms.subject,ms.created,ms.message,services.servicename,profile_details.firstname,profile_details.lastname,service_requests.usermessage,profiles.type FROM messages as ms LEFT join service_requests on ms.servicrequestid = service_requests.idservice_request LEFT JOIN services on service_requests.service = services.idservices LEFT join profile_details on service_requests.userid = profile_details.profileid LEFT JOIN profiles on ms.senderid = profiles.idprofiles where servicrequestid = "'.$this->servicrequestid.'" ORDER BY ms.created desc';
         
        $stmt = $this->conn->prepare($workerservicemessage);
           // prepare query statement
          
           $stmt->bindParam(':servicrequestid', $this->servicrequestid);
           // bind id of product to be updated
        
           // execute query
          if($stmt->execute())
          {
            return $stmt;
          }
        
    }

    public function getassignedworkerdetails()
    {
        $selectworker = "select firstname,lastname FROM messages LEFT JOIN service_requests on messages.servicrequestid = service_requests.idservice_request LEFT JOIN profile_details on service_requests.workerid = profile_details.profileid where servicrequestid = '.$this->idservice_request.'";
        $stmt = $this->conn->prepare($selectworker);
        $stmt->bindParam(':idservice_request', $this->idservice_request);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
      
    }
    public function getallmessages()
    {
        $selectmessages = "SELECT * FROM messages LEFT JOIN service_requests on messages.servicrequestid = service_requests.idservice_request LEFT JOIN services on service_requests.service = services.idservices where idmessage = '$this->idmessage'";
        $stmt = $this->conn->prepare($selectmessages);
        $stmt->bindParam(':idmessage', $this->idmessage);
        if($stmt->execute())
        {
          return $stmt;
        }
        error_log("row => " . json_encode($row));

    }
    public function getrewards()
    {
        $getrewards = "select * FROM rewards where userid = '$this->userid' and status ='1'";
        $stmt = $this->conn->prepare($getrewards);
        $stmt->bindParam(':userid',$this->userid);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
    }
    public function getuseremail()
    {
        $getuseremail = "select * FROM profiles where idprofiles = '$this->userid'";
        $stmt = $this->conn->prepare($getuseremail);
        $stmt->bindParam(':userid',$this->userid);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
    }
    
    public function updateuserpaymentstatus()
    {
        $this->payment_status = "1";
        $this->updated = date('Y-m-d H:i:s');
        $this->updatedby = $this->userid;
        $updateuserpaymentstatus = 'UPDATE '.$this->table3.'
        SET 
        payment_status =:payment_status,
        updated =:updated, 
        updatedby =:updatedby
        WHERE 
        idservice_request =:idservice_request and  userid =:userid';
        $stmt = $this->conn->prepare($updateuserpaymentstatus);
        $stmt->bindParam(':userid',$this->userid);
        $stmt->bindParam(':idservice_request',$this->idservice_request);
        $stmt->bindParam(':payment_status',$this->payment_status);
        $stmt->bindParam(':updated',$this->updated);
        $stmt->bindParam(':updatedby',$this->updatedby);
        if($stmt->execute())
        {
            return true;
        }
        error_log("row => " . json_encode($row));
    }
    // public function addtransaction()
    // {
    //     $this->payment_status = "1";
    //     $this->updated = date('Y-m-d H:i:s');
    //     $this->updatedby = $this->userid;
    //     $updateuserpaymentstatus = 'UPDATE '.$this->table3.'
    //     SET 
    //     payment_status =:payment_status,
    //     updated =:updated, 
    //     updatedby =:updatedby
    //     WHERE 
    //     idservice_request =:idservice_request and  userid =:userid';
    //     $stmt = $this->conn->prepare($updateuserpaymentstatus);
    //     $stmt->bindParam(':userid',$this->userid);
    //     $stmt->bindParam(':idservice_request',$this->idservice_request);
    //     $stmt->bindParam(':payment_status',$this->payment_status);
    //     $stmt->bindParam(':updated',$this->updated);
    //     $stmt->bindParam(':updatedby',$this->updatedby);
    //     if($stmt->execute())
    //     {
    //         return true;
    //     }
    //     error_log("row => " . json_encode($row));
    // }
    public function addtransactiondetails()
    {
        $this->created = date('Y-m-d H:i:s');
        $this->requestid = $this->idservice_request;
        $this->createdby = $this->userid;
        $addtransaction = 'INSERT INTO '.$this->table11.'
        SET 
        idtransaction =:idtransaction,
        requestid =:requestid,
        type =:type,
        transaction_provider =:transaction_provider,
        transactionstatus =:transactionstatus,
        transactionkey =:transactionkey,
        transactionresponse_message =:transactionresponse_message,
        amount =:amount,
        serviceid =:serviceid,
        categoryid =:categoryid,
        created =:created,
        createdby =:createdby';

        $stmt = $this->conn->prepare($addtransaction);

        $this->idtransaction = htmlspecialchars(strip_tags($this->idtransaction));
        $this->requestid = htmlspecialchars(strip_tags($this->requestid));
        $this->type = htmlspecialchars(strip_tags($this->type));
        $this->transaction_provider = htmlspecialchars(strip_tags($this->transaction_provider));
        $this->transactionstatus = htmlspecialchars(strip_tags($this->transactionstatus));
        $this->transactionkey = htmlspecialchars(strip_tags($this->transactionkey));
        $this->transactionresponse_message = htmlspecialchars(strip_tags($this->transactionresponse_message));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->serviceid = htmlspecialchars(strip_tags($this->serviceid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':idtransaction',$this->idtransaction);
        $stmt->bindParam(':requestid',$this->requestid);
        $stmt->bindParam(':type',$this->type);
        $stmt->bindParam(':transaction_provider',$this->transaction_provider);
        $stmt->bindParam(':transactionstatus',$this->transactionstatus);
        $stmt->bindParam(':transactionkey',$this->transactionkey);
        $stmt->bindParam(':transactionresponse_message',$this->transactionresponse_message);
        $stmt->bindParam(':amount',$this->amount);
        $stmt->bindParam(':serviceid',$this->serviceid);
        $stmt->bindParam(':categoryid',$this->categoryid);
        $stmt->bindParam(':created',$this->created);
        $stmt->bindParam(':createdby',$this->createdby);
        

        
        if($stmt->execute())
        {
            return true;
        }
        error_log("row => " . json_encode($row));
    }
    public function getmytransactions()
    {
        $gettransactions = "select transactions.amount,transactions.type,transactions.transaction_provider,transactions.transactionresponse_message,transactions.transactionstatus,transactions.created FROM transactions LEFT JOIN service_requests on service_requests.idservice_request = transactions.requestid where service_requests.userid = '$this->userid'";
        $stmt = $this->conn->prepare($gettransactions);
        $stmt->bindParam(':requestid',$this->requestid);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
    }
    public function getcategoryservicefromrequest()
    {
        $getrewards = "select * FROM service_requests where idservice_request = '$this->idservice_request' and deleted_status ='0'";
        $stmt = $this->conn->prepare($getrewards);
        $stmt->bindParam(':idservice_request',$this->idservice_request);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
    }
    
}//class ends
?>