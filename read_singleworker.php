<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);


// Get ID
$post->id = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');

//Get post

$result = $post->read_singleworker();
//Get row count

$num = $result->rowCount();

//Check if any posts
if($num > 0 ){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
         $categoryname[] = $row["categoryname"];
        $post_item = array(
            'idprofiles'=>$row["idprofiles"],
            'firstname'=>$row["firstname"],
            'lastname'=>$row["lastname"],
            'gender'=>$row["gender"],
            'age'=>$row["age"],
            'city'=>$row["city"],
            'district'=>$row["district"],
            'location'=>$row["location"],
        );
    }
    array_push($post_arr['data'], $post_item);
    array_push($post_arr['data'], $categoryname);
    
    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
    echo json_encode(
        array('message' => 'No records')
    );
    return false;
}


?>
