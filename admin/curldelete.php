<?php
session_start();
include 'urlrewrite.php';
$idprofiles = $_GET["idprofiles"];
$newdata = array("idprofiles" =>"$idprofiles");
//Option 1: Convert data array to json if you want to send data as json
$data = json_encode($newdata);
//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/delete.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$newresult = json_decode($result);
$message = $newresult->message;
//close curl connection
curl_close($ch);
//print result
if($message=="Data Deleted Successfully")
{
  $_SESSION["message"] = $message;
  header("location:listusers.php");
}
else
{
  $_SESSION["message"] = $message;
  header("location:listusers.php");

}
?>