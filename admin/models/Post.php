<?php 
date_default_timezone_set('Asia/Kolkata');

class Post {
    //DB 

    public $conn;
    public $id;
    public $lim;
    public $limit;
    public $off;
    public $page;
    private $table = 'profiles';

    //post properties
    public $adminid;
    public $idprofiles;
    public $type;
    public $username;
    public $password;
    public $email;
    public $phone;
    public $paymenttype;
    public $payment_type;
    public $status;
    public $deleted;
    public $created;
    public $createdon;
    public $createdby;
    public $updated;
    public $updatedby;
    public $currentpassword;
    public $newpassword;
    public $retypenewpassword;
    private  $table1 = 'profile_address';
    public $idprofile_address;
    public $profileid;
    public $address;
    public $address1;
    public $address2;
    public $location;
    public $sublocality;
    public $city;
    public $landmark;
    public $district;
    public $state;
    private $table2 = 'profile_details';
    public $idprofile_detail;
    public $firstname;
    public $lastname;
    public $gender;
    public $age;
    private $table3 = 'worker_categories';
    public $category_id;
    public $idworker_category;
    public $category_created;
    public $categoryid;
    private $table4 = 'worker_locations';
    public $idworker_location;
    public $placeid; 
    private $table5 = 'worker_services';
    public $idprofile;
    public $idworkservice;
    public $idreward;
    public $serviceid; 
    //public $servicename;
    public $adminusername;
    private $table6 = 'categories';
    public $idcategory;
    public $categoryname;
    public $applicationid="123456";
    public $updatedcategory;
    private $table7 = 'services';
    public $idservices;
    public $servicename;
    public $createdservice;
    public $catid;
    public $table8 = 'service_requests';
    public $idservice_request;
    public $userid;
    public $service;
    public $service_location;
    public $servicedistrict;
    public $payment_status;
    public $amount;
    public $servicedate;
    //public $created;
    public $usermessage;
    public $service_location_address;
    public $worker_status;
    public $servicetime;
    public $is_email;
    public $workerid;
    private $table9 = 'images';
    public $imagename;
    public $idimage;
    public $imagelocation;
    public $imageurl;
    public $is_corporate;
    public $requestid;
    public $idtransaction;
    public $transactionkey;
    public $transactionresponse_message;
    private $table10 = 'worker_categories';
    private $table11 = 'worker_services';
    private $table12 = 'transactions';
    private $table14 = 'messages';
    public $message;
    public $senderid;
    public $recieverid;
    public $idmessage;
    public $servicerequestid;
    public $subject;
    public $fromdate;
    public $todate;
    private $table15 = 'rewards';
   
    
   



    //constructor

    public function __construct($db){
        $this->conn = $db;
    }


    //Get posts

    public function create(){
        //Create query
        if($this->type=="C")
        {
            $this->type = "U";
            $this->is_corporate = "1";
        }
        $this->created = date("Y-m-d H:i:s");
        $this->createdby = $this->idprofiles;
        $query = 'INSERT INTO ' . $this->table . '
        SET 
        idprofiles = :idprofiles,
        type=:type,
        is_corporate = :is_corporate,
        username=:username,
        password=:password,
        email = :email,
        phone = :phone,
        created = :created,
        createdby = :idprofiles';


        //Prepare statement

        $stmt = $this->conn->prepare($query);

        //clean data

        $this->idprofiles = htmlspecialchars(strip_tags($this->idprofiles));
        $this->type = htmlspecialchars(strip_tags($this->type));
        if($this->type=="C")
        {
            $this->is_corporate = htmlspecialchars(strip_tags($this->is_corporate));
        }
        $this->username = htmlspecialchars(strip_tags($this->username));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->phone = htmlspecialchars(strip_tags($this->phone));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        


        //Bind data


        $stmt->bindParam(':idprofiles', $this->idprofiles);
        $stmt->bindParam(':type', $this->type);
        $stmt->bindParam(':is_corporate', $this->is_corporate);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':phone', $this->phone);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);
       

       

        //Execute query

        if($stmt->execute()){
           
            return $this->idprofiles;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }

    public function addprofileaddress($lastid,$idprofile_address)
    {
        $this->created = date("Y-m-d H:i:s");
        $this->createdby = $lastid;
        $query1 = 'INSERT INTO ' . $this->table1 . '
        SET 
        idprofile_address = :idprofile_address,
        profileid = :profileid,
        address1 = :address1,
        address2 = :address2,
        location = :location,
        sublocality = :sublocality,
        landmark = :landmark,
        city = :city,
        district = :district,
        state = :state,
        created = :created,
        createdby = :createdby';

        $stmt = $this->conn->prepare($query1);

        $this->idprofile_address = htmlspecialchars(strip_tags($idprofile_address));
        $this->profileid = htmlspecialchars(strip_tags($lastid));
        $this->address1 = htmlspecialchars(strip_tags($this->address1));
        $this->address2 = htmlspecialchars(strip_tags($this->address2));
        $this->location = htmlspecialchars(strip_tags($this->location));
        $this->sublocality = htmlspecialchars(strip_tags($this->sublocality));
        $this->landmark = htmlspecialchars(strip_tags($this->landmark));
        $this->city = htmlspecialchars(strip_tags($this->city));
        $this->district = htmlspecialchars(strip_tags($this->district));
        $this->state = htmlspecialchars(strip_tags($this->state));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));
        

        $stmt->bindParam(':idprofile_address', $this->idprofile_address);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':address1', $this->address1);
        $stmt->bindParam(':address2', $this->address2);
        $stmt->bindParam(':location', $this->location);
        $stmt->bindParam(':sublocality', $this->sublocality);
        $stmt->bindParam(':landmark', $this->landmark);
        $stmt->bindParam(':city', $this->city);
        $stmt->bindParam(':district', $this->district);
        $stmt->bindParam(':state', $this->state);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);
        
        if($stmt->execute()){
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
       

    }
    public function addprofiledetails($lastid,$idprofile_detail)
    {
        $this->created = date("Y-m-d H:i:s");
        $this->idprofile_detail = $idprofile_detail;
        $this->createdby = $lastid;
        $this->profileid = $lastid; 
        $query2 = 'INSERT INTO ' . $this->table2 . '
        SET 
        idprofile_detail = :idprofile_detail,
        profileid = :profileid,
        firstname = :firstname,
        lastname = :lastname,
        gender = :gender,
        age = :age,
        created = :created,
        createdby = :createdby';

        $stmt = $this->conn->prepare($query2);

        $this->idprofile_detail = htmlspecialchars(strip_tags($idprofile_detail));
        $this->profileid = htmlspecialchars(strip_tags($lastid));
        $this->firstname = htmlspecialchars(strip_tags($this->firstname));
        $this->lastname = htmlspecialchars(strip_tags($this->lastname));
        $this->gender = htmlspecialchars(strip_tags($this->gender));
        $this->age = htmlspecialchars(strip_tags($this->age));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));
        

        $stmt->bindParam(':idprofile_detail', $this->idprofile_detail);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':firstname', $this->firstname);
        $stmt->bindParam(':lastname', $this->lastname);
        $stmt->bindParam(':gender', $this->gender);
        $stmt->bindParam(':age', $this->age);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);
        
        

        

        if($stmt->execute()){
           
            return true;
        }



       
    }

    public function addworkercategory($lastid,$idworker_category)
    {
        $this->idworker_category = $idworker_category;
        $this->profileid = $lastid; 
        

        $query4 = 'INSERT INTO ' . $this->table3 . '
        SET 
        idworker_category =:idworker_category,
        profileid = :profileid,
        categoryid = :categoryid';

        $stmt = $this->conn->prepare($query4);

        $this->idworker_category = htmlspecialchars(strip_tags($idworker_category));
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        

        $stmt->bindParam(':idworker_category', $this->idworker_category);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':categoryid', $this->categoryid);
        
        if($stmt->execute()){
           
            return true;
        }


    }

    public function addworkerlocations($lastid,$idworker_location)
    {
        $this->profileid = $lastid;
        $this->idworker_location = $idworker_location; 
        $query4 = 'INSERT INTO ' . $this->table4 . '
        SET 
        idworker_location =:idworker_location,
        profileid = :profileid,
        placeid = :placeid';

        $stmt = $this->conn->prepare($query4);

        $this->idworker_location = htmlspecialchars(strip_tags($idworker_location));
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->placeid = htmlspecialchars(strip_tags($this->placeid));
        

        $stmt->bindParam(':idworker_location', $this->idworker_category);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':placeid', $this->placeid);
        
        if($stmt->execute()){
           
            return true;
        }
        
    }

    //  public function getcategoryid($value)
    // {
    //     $this->idservices = $value;
    //     $getservices = 'SELECT * from ' . $this->table7 . '  where idservices = "'.$this->idservices.'"';

    //     // Prepare statement 

    //     $stmt = $this->conn->prepare($getservices);
    //     $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        
    //     $stmt->bindParam(':idservices', $this->idservices);

    //     $stmt->execute();
    //     return $stmt;
    // }

    // public function addworkerservices($lastid,$idworker_services)
    // {
    //     $this->idprofile = $lastid;
    //     $this->idworkservice = $idworker_services; 
    //     // echo $this->idprofile;
    //     // echo $this->idworkservice;
    //     // exit();


    //     $query5 = 'INSERT INTO ' . $this->table5 . '
    //     SET 
    //     idworkservice =:idworkservice,
    //     idprofile = :idprofile,
    //     categoryid = :categoryid,
    //     serviceid = :serviceid,
    //     servicename = :servicename';

    //     $stmt = $this->conn->prepare($query5);
        
    //     $this->idworkservice = htmlspecialchars(strip_tags($this->idworkservice));
    //     $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
    //     $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
    //     $this->serviceid = htmlspecialchars(strip_tags($this->serviceid));
    //     $this->servicename = htmlspecialchars(strip_tags($this->servicename));

    //     //$this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
    //     //$this->serviceid = htmlspecialchars(strip_tags($this->serviceid));

        
    //     $stmt->bindParam(':idworkservice', $this->idworkservice);
    //     $stmt->bindParam(':idprofile', $this->idprofile);
    //     $stmt->bindParam(':categoryid', $this->categoryid);
    //     $stmt->bindParam(':serviceid', $this->serviceid);
    //     $stmt->bindParam(':servicename', $this->servicename);
    //     //$stmt->bindParam(':categoryid', $this->categoryid);
    //     //$stmt->bindParam(':serviceid', $this->serviceid);

        
    //     if($stmt->execute()){
           
    //         return true;
    //     }



    // }

    public function delete(){
        //delete query

        $query = 'UPDATE '. $this->table.' SET deleted_status = "1" where idprofiles = :idprofiles';

        //prepare statement

        $stmt = $this->conn->prepare($query);

        //clean data
        $this->idprofiles = htmlspecialchars(strip_tags($this->idprofiles));

        //Bind Data
        $stmt->bindParam(':idprofiles', $this->idprofiles);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }



    //Update post
    
        //Update query
        public function update(){
            //Update query
             
            $query = 'UPDATE ' . $this->table . '
            SET 
            username = :username,
            email = :email,
            phone = :phone,
            updated = :updated 
            WHERE 
            idprofiles = :idprofiles';
    
            //Prepare statement
            
            $stmt = $this->conn->prepare($query);
    
            //clean data
    
            $this->username = htmlspecialchars(strip_tags($this->username));
           
            $this->email = htmlspecialchars(strip_tags($this->email));
            $this->phone = htmlspecialchars(strip_tags($this->phone));
            $this->idprofiles = htmlspecialchars(strip_tags($this->idprofiles));
            $this->updated = htmlspecialchars(strip_tags($this->updated));
    
    
            //Bind data
    
    
            $stmt->bindParam(':username', $this->username);
            
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':phone', $this->phone);
            $stmt->bindParam(':idprofiles', $this->idprofiles);
            $stmt->bindParam(':updated', $this->updated);
    
            //Execute query
    
            if($stmt->execute()){
              return $this->idprofiles;
            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
    
    
            return false;
        }
        public function updateaddress($result)
        {
            $this->profileid = $result;
            $updatequery = 'UPDATE ' . $this->table1. '
            SET 
            address1 = :address1,
            address2 = :address2,
            location = :location,
            sublocality = :sublocality,
            landmark = :landmark,
            city = :city,
            district = :district,
            state = :state,
            updated = :updated
            WHERE 
            profileid = :profileid';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updatequery);
    
            //clean data
    
            $this->address1 = htmlspecialchars(strip_tags($this->address1));
            $this->address2 = htmlspecialchars(strip_tags($this->address2));
            $this->location = htmlspecialchars(strip_tags($this->location));
            $this->sublocality = htmlspecialchars(strip_tags($this->sublocality));
            $this->landmark = htmlspecialchars(strip_tags($this->landmark));
            $this->city = htmlspecialchars(strip_tags($this->city));
            $this->district = htmlspecialchars(strip_tags($this->district));
            $this->state = htmlspecialchars(strip_tags($this->state));
            $this->profileid = htmlspecialchars(strip_tags($this->profileid));
            $this->updated = htmlspecialchars(strip_tags($this->updated));

    
    
            //Bind data
    
    
            $stmt->bindParam(':address1', $this->address1);
            $stmt->bindParam(':address2', $this->address2);
            $stmt->bindParam(':location', $this->location);
            $stmt->bindParam(':sublocality', $this->sublocality);
            $stmt->bindParam(':landmark', $this->landmark);
            $stmt->bindParam(':city', $this->city);
            $stmt->bindParam(':district', $this->district);
            $stmt->bindParam(':state', $this->state);
            $stmt->bindParam(':profileid', $this->profileid);
            $stmt->bindParam(':updated', $this->updated);
    
            //Execute query
    
            if($stmt->execute()){
              return $this->profileid;
                
            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
    
    
            return false;


        }
        public function updateprofile($idprofile)
        {

            $this->profileid = $idprofile;
            $updatequery = 'UPDATE ' . $this->table2. '
            SET 
            firstname = :firstname,
            lastname = :lastname,
            gender = :gender,
            age = :age,
            updated = :updated
            WHERE 
            profileid = :profileid';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updatequery);
    
            //clean data
    
            $this->firstname = htmlspecialchars(strip_tags($this->firstname));
            $this->lastname = htmlspecialchars(strip_tags($this->lastname));
            $this->gender = htmlspecialchars(strip_tags($this->gender));
            $this->age = htmlspecialchars(strip_tags($this->age));
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->profileid = htmlspecialchars(strip_tags($this->profileid));
           
    
    
            //Bind data
    
    
            $stmt->bindParam(':firstname', $this->firstname);
            $stmt->bindParam(':lastname', $this->lastname);
            $stmt->bindParam(':gender', $this->gender);
            $stmt->bindParam(':age', $this->age);
            $stmt->bindParam(':updated', $this->updated);
            $stmt->bindParam(':profileid', $this->profileid);
    
            //Execute query
    
            if($stmt->execute()){

              return  $stmt;
                
            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
    
    
            return false;

            


        }
    
    

    public function addlastid($pid,$subname)
    {
        
        $this->pid = $pid; 
        $this->subname = $subname; 
        $newquery = 'INSERT INTO ' .$this->table1. ' 
        SET 
        subname = :subname,
        pid = :pid';
        $stmt = $this->conn->prepare($newquery);
        $stmt->bindParam(':pid', $this->pid);
        $stmt->bindParam(':subname', $this->subname);
        $this->pid = htmlspecialchars(strip_tags($this->pid));
        $this->subname = htmlspecialchars(strip_tags($this->subname));
        if($stmt->execute()){
            return true;
        }
       
    }

    //Delete Post

    
    public function checkusername()
    {
        $query3 = "SELECT * FROM  $this->table WHERE username =:username";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query3);
     
        // bind id of product to be updated
        $stmt->bindParam(':username', $this->username);
     
        if($stmt->execute())
        {
            return $stmt;
        }
       
    }
    public function checkcategoryname()
    {
        $checkcategory = "SELECT * FROM  $this->table6 WHERE categoryname =:categoryname";
     
        // prepare query statement
        $stmt = $this->conn->prepare($checkcategory);
     
        // bind id of product to be updated
        $stmt->bindParam(':categoryname', $this->categoryname);
     
        if($stmt->execute())
        {
            return $stmt;
        }
       
    }
    public function checkservicename()
    {
        $checkservices = "SELECT * FROM  $this->table7 WHERE servicename =:servicename";
     
        // prepare query statement
        $stmt = $this->conn->prepare($checkservices);
     
        // bind id of product to be updated
        $stmt->bindParam(':servicename', $this->servicename);
     
        if($stmt->execute())
        {
            return $stmt;
        }
       
    }
    public function login()
    {
        $this->email = $this->username;
        $this->username = $this->email;
        $this->type = "A";
        $query = 'SELECT * FROM profiles WHERE  password = :password and type=:type and (username = :username OR email =:email)';

        //prepare statement

        $stmt = $this->conn->prepare($query);

        //clean data
        $this->username = htmlspecialchars(strip_tags($this->username));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->type = htmlspecialchars(strip_tags($this->type));


        //Bind Data
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':type', $this->type);


         //Execute query

         if($stmt->execute()){
            $no=$stmt->rowCount();
            
            if($no>0)
            {
            return $stmt;
            }
            else
            {
            return false;
            }
            
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
       
    }
    public function read(){
        //Create query
        $query4 = 'SELECT ps.username,ps.password,ps.type,
    ps.email,pf.firstname,pf.lastname,
        ps.phone,pd.address1,pd.address2,pd.location,pd.sublocality,pd.landmark,pd.city,pd.district,pd.state,pd.profileid
    FROM 
        profiles as ps
    LEFT JOIN  
        profile_address as pd  ON ps.idprofiles = pd.profileid 
    LEFT JOIN profile_details as pf ON ps.idprofiles = pf.profileid
         where deleted="0"  
    ORDER BY email DESC ';

        // Prepare statement 

        $stmt = $this->conn->prepare($query4);

        //Bind ID 
        
        // $stmt->bindParam(':idprofiles', $this->idprofiles);
        // $stmt->bindParam(':username', $this->username);
        // $stmt->bindParam(':email', $this->email);
        // $stmt->bindParam(':phone', $this->phone);
        // $stmt->bindParam(':idprofile_address', $this->idprofile_address);
        // $stmt->bindParam(':profileid', $this->profileid);
        // $stmt->bindParam(':address1', $this->address1);


        //Execute query
        $stmt->execute();
        
        return $stmt;
        error_log("row => " . json_encode($row));
        // Set properties

        if($row){
            $this->title = $row['title'];
            $this->body = $row['body'];
            $this->author = $row['author'];
            $this->category_id = $row['category_id'];
        }
        else{
            return false;
        }
    }

    public function readwithpaging($lim,$off){
        //Create query
       $this->lim = $lim;
       $this->off = $off;
        $query4 = 'SELECT username,password, 
    email,
        phone,address1,address2,location,sublocality,landmark,city,district,state,profileid
    FROM 
        profiles
    INNER  JOIN
        profile_address  ON profiles.idprofiles = profile_address.profileid 
    ORDER BY email LIMIT '.$this->off.','.$this->lim.'';

        // Prepare statement 

        $stmt = $this->conn->prepare($query4);

        //Bind ID 
        
        $stmt->bindParam(':lim',$this->lim);
        $stmt->bindParam(':off',$this->off);
        // $stmt->bindParam(':email', $this->email);
        // $stmt->bindParam(':phone', $this->phone);
        // $stmt->bindParam(':idprofile_address', $this->idprofile_address);
        // $stmt->bindParam(':profileid', $this->profileid);
        // $stmt->bindParam(':address1', $this->address1);


        //Execute query
        $stmt->execute();
        
        return $stmt;
        error_log("row => " . json_encode($row));
        // Set properties

        if($row){
            $this->title = $row['title'];
            $this->body = $row['body'];
            $this->author = $row['author'];
            $this->category_id = $row['category_id'];
        }
        else{
            return false;
        }
    }

    public function read_single()
    {
        //Create query
              
            // query to read single record
            $readsinglequery = 'SELECT ps.idprofiles,ps.type,ps.username,ps.password,ps.email,ps.phone,ps.status,ps.created,pd.address1,pd.sublocality,pd.address2,pd.city,pd.landmark,pd.location,pd.district,pd.state,pf.firstname,pf.lastname,pf.age,pf.gender,img.imagename,img.idimage, 
            img.imagelocation,img.imageurl FROM 
                profiles as ps
            LEFT  JOIN
                profile_address as pd   ON ps.idprofiles = pd.profileid LEFT JOIN profile_details as pf  on ps.idprofiles = pf.profileid LEFT JOIN images as img  ON ps.idprofiles = img.idprofile
                WHERE idprofiles = "'.$this->id.'" ';
         
         $stmt = $this->conn->prepare($readsinglequery);
            // prepare query statement
            $stmt->bindParam(':profileid', $this->id);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            // set values to object properties
            $this->idprofiles = $row['idprofiles'];
            $this->username = $row['username'];
            $this->password = $row['password'];
            $this->firstname = $row['firstname'];
            $this->lastname = $row['lastname'];
            $this->age = $row['age'];
            $this->email = $row['email'];
            $this->address1 = $row['address1'];
            $this->address2 = $row['address2'];
            $this->phone = $row['phone'];
            $this->gender = $row['gender'];
            $this->location = $row['location'];
            $this->sublocality = $row['sublocality'];
            $this->landmark = $row['landmark'];
            $this->city = $row['city'];
            $this->district = $row['district'];
            $this->state = $row['state'];
            $this->created  = $row["created"];
            $this->status  = $row["status"];
            $this->imagename = $row["imagename"];
            $this->idimage = $row["idimage"];
            $this->imagelocation = $row["imagelocation"];
            $this->imageurl = $row["imageurl"];
            return true;
           }
            
        
    }

    public function searchuser()
    {
        $username = "$this->username%";
        
        $readsinglequery = 'SELECT *
            FROM 
                profiles
            INNER  JOIN
                profile_address  ON profiles.idprofiles = profile_address.profileid
                INNER JOIN profile_details ON profile_address.profileid = profile_details.profileid
                INNER JOIN worker_categories ON  profile_details.profileid = worker_categories.profileid
                WHERE username LIKE "'.$username.'"';
         
            $stmt = $this->conn->prepare($readsinglequery);
            // prepare query statement
            $stmt->bindParam(':username', $this->username);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            return $stmt;
           }
    }

    public function checkcurrentpassword()
    {
        $this->type = "A";
        $checkquery = "SELECT * FROM  $this->table WHERE username =:username and password =:password and type=:type";
     
        // prepare query statement
        $stmt = $this->conn->prepare($checkquery);
     
        // bind id of product to be updated
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':type', $this->type);
        
        if($stmt->execute())
        {
            $no = $stmt->rowCount();
            if($no>0)
            {
                return $no;
            }
            else
            {
                return false;
            }
        }
        
    }

    public function changeadminpassword()
    {
       
        if($this->newpassword=="$this->retypenewpassword")
        {
            $this->type = "A";
            $this->password = $this->newpassword;
            $updatepassword = 'UPDATE ' . $this->table . '
            SET 
            password = :password
            WHERE 
            username = :username and 
            type = :type';
        }
        $stmt = $this->conn->prepare($updatepassword);
                $stmt->bindParam(':username', $this->username);
                $stmt->bindParam(':password', $this->password);
                $stmt->bindParam(':type', $this->type);
        if($stmt->execute())
        {
            return true;
        }
    }

    public function addcategory()
    { 
        $insertcategory = 'INSERT INTO ' . $this->table6 . '
        SET 
        applicationid = :applicationid,
        idcategory = :idcategory,
        categoryname = :categoryname,
        created = :created,
        createdby = :createdby';
        $stmt = $this->conn->prepare($insertcategory);
         
         
        //clean data
        $this->applicationid = htmlspecialchars(strip_tags($this->applicationid));
        $this->idcategory = htmlspecialchars(strip_tags($this->idcategory));
        $this->categoryname = htmlspecialchars(strip_tags($this->categoryname));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':applicationid', $this->applicationid);
        $stmt->bindParam(':idcategory', $this->idcategory);
        $stmt->bindParam(':categoryname', $this->categoryname);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);

        if($stmt->execute()){
           
            return $this->idcategory;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
    public function readcategory()
    {
       

            //Create query
            $readcategory = 'SELECT * FROM categories';
    
            // Prepare statement 
    
            $stmt = $this->conn->prepare($readcategory);
    
            //Bind ID 
            
            // $stmt->bindParam(':idprofiles', $this->idprofiles);
            // $stmt->bindParam(':username', $this->username);
            // $stmt->bindParam(':email', $this->email);
            // $stmt->bindParam(':phone', $this->phone);
            // $stmt->bindParam(':idprofile_address', $this->idprofile_address);
            // $stmt->bindParam(':profileid', $this->profileid);
            // $stmt->bindParam(':address1', $this->address1);
    
    
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));
            // Set properties
    
            // if($row){
            //     $this->title = $row['title'];
            //     $this->body = $row['body'];
            //     $this->author = $row['author'];
            //     $this->category_id = $row['category_id'];
            // }
            // else{
            //     return false;
            // }
    }
    
    //Create query
    // query to read single record
    public function read_singlecategory()
    {
        
            $readsinglequery = 'SELECT *
            FROM 
            categories
            WHERE idcategory = "'.$this->id.'" ';
            $stmt = $this->conn->prepare($readsinglequery);
            // prepare query statement
            $stmt->bindParam(':profileid', $this->id);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            // set values to object properties
            $this->idcategory = $row['idcategory'];
            $this->categoryname = $row['categoryname'];
            return true;
           }
    }
    public function updatecategory()
    {
            
           
            
            $updatecategory = 'UPDATE ' . $this->table6. '
            SET 
            categoryname = :categoryname,
            updated = :updated,
            updatedby = :updatedby 
            WHERE 
            idcategory = :idcategory';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updatecategory);
    
            //clean data
    
            $this->categoryname = htmlspecialchars(strip_tags($this->categoryname));
            $this->idcategory = htmlspecialchars(strip_tags($this->idcategory));
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->updatedby = htmlspecialchars(strip_tags($this->updatedby));

           
    
    
            //Bind data
    
    
            $stmt->bindParam(':categoryname', $this->categoryname);
            $stmt->bindParam(':idcategory', $this->idcategory);
            $stmt->bindParam(':updated', $this->updated);
            $stmt->bindParam(':updatedby', $this->updatedby);
    
            //Execute query
    
            if($stmt->execute()){

              return  $stmt;
                
            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
    
    
            return false;

    }
    public function deletecategory(){
        //delete query
        
        $deletecategory = 'DELETE FROM ' . $this->table6 . ' WHERE idcategory = :idcategory';

        //prepare statement

        $stmt = $this->conn->prepare($deletecategory);

        //clean data
        $this->idcategory = htmlspecialchars(strip_tags($this->idcategory));

        //Bind Data
        $stmt->bindParam(':idcategory', $this->idcategory);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }
    public function addservices()
    {
        $categoryid = $this->catid;
        $insertservice = 'INSERT INTO ' . $this->table7 . '
        SET 
        idservices = :idservices,
        servicename = :servicename,
        categoryid = :categoryid,
        createdby = :createdby,
        created = :created';
        $stmt = $this->conn->prepare($insertservice);
         
         
        //clean data
        $this->idservices = htmlspecialchars(strip_tags($this->idservices));
        $this->servicename = htmlspecialchars(strip_tags($this->servicename));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));
        $this->created = htmlspecialchars(strip_tags($this->created));

        $stmt->bindParam(':idservices', $this->idservices);
        $stmt->bindParam(':servicename', $this->servicename);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':createdby', $this->createdby);
        $stmt->bindParam(':created', $this->created);

        if($stmt->execute()){
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
    public function getservices()
    {
       
        $getservices = 'SELECT idservices,servicename,categoryid,created from ' . $this->table7 . '  where categoryid = "'.$this->categoryid.'"';

        // Prepare statement 

        $stmt = $this->conn->prepare($getservices);
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));

        $stmt->bindParam(':categoryid', $this->categoryid);
         
        
        $stmt->execute();
        
        return $stmt;
    }

    public function read_singleservice($id)
    {
            
            $this->idservices = $id;
            $readsingleservice = 'SELECT *
            FROM 
            services
            WHERE idservices = "'.$this->idservices.'" ';
            $stmt = $this->conn->prepare($readsingleservice);
            // prepare query statement
            $stmt->bindParam(':idservices', $this->idservices);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
           
            // set values to object properties
            $this->idservices = $row['idservices'];
            $this->servicename = $row['servicename'];
            return true;
           }

    }
    public function updateservices($id,$servicename)
    {
       
        $this->servicename= $servicename;
        $this->idservices = $id;;
        $this->updated = date('Y-m-d H:i:s');
            // $this->updated = $this->updateservices;
            $updateservices = 'UPDATE services
            SET 
            servicename=:servicename,
            updated=:updated 
            WHERE 
            idservices=:idservices';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updateservices);
    
            //clean data
            $this->servicename = htmlspecialchars(strip_tags($this->servicename));
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->idservices = htmlspecialchars(strip_tags($this->idservices));
            
            //Bind data
    
            
            $stmt->bindParam(':servicename', $this->servicename);
            $stmt->bindParam(':updated', $this->updated);
            $stmt->bindParam(':idservices', $this->idservices);
    
            //Execute query
             
            if($stmt->execute()){

              return true;

            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
            return false;
    }

    public function deleteservices(){
        //delete query
        
        $deleteservices = 'DELETE FROM ' . $this->table7 . ' WHERE idservices = :idservices';

        //prepare statement

        $stmt = $this->conn->prepare($deleteservices);

        //clean data
        $this->idservices = htmlspecialchars(strip_tags($this->idservices));

        //Bind Data
        $stmt->bindParam(':idservices', $this->idservices);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }
    public function checkservices()
    {
        
        $checkservice = 'SELECT *
        FROM 
        services
        WHERE servicename = "'.$this->servicename.'" ';
        $stmt = $this->conn->prepare($checkservice);
        // prepare query statement
        $stmt->bindParam(':servicename', $this->servicename);
       
        // bind id of product to be updated
        // execute query
       if($stmt->execute())
       {
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
       
        // set values to object properties
        $this->idservices = $row['idservices'];
        $this->servicename = $row['servicename'];
        return $this->servicename;
       }

    }
    public function addservicerequest()
    {
        $this->service_location = $this->address;
        $servicequery = 'INSERT INTO ' . $this->table8. '
        SET 
        idservice_request = :idservice_request,
        userid = :userid,
        service = :service,
        categoryid = :categoryid,
        usermessage = :usermessage,
        servicedistrict =:servicedistrict,
        service_location = :service_location,
        payment_type = :payment_type,
        payment_status = :payment_status,
        servicedate = :servicedate,
        createdon = :createdon,
        createdby = :createdby';


        //Prepare statement

        $stmt = $this->conn->prepare($servicequery);

        //clean data

        $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));
        $this->userid = htmlspecialchars(strip_tags($this->userid));
        $this->service = htmlspecialchars(strip_tags($this->service));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->usermessage = htmlspecialchars(strip_tags($this->usermessage));
        $this->servicedistrict = htmlspecialchars(strip_tags($this->servicedistrict));
        $this->service_location = htmlspecialchars(strip_tags($this->service_location));
        $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
        $this->payment_status = htmlspecialchars(strip_tags($this->payment_status));
        //$this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
        $this->createdon = htmlspecialchars(strip_tags($this->createdon));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        // $this->service_location_address = htmlspecialchars(strip_tags($this->service_location_address));
        // $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
        // $this->amount = htmlspecialchars(strip_tags($this->amount));
        // $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
        // $this->servicetime = htmlspecialchars(strip_tags($this->servicetime));
        // $this->created = htmlspecialchars(strip_tags($this->created));

        


        //Bind data


        $stmt->bindParam(':idservice_request', $this->idservice_request);
        $stmt->bindParam(':userid', $this->userid);
        $stmt->bindParam(':service', $this->service);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':usermessage', $this->usermessage);
        $stmt->bindParam(':servicedistrict', $this->servicedistrict);
        $stmt->bindParam(':service_location', $this->service_location);
        $stmt->bindParam(':payment_status',$this->payment_status);
        $stmt->bindParam(':payment_type',$this->payment_type);
        //$stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':servicedate', $this->servicedate);
        $stmt->bindParam(':createdon', $this->createdon);
        $stmt->bindParam(':createdby', $this->createdby);
        // $stmt->bindParam(':usermessage', $this->usermessage);
        // $stmt->bindParam(':service_location', $this->service_location);
        // $stmt->bindParam(':service_location_address', $this->service_location_address);
        //$stmt->bindParam(':payment_type',$this->payment_type);
        // $stmt->bindParam(':amount', $this->amount);
        // $stmt->bindParam(':servicedate', $this->servicedate);
        // $stmt->bindParam(':servicetime', $this->servicetime);
        // $stmt->bindParam(':created', $this->created);
       

       

        //Execute query

        if($stmt->execute())
        {
        
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
    public function readrequest()
    {
        $readservicerequest = "select * from service_requests LEFT join categories on service_requests.categoryid = categories.idcategory LEFT join services on service_requests.service=services.idservices LEFT JOIN profiles on service_requests.userid = profiles.idprofiles LEFT join profile_details on service_requests.userid = profile_details.profileid order by createdon DESC";
        $stmt = $this->conn->prepare( $readservicerequest);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }
	 public function deleteservicerequest(){
        //delete query
        
        $deleteservices = 'DELETE FROM ' . $this->table8 . ' WHERE idservice_request = :idservice_request';

        //prepare statement

        $stmt = $this->conn->prepare($deleteservices);

        //clean data
        $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));

        //Bind Data
        $stmt->bindParam(':idservice_request', $this->idservice_request);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }
	
	public function read_singlerequest()
	{
		
		$readsinglerequest = 'SELECT * FROM service_requests LEFT JOIN categories on service_requests.categoryid = categories.idcategory LEFT JOIN services on service_requests.service =services.idservices where idservice_request= "'.$this->idservice_request.'"';
         
         $stmt = $this->conn->prepare($readsinglerequest);
            // prepare query statement
            $stmt->bindParam(':idservice_request', $this->idservice_request);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
              
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            // set values to object properties
            $this->idservice_request = $row['idservice_request'];
            $this->userid = $row['userid'];
            $this->usermessage = $row['usermessage'];
            $this->service_location = $row['service_location'];
            $this->service_status = $row['service_status'];
            $this->servicedate = $row['servicedate'];
            $this->worker_status = $row['worker_status'];
            $this->payment_type = $row['payment_type'];
            $this->payment_status = $row['payment_status'];
            $this->is_email = $row['is_email'];
            $this->servicetime = $row['servicetime'];
            $this->servicedistrict = $row['servicedistrict'];
            $this->amount = $row['amount'];
            $this->categoryid = $row["categoryid"];
            $this->service = $row["service"];
            $this->servicename = $row["servicename"];
            $this->categoryname = $row["categoryname"];
            return true;
           }
		
		
	}
	public function updateservicerequest()
	{
            $this->updated = date('Y-m-d H:i:s');
            $this->updatedby = $this->adminid;
        
            // $this->updated = $this->updateservices;
            $updateservicerequest = 'UPDATE '.$this->table8.'
            SET 
            workerid = :workerid,
            service = :service,
            categoryid = :categoryid,
            usermessage = :usermessage,
            servicedistrict = :servicedistrict,
			service_location = :service_location,
            servicedate = :servicedate,
            worker_status = :worker_status,
            is_email = :is_email,
            payment_type = :payment_type,
            payment_status = :payment_status,
            amount = :amount,
            updated = :updated,
            updatedby = :updatedby 
            WHERE 
            idservice_request = :idservice_request';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updateservicerequest);
    
            //clean data
            $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));
            if($this->workerid!=null)
            {
            $this->workerid = htmlspecialchars(strip_tags($this->workerid));
            }
            $this->service = htmlspecialchars(strip_tags($this->service));
            $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
            $this->usermessage = htmlspecialchars(strip_tags($this->usermessage));
            $this->servicedistrict = htmlspecialchars(strip_tags($this->servicedistrict));
			$this->service_location = htmlspecialchars(strip_tags($this->service_location));
            $this->servicedate = htmlspecialchars(strip_tags($this->servicedate));
            $this->worker_status = htmlspecialchars(strip_tags($this->worker_status));
            $this->is_email = htmlspecialchars(strip_tags($this->is_email));
            $this->payment_type = htmlspecialchars(strip_tags($this->payment_type));
            $this->payment_status = htmlspecialchars(strip_tags($this->payment_status));
            $this->amount = htmlspecialchars(strip_tags($this->amount));
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->updatedby = htmlspecialchars(strip_tags($this->updatedby));
			
            $stmt->bindParam(':idservice_request', $this->idservice_request);
            $stmt->bindParam(':workerid', $this->workerid);
            $stmt->bindParam(':service', $this->service);
            $stmt->bindParam(':categoryid', $this->categoryid);
            $stmt->bindParam(':usermessage', $this->usermessage);
            $stmt->bindParam(':servicedistrict', $this->servicedistrict);
			$stmt->bindParam(':service_location', $this->service_location);
            $stmt->bindParam(':servicedate', $this->servicedate);
            $stmt->bindParam(':worker_status', $this->worker_status);
            $stmt->bindParam(':is_email', $this->is_email);
            $stmt->bindParam(':payment_type', $this->payment_type);
            $stmt->bindParam(':payment_status', $this->payment_status);
            $stmt->bindParam(':amount', $this->amount);
            $stmt->bindParam(':updated', $this->updated);
            $stmt->bindParam(':updatedby', $this->updatedby);
			
			
			 if($stmt->execute()){

              return true;

            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
            return false;
		
		
    }

    
    

    
    public function getworker()
    {
        $selectworker = "select firstname,idprofiles from profiles INNER JOIN profile_details on profiles.idprofiles = profile_details.profileid where type = 'W'";
        $stmt = $this->conn->prepare($selectworker);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }
    public function adduserimage($idimage,$idprofiles)
    {
        $this->idprofile = $idprofiles;
        $this->idimage = $idimage;
        $this->created = date("Y-m-d H:i:s");
        $imagequery = 'INSERT INTO ' . $this->table9 . '
        SET 
        idimage = :idimage,
        idprofile =:idprofile,
        imagename=:imagename,
        created = :created';


        //Prepare statement

        $stmt = $this->conn->prepare($imagequery);

        //clean data

        $this->idimage = htmlspecialchars(strip_tags($this->idimage));
        $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
        $this->imagename = htmlspecialchars(strip_tags($this->imagename));
        $this->created = htmlspecialchars(strip_tags($this->created));

        //Bind data
        $stmt->bindParam(':idimage', $this->idimage);
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':imagename', $this->imagename);
        $stmt->bindParam(':created', $this->created);
       

       

        //Execute query

        if($stmt->execute()){
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
    public function updateuserimage($idimage,$idprofile)
    {
        $this->updated = date('Y-m-d H:i:s');
        $this->idimage= $idimage;
        $this->idprofile = $idprofile;
       
    
            // $this->updated = $this->updateservices;
            $updateuserimage = 'UPDATE ' . $this->table9. '
            SET 
            imagename = :imagename,
            updated = :updated 
            WHERE 
            idimage = :idimage and idprofile = :idprofile';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updateuserimage);
    
            //clean data
            $this->idimage = htmlspecialchars(strip_tags($this->idimage));
            $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
            $this->imagename = htmlspecialchars(strip_tags($this->imagename));
			$this->updated = htmlspecialchars(strip_tags($this->updated));
			
            $stmt->bindParam(':idimage', $this->idimage);
            $stmt->bindParam(':idprofile', $this->idprofile);
            $stmt->bindParam(':imagename', $this->imagename);
			$stmt->bindParam(':updated', $this->updated);
			
			
			 if($stmt->execute()){

              return true;

            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
            return false;


    }
    
    public function updateworkercategory($value,$idprofiles)
    {
        $idworker_category = str_shuffle("0123456789");
        $this->idworker_category = $idworker_category;
        $this->profileid = $idprofiles;
        $this->categoryid= $value;
        $this->created = date('Y-m-d H:i:s');
        $this->createdby = $idprofiles;
        $updatecategory = 'INSERT INTO ' . $this->table5 . '
        SET 
        idworker_category = :idworker_category,
        profileid = :profileid,
        categoryid = :categoryid,
        created = :created,
        createdby = :createdby';
        $stmt = $this->conn->prepare($updatecategory);
         
         
        //clean data
        $this->idworker_category = htmlspecialchars(strip_tags($this->idworker_category));
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':idworker_category', $this->idworker_category);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);
       
        
        if($stmt->execute())
        {
           
            return $this->idworker_category;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }

    public function getworkercategory($idworkercategory)
	{
        
        $this->profileid = $idworkercategory;
		$readsinglerequest = 'SELECT *
            FROM 
                worker_categories
                WHERE profileid = "'.$this->profileid.'"';
         
            $stmt = $this->conn->prepare($readsinglerequest);
            // prepare query statement
            $stmt->bindParam(':idworker_category', $this->idworker_category);

            // bind id of product to be updated
         
            // execute query
          $stmt->execute();
        
        return $stmt;
		
		
    }

    public function insertworkercategory($profileid,$adminid,$value){
        //delete query
       
        $this->idworker_category = str_shuffle("0123456789");
        $this->profileid = $profileid;
        $this->categoryid = $value;
        $this->created = date('Y-m-d H:i:s');
        $this->createdby = $adminid;
        $updatecategory = 'INSERT INTO ' . $this->table10. '
        SET 
        idworker_category = :idworker_category,
        categoryid = :categoryid,
        profileid = :profileid,
        created = :created,
        createdby = :createdby';

        //prepare statement

        $stmt = $this->conn->prepare($updatecategory);

        //clean data
        $this->idworker_category = htmlspecialchars(strip_tags($this->idworker_category));
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        //Bind Data
        $stmt->bindParam(':idworker_category', $this->idworker_category);
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);


         //Execute query

         if($stmt->execute())
         {
            return true;
         }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }

    public function deleteworkercategory($id,$catid){
        //delete query
        $this->profileid = $id;
        $this->categoryid = $catid;
        
        $deletecategory = 'DELETE FROM ' . $this->table10. ' WHERE categoryid = :categoryid and profileid=:profileid';

        //prepare statement

        $stmt = $this->conn->prepare($deletecategory);

        //clean data
        $this->profileid = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));

        //Bind Data
        $stmt->bindParam(':profileid', $this->profileid);
        $stmt->bindParam(':categoryid', $this->categoryid);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }


    public function selectedworker_categories()
    {
        //Create query
              
            // query to read single record
            $readselectedworker_category = 'SELECT *
            FROM worker_categories
            LEFT  JOIN
               categories ON worker_categories.categoryid = categories.idcategory where profileid = "'.$this->id.'" ';
         
         $stmt = $this->conn->prepare($readselectedworker_category);
            // prepare query statement
            $stmt->bindParam(':profileid', $this->id);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
             return $stmt;
           }
            
        
    }
    //get workers
    public function readworkers(){
        //Create query
        $this->type = 'W';
        $query4 = 'SELECT username,password, 
    email,
        phone,address1,address2,location,sublocality,landmark,city,district,state,profileid
    FROM 
        profiles
    LEFT  JOIN
        profile_address  ON profiles.idprofiles = profile_address.profileid where type = "'.$this->type.'" ';

        // Prepare statement 

        $stmt = $this->conn->prepare($query4);

        //Bind ID 
        
        // $stmt->bindParam(':idprofiles', $this->idprofiles);
        // $stmt->bindParam(':username', $this->username);
        // $stmt->bindParam(':email', $this->email);
        // $stmt->bindParam(':phone', $this->phone);
        // $stmt->bindParam(':idprofile_address', $this->idprofile_address);
        // $stmt->bindParam(':profileid', $this->profileid);
        // $stmt->bindParam(':address1', $this->address1);


        //Execute query
        $stmt->execute();
        
        return $stmt;
        error_log("row => " . json_encode($row));
        // Set properties

        if($row){
            $this->title = $row['title'];
            $this->body = $row['body'];
            $this->author = $row['author'];
            $this->category_id = $row['category_id'];
        }
        else{
            return false;
        }
    }
    public function readworkercategory($id)
    {
        $this->profileid = $id;
        $readcategory ='SELECT * FROM categories WHERE NOT EXISTS(SELECT * FROM worker_categories WHERE categories.idcategory = worker_categories.categoryid and profileid = :profileid)';
       
        $stmt = $this->conn->prepare($readcategory);
            // Prepare statement 
        $stmt->bindParam(':profileid', $this->profileid);
        
    
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));

    }

    //delete worker services based on category
    public function deleteworkerservices($id,$catid){
        //delete query
        $this->idprofile = $id;
        $this->categoryid = $catid;
        
        $deletecategory = 'DELETE FROM ' . $this->table11 . ' WHERE categoryid = :categoryid and idprofile=:idprofile';

        //prepare statement

        $stmt = $this->conn->prepare($deletecategory);

        //clean data
        $this->idprofile = htmlspecialchars(strip_tags($this->profileid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));

        //Bind Data
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':categoryid', $this->categoryid);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }

//delete only worker services from 4th column
    public function deleteonlyworkerservices($id,$serviceid){
        //delete query
        $this->idprofile = $id;
        $this->serviceid = $serviceid;
        
        $deletecategory = 'DELETE FROM ' . $this->table11 . ' WHERE serviceid = :serviceid and idprofile=:idprofile';

        //prepare statement

        $stmt = $this->conn->prepare($deletecategory);

        //clean data
        $this->idprofile = htmlspecialchars(strip_tags($this->profileid));
        $this->serviceid = htmlspecialchars(strip_tags($this->serviceid));

        //Bind Data
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':serviceid', $this->serviceid);


         //Execute query

         if($stmt->execute()){
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }

    

    public function readservices($id)
    {
       
            $this->idprofile = $id;
            //Create query
            $readservices = 'SELECT * FROM services WHERE NOT EXISTS(SELECT * FROM worker_services WHERE services.idservices=worker_services.serviceid and idprofile=:idprofile)';
    
            // Prepare statement 
    
            $stmt = $this->conn->prepare($readservices);
    
            //Bind ID 
            $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
            $stmt->bindParam(':idprofile', $this->idprofile);
            // $stmt->bindParam(':username', $this->username);
            // $stmt->bindParam(':email', $this->email);
            // $stmt->bindParam(':phone', $this->phone);
            // $stmt->bindParam(':idprofile_address', $this->idprofile_address);
            // $stmt->bindParam(':profileid', $this->profileid);
            // $stmt->bindParam(':address1', $this->address1);
    
    
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));
            // Set properties
    
            // if($row){
            //     $this->title = $row['title'];
            //     $this->body = $row['body'];
            //     $this->author = $row['author'];
            //     $this->category_id = $row['category_id'];
            // }
            // else{
            //     return false;
            // }
    }



    //Filling 4th column in add worker services
    public function addworkerservices($profileid,$adminid,$servicename,$categoryid,$amount,$value){
        
       
        $this->idworkservice = str_shuffle("0123456789");
        $this->idprofile = $profileid;
        $this->serviceid = $value;
        $this->amount = $amount;
        $this->categoryid = $categoryid;
        $this->servicename = $servicename;
        $this->created = date('Y-m-d H:i:s');
        $this->createdby = $adminid;
        
        $addworkerservices = 'INSERT INTO ' . $this->table11. '
        SET 
        idworkservice =:idworkservice,
        idprofile =:idprofile,
        serviceid =:serviceid,
        categoryid =:categoryid,
        amount =:amount,
        servicename =:servicename,
        created =:created,
        createdby =:createdby';

        //prepare statement

        $stmt = $this->conn->prepare($addworkerservices);

        //clean data
        $this->idworkservice = htmlspecialchars(strip_tags($this->idworkservice));
        $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
        $this->serviceid = htmlspecialchars(strip_tags($this->serviceid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->servicename = htmlspecialchars(strip_tags($this->servicename));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        //Bind Data
        $stmt->bindParam(':idworkservice', $this->idworkservice);
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':serviceid', $this->serviceid);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':servicename', $this->servicename);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);


         //Execute query

         if($stmt->execute())
         {
            return true;
         }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;
    }
    public function getcategoryid($value)
    {
        $this->idservices = $value;
        $getservices = 'SELECT * from ' . $this->table7 . '  where idservices = "'.$this->idservices.'"';

        // Prepare statement 

        $stmt = $this->conn->prepare($getservices);
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        
        $stmt->bindParam(':idservices', $this->idservices);

        $stmt->execute();
        return $stmt;
    }
      //Filling 4th column in add worker services

    public function getworkerservices($id)
    {
     
        $this->idprofile = $id;
       
        $readworkerservices ='SELECT * FROM worker_services where idprofile=:idprofile';
       
        $stmt = $this->conn->prepare($readworkerservices);
            // Prepare statement 
        $stmt->bindParam(':idprofile',$this->idprofile);
        
            //Execute query
           if($stmt->execute())
            {
            return $stmt;
            }
            error_log("row => " . json_encode($row));

    }
    public function addworkerimage($idimage,$idprofiles)
    {
        $this->idprofile = $idprofiles;
        $this->idimage = $idimage;
        $this->created = date("Y-m-d H:i:s");
        $this->createdby = $idprofiles;
       
        $imagequery = 'INSERT INTO ' . $this->table9 . '
        SET 
        idimage =:idimage,
        idprofile =:idprofile,
        imagename=:imagename,
        created =:created,
        createdby =:createdby';


        //Prepare statement

        $stmt = $this->conn->prepare($imagequery);

        //clean data

        $this->idimage = htmlspecialchars(strip_tags($this->idimage));
        $this->idprofile = htmlspecialchars(strip_tags($this->idprofile));
        $this->imagename = htmlspecialchars(strip_tags($this->imagename));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        //Bind data
        $stmt->bindParam(':idimage', $this->idimage);
        $stmt->bindParam(':idprofile', $this->idprofile);
        $stmt->bindParam(':imagename', $this->imagename);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);
        
       

        //Execute query

        if($stmt->execute())
        {
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }
     public function checkemail()
    {
        $checkemail = "SELECT * FROM  $this->table WHERE email =:email";
     
        // prepare query statement
        $stmt = $this->conn->prepare($checkemail);
     
        // bind id of product to be updated
        $stmt->bindParam(':email', $this->email);
     
        if($stmt->execute())
        {
            return $stmt;
        }
       
    }

    public function getrequesteduserdetails()
    {
        //Create query
        
              
            // query to read single record
            $readsinglequery = 'SELECT *
            FROM service_requests LEFT JOIN profiles on service_requests.userid = profiles.idprofiles  LEFT JOIN profile_address on service_requests.userid = profiles.idprofiles LEFT JOIN
            profile_details on service_requests.userid = profile_details.profileid    WHERE idservice_request = "'.$this->id.'" ';
         
         $stmt = $this->conn->prepare($readsinglequery);
            // prepare query statement
            $stmt->bindParam(':profileid', $this->id);
           
            // bind id of product to be updated
         
            // execute query
           if($stmt->execute())
           {
               
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
           
            // set values to object properties
            $this->idprofiles = $row['idprofiles'];
            $this->username = $row['username'];
            $this->firstname = $row['firstname'];
            $this->lastname = $row['lastname'];
            $this->age = $row['age'];
            $this->email = $row['email'];
            $this->address1 = $row['address1'];
            $this->address2 = $row['address2'];
            $this->phone = $row['phone'];
            $this->gender = $row['gender'];
            $this->location = $row['location'];
            $this->sublocality = $row['sublocality'];
            $this->landmark = $row['landmark'];
            $this->city = $row['city'];
            $this->state = $row['state'];
            $this->createdon  = $row["createdon"];
            $this->status  = $row["status"];
            return true;
           }
            
        
    }

    public function addtransaction()
    { 
        $this->createdby = $this->idprofiles;
        $inserttransaction = 'INSERT INTO ' . $this->table12. '
        SET 
        idtransaction = :idtransaction,
        requestid = :requestid,
        type = :type,
        transactionkey =:transactionkey,
        amount = :amount,
        serviceid = :serviceid,
        categoryid = :categoryid,
        created = :created,
        createdby = :createdby';
        $stmt = $this->conn->prepare($inserttransaction);
         
        //clean data
        $this->idtransaction = htmlspecialchars(strip_tags($this->idtransaction));
        $this->requestid = htmlspecialchars(strip_tags($this->requestid));
        $this->type = htmlspecialchars(strip_tags($this->type));
        $this->transactionkey = htmlspecialchars(strip_tags($this->transactionkey));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->serviceid = htmlspecialchars(strip_tags($this->serviceid));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':idtransaction',$this->idtransaction);
        $stmt->bindParam(':requestid',$this->requestid);
        $stmt->bindParam(':type',$this->type);
        $stmt->bindParam(':transactionkey',$this->transactionkey);
        $stmt->bindParam(':amount',$this->amount);
        $stmt->bindParam(':serviceid',$this->serviceid);
        $stmt->bindParam(':categoryid',$this->categoryid);
        $stmt->bindParam(':created',$this->created);
        $stmt->bindParam(':createdby',$this->createdby);

        if($stmt->execute())
        {
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }

    public function gettransaction()
    {
        $gettransactions = "select * FROM transactions";
        $stmt = $this->conn->prepare($gettransactions);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }
    public function getworkers()
    {
        $selectworker = "SELECT DISTINCT idprofiles,firstname,district,email,phone,ws.categoryid,ws.serviceid FROM `worker_services` as ws LEFT JOIN service_requests ON  ws.categoryid = service_requests.categoryid and service_requests.service = ws.serviceid LEFT JOIN profiles on ws.idprofile = profiles.idprofiles LEFT JOIN profile_address on ws.idprofile = profile_address.profileid LEFT JOIN profile_details on ws.idprofile = profile_details.profileid where district = '$this->district' and ws.categoryid = '$this->categoryid' and ws.serviceid = '$this->service'";
        $stmt = $this->conn->prepare($selectworker);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }

    public function getassignedworkerdetails()
    {
        $this->type = "W";
        $assignedworkerservices = "SELECT * FROM service_requests LEFT JOIN profiles on service_requests.workerid = profiles.idprofiles LEFT JOIN profile_details on service_requests.workerid=profile_details.profileid LEFT join profile_address on service_requests.workerid=profile_address.profileid where type = '$this->type' and idservice_request='$this->idservice_request' and district = '$this->district'";
     
        // prepare query statement
        $stmt = $this->conn->prepare($assignedworkerservices);
     
        // bind id of product to be updated
        $this->idservice_request = htmlspecialchars(strip_tags($this->idservice_request));
        $this->categoryid = htmlspecialchars(strip_tags($this->categoryid));
        $this->service = htmlspecialchars(strip_tags($this->service));

        $stmt->bindParam(':idservice_request', $this->idservice_request);
        $stmt->bindParam(':categoryid', $this->categoryid);
        $stmt->bindParam(':service', $this->service);
        
        if($stmt->execute())
        {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            // set values to object properties
            $this->idprofiles = $row['idprofiles'];
            $this->username = $row['username'];
            $this->firstname = $row['firstname'];
            $this->lastname = $row['lastname'];
            $this->age = $row['age'];
            $this->email = $row['email'];
            $this->phone = $row['phone'];
            return true;
        }
        


    }

    public function addadminmessage()
    { 
      
        $insertmessage = 'INSERT INTO '.$this->table14.'
        SET 
        idmessage =:idmessage,
        senderid =:senderid,
        servicrequestid =:servicrequestid,
        recieverid =:recieverid,
        message =:message,
        subject =:subject,
        status =:status,
        created =:created,
        createdby =:createdby';
        

        $stmt = $this->conn->prepare($insertmessage);
        //clean data
        $this->idmessage = htmlspecialchars(strip_tags($this->idmessage));
        $this->senderid = htmlspecialchars(strip_tags($this->senderid));
        $this->servicrequestid = htmlspecialchars(strip_tags($this->servicrequestid));
        $this->recieverid = htmlspecialchars(strip_tags($this->recieverid));
        $this->message = htmlspecialchars(strip_tags($this->message));
        $this->subject = htmlspecialchars(strip_tags($this->subject));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':idmessage', $this->idmessage);
        $stmt->bindParam(':senderid', $this->senderid);
        $stmt->bindParam(':servicrequestid', $this->servicrequestid);
        $stmt->bindParam(':recieverid',$this->recieverid);
        $stmt->bindParam(':message',$this->message);
        $stmt->bindParam(':subject',$this->subject);
        $stmt->bindParam(':status',$this->status);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby', $this->createdby);

        if($stmt->execute()){
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }

    public function getservicemessage()
    {

          // query to read single record
          $readservicemessage = 'select ms.senderid,ms.recieverid,ms.idmessage,ms.servicrequestid,ms.subject,ms.created,ms.message,services.servicename,profile_details.firstname,profile_details.lastname,service_requests.usermessage,profiles.type FROM messages as ms LEFT join service_requests on ms.servicrequestid = service_requests.idservice_request LEFT JOIN services on service_requests.service = services.idservices LEFT join profile_details on service_requests.userid = profile_details.profileid LEFT JOIN profiles on ms.senderid = profiles.idprofiles where servicrequestid = "'.$this->servicerequestid.'" ORDER BY ms.created desc';
         
          $stmt = $this->conn->prepare($readservicemessage);
             // prepare query statement
             $stmt->bindParam(':servicerequestid',$this->servicerequestid);
            
             // bind id of product to be updated
          
             // execute query
            if($stmt->execute())
            {
              return $stmt;
            }
             
    }
    public function getmessages()
    {
          // query to read single record
          $readservicemessage = 'select ms.senderid,ms.recieverid,ms.idmessage,ms.servicrequestid,ms.subject,ms.created,ms.message,services.servicename,profile_details.firstname,profile_details.lastname,service_requests.usermessage,profiles.type FROM messages as ms LEFT join service_requests on ms.servicrequestid = service_requests.idservice_request LEFT JOIN services on service_requests.service = services.idservices LEFT join profile_details on service_requests.userid = profile_details.profileid LEFT JOIN profiles on ms.senderid = profiles.idprofiles where idmessage = "'.$this->idmessage.'" ORDER BY ms.created asc';
         
          $stmt = $this->conn->prepare($readservicemessage);
             // prepare query statement
             $stmt->bindParam(':idmessage',$this->idmessage);
            
             // bind id of product to be updated
          
             // execute query
            if($stmt->execute())
            {
              return $stmt;
            }
             


    }

    public function getworkerdetails()
    {
        $selectworker = "select firstname,lastname,worker_status FROM messages LEFT JOIN service_requests on messages.servicrequestid = service_requests.idservice_request LEFT JOIN profile_details on service_requests.workerid = profile_details.profileid where servicrequestid = '.$this->idservice_request.'";
        $stmt = $this->conn->prepare($selectworker);
        $stmt->bindParam(':idservice_request', $this->idservice_request);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
      
    }

    public function getallmessages()
    {
        $selectmessages = "SELECT * FROM messages where idmessage = '$this->idmessage'";
        $stmt = $this->conn->prepare($selectmessages);
        $stmt->bindParam(':idmessage', $this->idmessage);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));

    }
    public function getrequesteduserservicesid()
    {
        $requesteduserdata = "SELECT * FROM service_requests LEFT JOIN  services on service_requests.service = services.idservices  LEFT JOIN profile_details on service_requests.userid = profile_details.profileid where idservice_request='$this->idservice_request'";
        $stmt = $this->conn->prepare($requesteduserdata);
        $stmt->bindParam(':idservice_request', $this->idservice_request);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
        
    }
    public function getservicerequestworkerdetails()
    {
        $selectworker = "select * FROM service_requests LEFT JOIN profile_details on service_requests.workerid = profile_details.profileid  where idservice_request = '$this->idservice_request'";
        $stmt = $this->conn->prepare($selectworker);
        $stmt->bindParam(':idservice_request', $this->idservice_request);
        if($stmt->execute())
        {
        return $stmt;
        }
        error_log("row => " . json_encode($row));
      
    }

    public function getassignedworkeremail($id)
    {
        $this->idprofiles = $id;
        $selectworker = "select * FROM profiles  where idprofiles = '$this->idprofiles'";
        $stmt = $this->conn->prepare($selectworker);
        $stmt->bindParam(':idprofiles', $this->idprofiles);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
      
    }

    public function getadminid($id)
    {
        $this->idprofiles = $id;
        $selectadminid = "select * FROM profiles where type = 'A' and idprofiles = '$this->idprofiles'";
        $stmt = $this->conn->prepare($selectadminid);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
       
    }

    public function getrequesteduseronly()
    {
       
        $getrequesteduser = "SELECT DISTINCT district,type,idprofiles,firstname,lastname,email,phone,address1,address2,location,sublocality,landmark FROM  profiles LEFT JOIN profile_address on profiles.idprofiles = profile_address.profileid LEFT JOIN profile_details on profiles.idprofiles = profile_details.profileid  WHERE type= :type and deleted='0' and firstname!=''";
        $stmt = $this->conn->prepare($getrequesteduser);
        $this->type = htmlspecialchars(strip_tags($this->type));
        $stmt->bindParam(':type', $this->type);
        if($stmt->execute())
        {
            return $stmt;
        }

    }

    public function addrewards()
    {
        
        $this->status="1";
        $addreward = 'INSERT INTO '.$this->table15.'
        SET 
        idreward =:idreward,
        userid =:userid,
        rewardname =:rewardname,
        description =:description,
        points = :points,
        status = :status,
        created = :created,
        createdby = :createdby';
        

        $stmt = $this->conn->prepare($addreward);
        //clean data
        $this->idreward = htmlspecialchars(strip_tags($this->idreward));
        $this->userid = htmlspecialchars(strip_tags($this->userid));
        $this->rewardname = htmlspecialchars(strip_tags($this->rewardname));
        $this->description = htmlspecialchars(strip_tags($this->description));
        $this->points = htmlspecialchars(strip_tags($this->points));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->created = htmlspecialchars(strip_tags($this->created));
        $this->createdby = htmlspecialchars(strip_tags($this->createdby));

        $stmt->bindParam(':idreward', $this->idreward);
        $stmt->bindParam(':userid', $this->userid);
        $stmt->bindParam(':rewardname',$this->rewardname);
        $stmt->bindParam(':description',$this->description);
        $stmt->bindParam(':points',$this->points);
        $stmt->bindParam(':status',$this->status);
        $stmt->bindParam(':created', $this->created);
        $stmt->bindParam(':createdby',$this->createdby);
        if($stmt->execute())
        {
           
            return true;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;


    }

    public function readrewards()
    {
        $readrewards = 'SELECT * FROM rewards LEFT JOIN profile_details on rewards.userid = profile_details.profileid LEFT JOIN profiles on rewards.userid = profiles.idprofiles';
        $stmt = $this->conn->prepare($readrewards);
        if($stmt->execute())
        {
            return $stmt;
        }
    }

    public function searchrequestbydate()
    {
        $getservicerequestbydate = "SELECT * FROM service_requests WHERE servicedate >= '$this->fromdate' AND servicedate <= '$this->todate'  ORDER by idservice_request DESC";
        $stmt = $this->conn->prepare($getservicerequestbydate);
        if($stmt->execute())
        {
            return $stmt;
        }

    }

    public function searchrequestbydatecorporate()
    {
        $getservicerequestbydate = "SELECT * FROM service_requests LEFT JOIN profiles ON service_requests.userid = profiles.idprofiles WHERE servicedate >= '$this->fromdate' AND servicedate <= '$this->todate' AND is_corporate = 1  ORDER by idservice_request DESC";
        $stmt = $this->conn->prepare($getservicerequestbydate);
        if($stmt->execute())
        {
            return $stmt;
        }
    }
    public function getsinglereward()
    {
        $selectrewards = "SELECT * FROM rewards where idreward = '$this->idreward'";
        $stmt = $this->conn->prepare($selectrewards);
        $stmt->bindParam(':idreward', $this->idreward);
        if($stmt->execute())
        {
            return $stmt;
        }
        error_log("row => " . json_encode($row));
    }

    public function updaterewards()
        {
            $this->updatedby = $this->idreward;
            $this->updated = date("Y-m-d H:i:s");
            $updatereward = 'UPDATE ' .$this->table15. '
            SET 
            rewardname = :rewardname,
            description = :description,
            points = :points,
            updated = :updated,
            updatedby = :updatedby
            WHERE 
            idreward = :idreward';
    
            //Prepare statement
    
            $stmt = $this->conn->prepare($updatereward);
    
            //clean data
    
            $this->rewardname = htmlspecialchars(strip_tags($this->rewardname));
            $this->description = htmlspecialchars(strip_tags($this->description));
            $this->points = htmlspecialchars(strip_tags($this->points));
            $this->updated = htmlspecialchars(strip_tags($this->updated));
            $this->updatedby = htmlspecialchars(strip_tags($this->updatedby));
            $this->idreward = htmlspecialchars(strip_tags($this->idreward));
           

    
    
            //Bind data
    
    
            $stmt->bindParam(':rewardname', $this->rewardname);
            $stmt->bindParam(':description', $this->description);
            $stmt->bindParam(':points', $this->points);
            $stmt->bindParam(':updated', $this->updated);
            $stmt->bindParam(':updatedby', $this->updatedby);
            $stmt->bindParam(':idreward', $this->idreward);
           
    
            //Execute query
    
            if($stmt->execute()){
              return true;
                
            }
            echo 'Connection  Error' . $e->getMessage();
            //print error message if it has errors
    
            printf("Error: %s.\n", $stmt->error);
    
    
            return false;


        }

    

    public function deleteuserorworker()
    {
        $query = 'UPDATE '.$this->table.'
        SET deleted="1" where idprofiles = "'.$this->idprofiles.'"';

        //Prepare statement
        
        $stmt = $this->conn->prepare($query);

        //clean data

        
        $this->idprofiles = htmlspecialchars(strip_tags($this->idprofiles));
        


        //Bind data


        
        $stmt->bindParam(':idprofiles', $this->idprofiles);
        
        //Execute query

        if($stmt->execute()){
          return $this->idprofiles;
        }
        echo 'Connection  Error' . $e->getMessage();
        //print error message if it has errors

        printf("Error: %s.\n", $stmt->error);


        return false;

    }

}
?>