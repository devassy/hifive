<?php
session_start();
include 'urlrewrite.php';
if(isset($_POST["chk"]))
{
    $check = $_POST["chk"];
    if($check == "false")
    {
    if($_FILES["userfile"]["name"])
    {
        $username = $_POST["username"];
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $age = $_POST["age"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $gender = $_POST["inlineRadioOptions"];
        $address1 = $_POST["address1"];
        $address2 = $_POST["address2"];
        $location = $_POST["location"];
        $sublocality = $_POST["sublocality"];
        $landmark = $_POST["landmark"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $state = $_POST["state"];
        $idprofiles = $_POST["idprofile"];
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["userfile"]["name"];
        $tmp_name = $_FILES["userfile"]["tmp_name"];
        $filename = $random.$filename;
        $imagename = '../images/'.$filename;
        $newdata = array("username"=>"$username", "phone"=>"$phone","created"=>date("Y-m-d"),"address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender","imagename"=>"$filename");
        //Option 1: Convert data array to json if you want to send data as json
        $data = json_encode($newdata);

        //Option 2: else send data as post array.
        //$data = urldecode(http_build_query($data));
        /****** curl code ****/
        //init curl
        if(move_uploaded_file($tmp_name,$imagename))
        {
        $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/updateworkerwithimage.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $newresult = curl_exec($ch);
        $result = json_decode($newresult);
        //close curl connection
        curl_close($ch);
        if($newresult)
        {
           
            $_SESSION["message"] = $result->message;
            header("location:edituser.php?id=$idprofiles");
        }
        
        }//move_uploaded_file() ends
        else
        {
            $_SESSION["message"]= "Failed To Upload";
            header("location:edituser.php?id=$idprofiles");
        }
    }//image upload ends
    else //updatewithoutimage
    {
        $username = $_POST["username"];
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $age = $_POST["age"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $gender = $_POST["inlineRadioOptions"];
        $address1 = $_POST["address1"];
        $address2 = $_POST["address2"];
        $location = $_POST["location"];
        $sublocality = $_POST["sublocality"];
        $landmark = $_POST["landmark"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $state = $_POST["state"];
        $idprofiles = $_POST["idprofile"];
        $newdata = array("username"=>"$username", "phone"=>"$phone","address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender");
        //Option 1: Convert data array to json if you want to send data as json
        $data = json_encode($newdata);

        //Option 2: else send data as post array.
        //$data = urldecode(http_build_query($data));
        /****** curl code ****/
        //init curl

        $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/updatedetails.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $newresult = curl_exec($ch);
        $result = json_decode($newresult);
        
        //close curl connection
        curl_close($ch);
        if($result)
        {
            $_SESSION["message"]= "Successfully Updated";
            header("location:edituser.php?id=$idprofiles");
        }

    }
}//chk=false ends

//if there is an existing image
if($check == "true")
{
    if($_FILES["userfile"]["name"])
    {
        
        $oldimage = $_POST["oldimage"];
        $username = $_POST["username"];
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $age = $_POST["age"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $gender = $_POST["inlineRadioOptions"];
        $address1 = $_POST["address1"];
        $address2 = $_POST["address2"];
        $location = $_POST["location"];
        $sublocality = $_POST["sublocality"];
        $landmark = $_POST["landmark"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $state = $_POST["state"];
        $idprofiles = $_POST["idprofile"];
        $idimage = $_POST["idimage"];
        $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
        $filename = $_FILES["userfile"]["name"];
        $tmp_name = $_FILES["userfile"]["tmp_name"];
        $filename = $random.$filename;
        $imagename = '../images/'.$filename;
        $newdata = array("username"=>"$username", "phone"=>"$phone","created"=>date("Y-m-d"),"address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender","imagename"=>"$filename","idimage"=>"$idimage");
        
        //Option 1: Convert data array to json if you want to send data as json
        $data = json_encode($newdata);

        //Option 2: else send data as post array.
        //$data = urldecode(http_build_query($data));
        /****** curl code ****/
        //init curl

        if(move_uploaded_file($tmp_name,$imagename))
        {
        $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/updateworker.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $newresult = curl_exec($ch);
        $oldimage = "../images/".$oldimage;
        unlink($oldimage);
        $result = json_decode($newresult);
        if($result)
        {
            $_SESSION["message"] = $result->message;
            header("location:edituser.php?id=$idprofiles");
        }
        //close curl connection
        curl_close($ch);
        
        }//move_uploaded_file() ends
        else
        {
        $_SESSION["message"]= "Failed to Upload";
        header("location:edituser.php?id=$idprofiles");
        }
    }
    else
    {
        
        $username = $_POST["username"];
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $age = $_POST["age"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $gender = $_POST["inlineRadioOptions"];
        $address1 = $_POST["address1"];
        $address2 = $_POST["address2"];
        $location = $_POST["location"];
        $sublocality = $_POST["sublocality"];
        $landmark = $_POST["landmark"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $state = $_POST["state"];
        $idprofiles = $_POST["idprofile"];
        
        $newdata = array("username"=>"$username", "phone"=>"$phone","address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender");
        //Option 1: Convert data array to json if you want to send data as json
        $data = json_encode($newdata);

        //Option 2: else send data as post array.
        //$data = urldecode(http_build_query($data));
        /****** curl code ****/
        //init curl

        $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/updatedetails.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $newresult = curl_exec($ch);
        $result = json_decode($newresult);
        
        //close curl connection
        curl_close($ch);
        if($result)
        {
            $_SESSION["message"] = $result->message;
            header("location:edituser.php?id=$idprofiles");
        }

    }
}

 


}//_POST["chk"]
?>
