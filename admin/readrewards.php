<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);

//Blog post query
$result = $post->readrewards();
//Get row count

$num = $result->rowCount();
//Check if any posts
if($num>0){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
            $post_item = array(
            'idreward'=>$row["idreward"],
            'userid'=>$row["userid"],
            'rewardname'=>$row["rewardname"],
            'description' => $row["description"],
            'points' => $row["points"],
            'status'=>$row["status"],
            'firstname'=>$row["firstname"],
            'lastname'=>$row["lastname"],
            'type'=>$row["type"],
            'created'=>$row["created"],
            'message'=>"success"
        );
        array_push($post_arr['data'], $post_item);
    }

    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
    $post_arry = array();
    $post_arry['data'] =  array();
    $post_item = array("message"=>"No records Found");
    array_push($post_arry['data'], $post_item);
    echo json_encode($post_arry);
    return true;

}

?>