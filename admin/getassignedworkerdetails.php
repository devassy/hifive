<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));

$post->idservice_request = $data->idservice_request;
$post->categoryid = $data->categoryid;
$post->service = $data->service;
$post->district = $data->district;


// Update post

if($result = $post->getassignedworkerdetails()){
    
    if($result)
    {
           $product_arr = array(
            "idprofiles"=>$post->idprofiles,
            "username"=>$post->username,
            "email"=>$post->email,
            "firstname"=>$post->firstname,
            "lastname"=>$post->lastname,
            "phone"=>$post->phone
        );
    }
    if(count($product_arr) > 0){
        echo json_encode($product_arr);
        return true;
    }
    else{
        print_r(array("Message" => "No Records found"));
    }
    
}