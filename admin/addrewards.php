<?php
session_start();
?>

<?php

if($_SESSION["type"]=="A")
{ 
$adminid = $_SESSION["idprofiles"];
include 'urlrewrite.php';
$ch = curl_init();
$data = array("idcategory","1");
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getusers.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$resultusers = curl_exec($ch);
$getusers = json_decode($resultusers);

//close curl connection
curl_close($ch);

//print result
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="../assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="../assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                    <!-- CSS Files -->
                    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
            </link>
        </link>
    </head>
    <body class="">
    <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            
            <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="../hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px" >
                  </a>
            </div>
                <?php include 'adminmenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title">
                                            Add Rewards
                                        </h4>
                                        <?php
                                        if(isset($_SESSION["idprofiles"]))
                                        {
                                        $userid = $_SESSION["idprofiles"];
                                        $username = $_SESSION["username"];
                                        }
                                        ?>
                                    </div>
                                    <div class="card-body">
                                    <div class="col-md-4">
                                                     
                                                     <label class="bmd-label-floating">
                                                             Type of User
                                                         </label>
                                                    <span style = "padding-right:15px;"></span>
                                                     <div class="form-check form-check-radio form-check-inline">
                                                         <label class="form-check-label">
                                                           <input class="form-check-input" type="radio" name="typeofuser" id="user" value="user">User
                                                           <span class="circle">
                                                               <span class="check"></span>
                                                           </span>
                                                         </label>
                                                       </div>
                                                       <div class="form-check form-check-radio form-check-inline">
                                                         <label class="form-check-label">
                                                           <input class="form-check-input" type="radio" name="typeofuser" id="worker" value="worker">worker
                                                           <span class="circle">
                                                               <span class="check"></span>
                                                           </span>
                                                         </label>
                                                       </div>
                                                 </div>
                                        <form name = "f1" method="post" action="curlreward.php">
                                            <div class ="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-md-offset-3">
                                                             <div id="messages">
                                                                 <p></p>
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                        User/worker name
                                                        </label>
                                                      <select name="userid" id="userid" class="browser-default custom-select">
                                                      <option>Please select user</option>
                                                          
                                                       </select>
                                                    </div>
                                                </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                        Reward Name
                                                        </label>
                                                        <select class="browser-default custom-select" name="rewardname" id = "rewardname">
                                                           <option value='cashback'>Cash Back</option>
                                                           <option value='halfprice'>Half Price</option>
                                                        </select>
                                                    
                                                        <!-- <input class="form-control" type="text" id="service_name" name="service_name">
                                                        </input>
                                                    </div> -->
                                                </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                        Add Points
                                                        </label>
                                                      <select name="points" id="points" class="browser-default custom-select">
                                                      <option>Please Select Points</option>
                                                      <option value="5">5</option>
                                                      <option value="10">10</option>
                                                      <option value="15">15</option>
                                                       </select>
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                            
                                            
                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Description
                                                            </label>
                                                        <textarea class="form-control" rows = "4" cols = "90" name="description" id="description"></textarea> 
                                                        <!-- <input class="form-control" type="text" name="cat_name" id="cat_name">
                                                        </input> -->
                                                    </div>
                                                </div>
                                                
                                            </div>
                                             
                                            
                                            <button class="btn btn-primary pull-right" type="submit" id="submit" name="submit">
                                                Request
                                            </button>
                                                    <table id= "mytable">
                                                    </table>    
                                            <div class="clearfix">
                                            </div>
                                        </form>
                                        <?php
                                            if(isset($_SESSION["result"]))
                                            {
                                                $result = $_SESSION["result"];
                                                if($result=="Reward Added Successfully")
                                                {
                                                ?>
                                                <div style="color:green;" id="testdiv"><?php echo $result;?></div>
                                                <script type="text/javascript">
                                                   $(function()
                                                   {
                                                           $("#testdiv").delay(4000).fadeOut();
                                                   });
                                               </script>
                                                <?php
                                                unset($_SESSION["result"]);
                                                unset($result);
                                                }
                                                else
                                                {
                                                    ?>
                                                    <div style="color:red;" id="testdiv"><?php echo $result;?></div>
                                                    <script type="text/javascript">
                                                       $(function()
                                                       {
                                                               $("#testdiv").delay(5000).fadeOut();
                                                       });
                                                   </script>
                                                    <?php
                                                    unset($_SESSION["result"]);
                                                    unset($result);                                                
                                                }
                                            }
                                            ?>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>

                    <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright">
                         ©
                        <script>
                            document.write(new Date().getFullYear())
                            </script>
    <!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
                    </div>
                </div>
            </footer>

                </div>
            </div>
            <!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js" type="text/javascript">
</script>
<script src="../assets/js/core/popper.min.js" type="text/javascript">
</script>
<script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
</script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js">
</script>

<!-- add services vaidation -->
<script src="assets/js/addservicerequest.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
</script>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js">
</script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js">
</script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
</script>
<script type="text/javascript">
  $(document).ready(function() {

      $('#user').click(function(e){
        var user = $("#user").val();
        var jsonData = {type:user};
        $.ajax({
            type: "POST",
            data: JSON.stringify(jsonData),
            url: 'getusers.php',
            dataType: "json",
            success : function(data){
                
                if (data)
                {
                   
                    $('#userid').children().remove();
                    var person = JSON.stringify(data['data'][0]);
                    for(var i=0;i<person.length;i++)
                    {
                        var stddata = JSON.stringify(data['data'][i]);
                        var json = JSON.parse(stddata);
                        var firstname = json["firstname"];
                        var lastname = json["lastname"];
                        var district = json["district"];
                        var location = json["location"];
                        var idprofiles = json["idprofiles"];
                        $('#userid').append( '<option value="'+idprofiles+'">'+firstname+' '+lastname+' -'+location+' -'+district+'</option>' );
                        
                    }
                    
                    
                }
                else {
                    alert("error");
                }
            }
        });


      });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {

      $('#worker').click(function(e){
        var worker = $("#worker").val();
        var jsonData = {type:worker};
        $.ajax({
            type: "POST",
            data: JSON.stringify(jsonData),
            url: 'getusers.php',
            dataType: "json",
            success : function(data){
               
                if (data)
                {
                    
                    $('#userid').children().remove();
                    var person = JSON.stringify(data['data'][0]);
                    for(var i=0;i<person.length;i++)
                    {
                        var stddata = JSON.stringify(data['data'][i]);
                        var json = JSON.parse(stddata);
                        var firstname = json["firstname"];
                        var lastname = json["lastname"];
                        var idprofiles = json["idprofiles"];
                        var district = json["district"];
                        var location = json["location"];
                        $('#userid').append( '<option value="'+idprofiles+'">'+firstname+' '+lastname+','+location+','+district+'</option>' );
                    }
                    
                    
                }
                else {
                    alert("error");
                }
            }
        });


      });
  });
</script>



        </div>
    </body>
   
</html>
<?php
}
else
{
    header("location:index.php");
}  

?>