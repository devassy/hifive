<?php
include 'urlrewrite.php';
session_start();
if($_SESSION["type"]=="A")
{
$userid = $_POST["userid"];
$rewardname = $_POST["rewardname"];
$description = $_POST["description"];
$adminid = $_SESSION["idprofiles"];
$points = $_POST["points"];
$rewarddata = array("userid"=>$userid,"rewardname"=>$rewardname,"description"=>$description,"createdby"=>$adminid,"points"=>$points,"userid"=>$userid);
$data = json_encode($rewarddata);

$ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/addreward.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $result = curl_exec($ch);
       
        $rewardresult = json_decode($result);
        
        $message = $rewardresult->message;
        //close curl connection
        curl_close($ch);
       
        
        if($message=="Reward Added Successfully")
        {
            $_SESSION["result"]=$message;
            header("location:addrewards.php");
        }
        else
        {
           
            $_SESSION["result"]="Failed To Add Reward";
            header("location:addrewards.php");

        }
    }
    else
    {
        header("location:index.php");
    }
?>