<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';
include_once 'Sendemail.php';


//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();
$send = new Sendemail();

// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));

$post->idservice_request = $data->idservice_request;
$post->userid = $data->userid;
$post->workerid = $data->workerid;
$post->adminid = $data->adminid;
$post->servicedate = $data->servicedate;
$post->service_location = $data->service_location;
$post->usermessage = $data->usermessage;
$post->worker_status = $data->worker_status;
$post->payment_status = $data->payment_status;
$post->payment_type = $data->payment_type;
$post->servicedistrict = $data->servicedistrict;
$post->amount = $data->amount;
$post->categoryid = $data->categoryid;
$post->service = $data->service;
// Update post

if($result = $post->updateservicerequest()){
    
    if($result)
    {
        if($post->workerid)
        {
            $result = $post->getassignedworkeremail($post->workerid);
            $num = $result->rowCount();
            if($num>0)
            {
                if($row = $result->fetch(PDO::FETCH_ASSOC))
                {
                    $email = $row["email"];
                    $username = $row["username"];
                }
            }
            $admindata = $post->getadminid($post->adminid);
            while($row = $admindata->fetch(PDO::FETCH_ASSOC))
            {
                $adminemail = $row["email"];
            }
           
            $qu = $send->send($email,$post->service_location,$post->servicedistrict,$post->servicedate,$username,$adminemail);
            if($qu)
            {
               
                echo json_encode(array('message' =>'service request updated successfully'));
                return true;
            }

        }
        else
        {
            echo json_encode(array('message' => 'service request updated successfully'));
            return true;
        }
    }
    
}else{
    echo json_encode(array('message' => 'Failed to  update Service Request'));
    return true;
}