<?php
session_start();
include 'urlrewrite.php';
date_default_timezone_set('Asia/Kolkata');
$data = array();
//Option 1: Convert data array to json if you want to send data as json


//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/readrequest.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$newdata = curl_exec($ch);
$result = json_decode($newdata);

//close curl connection
curl_close($ch);
//print result
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="../assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="../assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
                    <!-- CSS Files -->
                    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <link href="../assets/css/listrequest.css" rel="stylesheet"/>
                   
            </link>
        </link>
        

    </head>
   
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
               
            <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="../hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px" >
                  </a>
            </div>
                <?php include 'adminmenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title ">
                                            View Service Requests
                                        </h4>
                                       
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class=" text-primary">
                                                    <th>Servicedate</th>
                                                    <th>Username</th>
                                                    <th>Service</th>
                                                    <th>Category</th>
                                                    <th>Message</th>
                                                    <th>User Created Date</th>
                                                    <th>Location</th>
                                                    <th>Work Status</th>
                                                    <th>Action</th>
                                                </thead>
                                                <tbody>
												<?php
                                                
												if(!empty($result))
												{
                                                    if(isset($result->message))
                                                    {
                                                        echo "$result->message";

                                                    }
                                                    else
                                                    {
												foreach($result as $value)
												{
													foreach($value as $data)
													{
												?>
                                                   <tr><td><?php $createddate = date("d-M-Y",strtotime($data->servicedate));echo $createddate;?></td>
                                                   <td><?php echo $data->firstname;?></td>
                                                   <td><?php echo $data->servicename;?></td>
                                                   <td><?php echo $data->categoryname;?></td>
                                                   <td><?php echo $data->usermessage;?></td>
                                                   <td>
                                                   <?php
                                                   $datetime = $data->createdon;
                                                   $created = date("d-M-Y",strtotime($datetime)); // output 2013-08-14
                                                   echo $created."<br>";
                                                   $createdtime = date("H:i:s",strtotime($datetime));
                                                   echo $createdtime; // output 11:45:45
                                                   ?>
                                                   </td>
                                                   <td><?php echo $data->service_location;?></td>
                                                   <td>
                                                   <?php 
                                                   $servicestatus = $data->service_status;
                                                   if($servicestatus=="N")
                                                   {
                                                       echo "Not started";
                                                   }
                                                   else if($servicestatus=="S")
                                                   {
                                                       echo "Started";
                                                   }
                                                   else if($servicestatus=="ca")
                                                   {
                                                    echo "Cancelled";
                                                   }
                                                   else if($servicestatus=="P")
                                                   {
                                                    echo "In Progress";
                                                   }
                                                   else if($servicestatus=="C")
                                                   {
                                                    echo "Completed";
                                                   }
                                                   else
                                                   {

                                                   }
                                                   ?>
                                                   </td> 
													<td>
													 <a href="editcurlservicerequest.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-success" rel="tooltip" type="button">
                                                                    <i class="material-icons">
                                                                        edit
                                                                    </i>
                                                                </button>
                                                            </a>
                                                            <a href="curldeleterequest.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-danger demo" rel="tooltip" type="button" onclick="return confirm('Are you sure you want to Remove?');">
                                                                    <i class="material-icons">
                                                                       close
                                                                    </i>
                                                                </button>
                                                            </a>
													
                                                    <a href="addtransaction.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-danger" rel="tooltip" type="button">
                                                                    <i class="material-icons">
                                                                    add
                                                                    </i>
                                                                </button>
                                                            </a>
													
                                                   
                                                            <a href="viewservicemessages.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-success" rel="tooltip" type="button">
                                                                    <i class="material-icons">
                                                                   message
                                                                    </i>
                                                                </button>
                                                            </a>
													</td>

                                                    </tr>
                                                </tbody>
												<?php
													}
												}
                                                }
                                            }
												
												?>
                                            </table>
                                            <?php
                                         if(isset($_SESSION["message"]))
                                         {
                                             $result = $_SESSION["message"];
                                             if($result=="Data Deleted Successfully")
                                             {
                                                 $result ="Service Request Deleted Successfully";
                                             ?>
                                             <div style="color:green;" id = "testdiv"><?php echo $result;?></div>
                                             <script type="text/javascript">
                                                $(function()
                                                {
                                                        $("#testdiv").delay(7000).fadeOut();
                                                });
                                            </script>
                                             <?php
                                             unset($_SESSION["message"]);
                                             unset($result);
                                            }
                                            else
                                            {
                                                ?>
                                                <div style="color:red;" id = "testdiv"><?php echo $result;?></div>
                                                <script type="text/javascript">
                                                   $(function()
                                                   {
                                                           $("#testdiv").delay(7000).fadeOut();
                                                   });
                                               </script>
                                                <?php
                                                unset($_SESSION["message"]);
                                                unset($result);


                                            }
                                         }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include 'footer.php';?>
                    </div>
                </div>
                <div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>				
				<h4 class="modal-title">Are you sure?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger">Delete</button>
			</div>
		</div>
	</div>
</div>     
                <!--   Core JS Files   -->
               
                <script src="../assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="../assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="../assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
                
            </div>
        </div>
    </body>
</html>