<?php
session_start();
include 'urlrewrite.php';
$catid = $_POST["catid"];
$service = $_POST["servicename"];
$adminid = $_SESSION["idprofiles"];
$newdata = array("categoryid"=>$catid,"servicename"=>$service,"createdby"=>$adminid);
$data = json_encode($newdata);

$ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/addservice.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $result = curl_exec($ch);
        //close curl connection
        curl_close($ch);
        $categoryresult = json_decode($result);
        
        $message = $categoryresult->message;
        
        if($message=="Service Name Added Successfully")
        {
            $_SESSION["servicemessage"]= $message;
            header("location:addservices.php");
        }
        else
        {
           
            $_SESSION["servicefailure"]="Failed To Add Services";
            header("location:addservices.php");

        }
?>