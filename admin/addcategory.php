<?php
session_start();
?>
<?php
if($_SESSION["type"]=="A")
{
?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
        <link href="../assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="../assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                    <!-- CSS Files -->
                    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    
            </link>
        </link>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    </head>
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="../hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px" >
                  </a>
            </div>
                <?php include 'adminmenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title">
                                            Add Category

                                        </h4>
                                       
                                    </div>
                                    <div class="card-body">
                                        <form id="add_category" method="post" action ="addcurlcategory.php">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Category name
                                                        </label>
                                                        <input class="form-control" type="text" id="cat_name" name="cat_name">
                                                        </input>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                
                                                <div id = "messages"></div>
                                            </div>
                                            
                                            
                                           
                            
                                            <button class="btn btn-primary pull-right" type="submit" id="submit" name="submit">
                                                Add
                                            </button>
                                            
                                            <div class="clearfix">
                                            </div>
                                        </form>
                                        <?php
                                         if(isset($_SESSION["message"]))
                                         {
                                             $result = $_SESSION["message"];
                                             if($result=="Category Added Successfully")
                                             {
                                             ?>
                                             <div style="color:green;" id = "testdiv"><?php echo $result;?></div>
                                             <script type="text/javascript">
                                                $(function()
                                                {
                                                        $("#testdiv").delay(7000).fadeOut();
                                                });
                                            </script>
                                             <?php
                                             unset($_SESSION["message"]);
                                             unset($result);
                                            }
                                            else
                                            {
                                                ?>
                                                <div style="color:red;" id = "testdiv"><?php echo $result;?></div>
                                                <script type="text/javascript">
                                                   $(function()
                                                   {
                                                           $("#testdiv").delay(4000).fadeOut();
                                                   });
                                               </script>
                                                <?php
                                                unset($_SESSION["message"]);
                                                unset($result);


                                            }
                                         }
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>

                    <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright">
                         ©
                        <script>
                            document.write(new Date().getFullYear())
                            </script>
    <!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
                    </div>
                </div>
            </footer>

                </div>
            </div>
            <!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js" type="text/javascript">
</script>
<script src="../assets/js/core/popper.min.js" type="text/javascript">
</script>
<script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
</script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js">
</script>


<!--Add category form validation-->
<script src="../assets/js/addcategory.js">
</script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
</script>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js">
</script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js">
</script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
</script>
<script type="text/javascript">
 $(document).ready(function(){
  $('#cat_name').keyup(function(){
    var categoryname = $('#cat_name').val();
    var jsonData = {categoryname:categoryname};
    
    $.ajax({
     type: "POST",
     url: "checkcategoryname.php",
     data: JSON.stringify(jsonData),
     dataType: "json",
     success: function(data)
     {
        $('#cat_name').val("");
        $('#messages').html('This Category has already added').fadeIn().delay(4000).fadeOut();
        $('#messages').css('color','red');
     }
      
    }); //ajax ends
   
  });//user name change function ends
 }); //document ready function ends
</script> 


        </div>
    </body>
    
</html>     
<?php
}
else
{
    header("location:index.php");
}
?>
