<?php
session_start();
?>
<?php
if($_SESSION["type"]=="A")
{
    if(isset($_POST["submit"]))
    {
    include 'urlrewrite.php';
    $fromdate = $_POST["fromdate"];
    $todate = $_POST["todate"];
    $type = $_POST["type"];
    $userid = $_SESSION["idprofiles"];
    
    $datedata = array("fromdate" =>'$fromdate',"todate"=>$todate,"type"=>$type);
    $data = json_encode($datedata);
    //Option 1: Convert data array to json if you want to send data as json
    
    //Option 2: else send data as post array.
    //$data = urldecode(http_build_query($data));
    /****** curl code ****/
    //init curl
    $ch = curl_init();
    // URL to be called
    curl_setopt($ch, CURLOPT_URL, "$url/searchrequest.php");
    //set post TRUE to do a regular HTTP POST
    curl_setopt($ch, CURLOPT_POST, 1);
    //set http headers - if you are sending as json data (i.e. option 1) else comment this 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //send post data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //return as output instead of printing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute curl request
    $newresult = curl_exec($ch);
    $searchresult = json_decode($newresult);
   
    if(isset($searchresult->message))
    {
        $message = $searchresult->message;
        
    }
    else
    {
        foreach($searchresult as $value)
        {
            foreach($value as $data)
            {
                $message = $data->message;
               
            }
        }

    }
    }
?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
        <link href="../assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="../assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                    <!-- CSS Files -->
                    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <link href="../assets/css/summary.css" rel="stylesheet">
           
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    </head>
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            
            <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="../hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px" >
                  </a>
            </div>
                <?php include 'adminmenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title">
                                        Summary Of Work

                                        </h4>
                                       
                                    </div>
                                    <div class="card-body">
                                        <form id="add_category" method="post" action ="summary.php">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        From Date
                                                        <input class="form-control" type="date" id="fromdate" name="fromdate" value="<?php if(isset($fromdate)){echo $fromdate;}?>">
                                                        </input>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                       To Date
                                                        <input class="form-control" type="date" id="todate" name="todate" value ="<?php if(isset($todate)){echo $todate;}?>">
                                                        </input>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="bmd-label-floating">
                                                                Type Of User
                                                            </label>
                                                            <select class="browser-default custom-select" type="text" name="type" id="type">
                                                           
                                                            <option value="U">User</option>
                                                            <option value="C">Corporate</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                            </div>
                                            
                                            <div class="row">
                                                
                                                <div id = "messages"></div>
                                            </div>
                                            
                                            
                                           
                            
                                            <button class="btn btn-primary pull-right" type="submit" id="submit" name="submit">
                                            Search
                                            </button>
                                            
                                            <div class="clearfix">
                                            </div>
                                        </form>
                                    </div>
                                    <?php
                                    date_default_timezone_set('Asia/Kolkata');
                                    if(isset($searchresult)&&($message=="found"))
                                    {
                                    ?>
                                    <div class="table-responsive">
                                            <table class="table">
                                                <thead class=" text-primary">
                                                    <th>Sl.No</th>
                                                    <th>User Message</th>
                                                    <th>Service Location</th>
                                                    <th>Service District</th>
                                                    <th>Service Status</th>
                                                    <th>Service Created Date</th>
                                                    <th>Service Date</th>
                                                    <th>Worker Status</th>
                                                    <th>Days Delayed</th>
                                                </thead>
                                                <tbody>
												<?php
                                                
												
                                                    if(isset($result->message))
                                                    {
                                                        echo "$result->message";

                                                    }
                                                    else
                                                    {
                                                    $i=1;
												foreach($searchresult as $value)
												{
													foreach($value as $data)
													{
												?>
                                                   <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $data->usermessage;?></td>
                                                   <td><?php echo $data->service_location;?></td>
                                                   <td><?php echo $data->servicedistrict;?></td>
                                                   <?php
                                                   if($data->service_status=='C')
                                                   {
                                                    ?>
                                                   <td><?php echo "Completed";?></td>
                                                 <?php
                                                   }
                                                   else if($data->service_status=='P')
                                                   {
                                                    ?>
                                                   <td><?php echo "Progress";?></td>
                                                 <?php
                                                   }
                                                   else if($data->service_status=='Ca')
                                                   {
                                                    ?>
                                                   <td><?php echo "Cancelled";?></td>
                                                 <?php
                                                   }
                                                   else if($data->service_status=='N')
                                                   {
                                                    ?>
                                                   <td><?php echo "Not Started";?></td>
                                                 <?php
                                                   }
                                                   else
                                                   {
                                                       
                                                   }

                                                   ?>
                                                   <td><?php $sercreateddate = date('d-M-Y',strtotime($data->createdon));echo $sercreateddate;?></td>
                                                   <td><?php  $serdata = date('d-M-Y',strtotime($data->servicedate));echo $serdata;?></td>

                                                   <td>
                                                   <?php
                                                   
                                                       if(empty($data->workerid))
                                                       {
                                                           echo "Not Assigned";
                                                       }
                                                       else
                                                       {
                                                           echo "Assigned";
                                                       }
                                                   
                                                   ?>
                                                   </td>
                                                   <?php
                                                   if($data->service_status!='C')
                                                   {
                                                       $currentdate = date('Y-m-d');
                                                       $servicedate = $data->servicedate;
                                                       $date1Timestamp = strtotime($currentdate);
                                                       $date2Timestamp = strtotime($servicedate);
                                                       $diff = $date1Timestamp - $date2Timestamp;
                                                       $days = floor($diff/ (60*60*24) );

                                                   ?>
                                                   
                                                   <?php if($currentdate>$servicedate){echo "<td>$days</td>";}?>
                                                   <?php
                                                   }

                                                   ?>
                                                
                                                    </tr>
                                                </tbody>
												<?php
                                                $i++;
                                                    }
                                                    
												}
                                                }
                                            
												
												?>
                                            </table>


                                    <?php
                                    }
                                    else
                                    {
                                        echo "No Records Found";
                                    }
                                    ?>
                                </div>
                            </div>
                            
                        </div>
                        
        
                    </div>
                    

                    <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright">
                         ©
                        <script>
                            document.write(new Date().getFullYear())
                            </script>
    <!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
                    </div>
                </div>
            </footer>

                </div>
            </div>
            <!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js" type="text/javascript">
</script>
<script src="../assets/js/core/popper.min.js" type="text/javascript">
</script>
<script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
</script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js">
</script>


<!--Add category form validation-->
<script src="/assets/js/addcategory.js">
</script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
</script>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js">
</script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js">
</script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
</script>


        </div>
    </body>
    
</html>     
<?php
}
else
{
    header("location:index.php");
}
?>
