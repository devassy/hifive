<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$post->servicerequestid = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');

$result = $post->getservicemessage();

//Get row count

$num = $result->rowCount();


//Check if any posts
if($num >0 ){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
            $post_item = array(
            'idmessage'=>$row["idmessage"],
            'senderid' =>$row["senderid"],
            'servicrequestid' =>$row["servicrequestid"],
            'recieverid'=>$row["recieverid"],
            'message'=>$row["usermessage"],
            'subject'=>$row["subject"],
            'servicename'=>$row["servicename"],
            'usermessage'=>$row["usermessage"],
            'message'=>$row["message"],
            'firstname'=>$row["firstname"],
            'lastname'=>$row["lastname"],
            'type'=>$row["type"],
            'created'=>$row["created"],
            'resultedmessage'=>"success"
        );
        array_push($post_arr['data'], $post_item);
    }
   
    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
    echo json_encode(
        array('resultedmessage' => 'No records')
    );
}
?>