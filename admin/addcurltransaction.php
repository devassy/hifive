<?php
session_start();
include 'urlrewrite.php';
$adminid = $_SESSION["idprofiles"];
$amount = $_POST["amount"];
$type = $_POST["type"];
$requestid = $_POST["requestid"];
$categoryid = $_POST["categoryid"];
$serviceid = $_POST["serviceid"];

$transactiondata = array("requestid"=>$requestid,"type"=>$type,"categoryid"=>$categoryid,"serviceid"=>$serviceid,"amount"=>$amount,"idprofiles"=>$adminid);

$data = json_encode($transactiondata);

$ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/addtransactions.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $result = curl_exec($ch);
        
        //close curl connection
        curl_close($ch);
        $transactionresult = json_decode($result);
        
        $message = $transactionresult->message;
        
        if($message=="Transaction Added Successfully")
        {
            $_SESSION["message"]=$message;
            header("location:addtransaction.php");
        }
        else
        {
           
            $_SESSION["message"]="Failed To Add Transaction";
            header("location:addtransaction.php?id=$requestid");

        }
?>