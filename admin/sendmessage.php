<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';
date_default_timezone_set('Asia/Kolkata');
//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);
$data = json_decode(file_get_contents("php://input"));

$string = "0123456789";
$post->idmessage = str_shuffle($string); 
$post->senderid = $data->senderid;
$post->servicrequestid = $data->idservice_request;
$post->recieverid = $data->recieverid;
$post->subject = $data->subject;
$post->message = $data->message;
$post->status = "1";
$post->created = date("Y-m-d h:i:sa");
$post->createdby = $data->senderid;
if($result = $post->addadminmessage())
{
    if($result=="1")
    {
        echo json_encode(array('message' => 'Message Sent Successfully'));
        return true;
    }
    else{
        echo json_encode(array('message' => 'Message Not Send'));
        return false;
    }
     
}
else{
    echo json_encode(array('message' => 'Message Not Send'));
    return false;
}
?>