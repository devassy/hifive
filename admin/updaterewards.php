<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));

$post->idreward = $data->idreward;
$post->description = $data->description;
$post->points = $data->points;
$post->rewardname = $data->rewardname;

$update = $post->updaterewards();
if($update=="1")
{
echo json_encode(array('message' =>'Reward Updated Successfully'));
return true;
}
else
{
    echo json_encode(array('message' =>'Failed To Update Reward'));
    return true;
}




?>

