<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';
date_default_timezone_set('Asia/Kolkata');
//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);
$data = json_decode(file_get_contents("php://input"));
$string = "0123456789";
$post->idservice_request = str_shuffle($string);
$post->userid = $data->userid; 
$post->service = $data->service;
$post->address = $data->address;
$post->payment_status = "0";
//$post->amount = $data->amount;
$post->servicedate = $data->servicedate;
$post->categoryid = $data->categoryid;
$post->usermessage = $data->usermessage;
$post->servicedistrict = $data->servicedistrict;
$post->createdon = date("Y-m-d H:i:s");
$currentdate = date("Y-m-d");
$post->createdby = $data->userid;
$servdate = $post->servicedate;
$post->servicedistrict = $data->servicedistrict;
$post->payment_type = $data->payment_type;
if($currentdate > $servdate)
{
    echo json_encode(array('message' => 'Please select a future date'));
    return true;
}

//$post->type = $data->type;



if($lastid = $post->addservicerequest())
{
    echo json_encode(array('message' => 'Inserted Successfully'));
    return true;
}
else{
    echo json_encode(array('message' => 'Request Not Created'));
    return true;
}

?>