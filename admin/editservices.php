<?php
session_start();
if($_SESSION["type"]=='A')
{
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Hifive
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>

<body class="">
<?php
 include 'urlrewrite.php';
$id = $_GET["id"];

$data = array("id" =>'$id');
//Option 1: Convert data array to json if you want to send data as json
//$data = json_encode($data);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/read_singleservice.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);

$newdata = json_decode($result);

//$newdata = json_decode($result);
//close curl connection
curl_close($ch);

?>

<script>

//document.write(x);
</script>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
     
    <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="../hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px" >
                  </a>
            </div>
      <div class="sidebar-wrapper">
      <?php include 'adminmenu.php';?>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php include 'header.php';?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Services</h4>
                  
                </div>
                           
                <div class="card-body">
                  <form method = "post" action = "editcurlservices.php">
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Servicename
                                  </label>
                                  <input class="form-control" type="text" name="servicename" id="servicename" value = "<?php print_r($newdata->servicename);?>">
                                  </input>
                              </div>
                          </div>
                          
                      </div>
                      <div id = "messages"></div>
                      
                    <input type="hidden" name ="idservices" value= "<?php print_r($newdata->idservices);?>">
                    <button type="submit" id="submit" class="btn btn-primary pull-right">Update Service</button>
                    <div class="clearfix"></div>
                  </form>
                  <?php
                  if(isset($_SESSION["message"]))
                  {
                    $result = $_SESSION["message"];
                    if($result=="Updated Successfully")
                    {
                    ?>
                    <div style="color:green;" id="testdiv"><?php echo $result;?></div>
                    <script type="text/javascript">
                       $(function()
                       {
                               $("#testdiv").delay(4000).fadeOut();
                       });
                   </script>
                    <?php
                    unset($_SESSION["message"]);
                    unset($result);
                    }
                    else
                    {
                        $result = $_SESSION["servicemessage"];
                        ?>
                        <div style="color:red;" id="testdiv"><?php echo $result;?></div>
                        <script type="text/javascript">
                           $(function()
                           {
                                   $("#testdiv").delay(4000).fadeOut();
                           });
                       </script>
                        <?php
                        unset($_SESSION["servicemessage"]);
                        unset($result);
                    }

                  }
                 
                ?>
                </div>
              </div>
            </div>
          
            </div> 
          </div>

            <footer class="footer">
        <div class="container-fluid">
         
          </nav> 
          <div class="copyright">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script><!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
          </div>
        </div>
      </footer>


        </div>
      </div>
               <script src="../assets/js/editservice.js" type="text/javascript"></script>

              <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
              <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
              <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
              <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
              <!--  Google Maps Plugin    -->
              <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
              <!-- Chartist JS -->
              <script src="../assets/js/plugins/chartist.min.js"></script>
              <!--  Notifications Plugin    -->
              <script src="../assets/js/plugins/bootstrap-notify.js"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
              
                </div>
              </div>
  <!--   Core JS Files   -->
  
 
</body>

</html>
<script type="text/javascript">
 $(document).ready(function(){
  $('#servicename').keyup(function(){
    var servicename = $('#servicename').val();
    var jsonData = {servicename:servicename};
    
    $.ajax({
     type: "POST",
     url: "checkservicename.php",
     data: JSON.stringify(jsonData),
     dataType: "json",
     success: function(data)
     {
        $('#servicename').val("");
        $('#messages').html('This Service has already added').fadeIn().delay(4000).fadeOut();
        $('#messages').css('color','red');
     }
      
    }); //ajax ends
   
  });//user name change function ends
 }); //document ready function ends
</script> 
<?php
}
else
{
    header("location:index.php");
}
