<?php
include 'urlrewrite.php';
$data = array("readcategory"=>"read");
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/gettransactions.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$transactionresult = curl_exec($ch);
$viewresult = json_decode($transactionresult);

//close curl connection
curl_close($ch);

?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="../assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="../assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

                    <!-- CSS Files -->
                    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <link href="../assets/css/admintransaction.css" rel="stylesheet"/>

            </link>
        </link>
    </head>
    <body class="">
    <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
              
            <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="../hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px" >
                  </a>
            </div>
                <?php include 'adminmenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title ">
                                            Categories
                                        </h4>
                                      
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class=" text-primary">
                                                    <tr><th>Transaction_id</th>
                                                    <th>Requestid</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Date</th>
                                                    <th>Transaction Response Message</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                 if (!empty($viewresult))
                                                 {
                                                     if(isset($viewresult->message))
                                                     {
                                                         echo $viewresult->message;

                                                     }
                                                     else
                                                     {
                                                   $i=0;
                                                     foreach($viewresult as $value)
                                                     {
                                                       foreach($value as $data)
                                                       {
                                                        $i++;
                                                    ?>
                                                <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $data->requestid;?></td>
                                                <td><?php echo $data->type;?></td>
                                                <td><?php echo $data->amount;?></td>
                                                <td><?php echo $data->created;?></td>
                                                <td><?php echo $data->transactionresponse_message;?></td>
                                                    <?php
                                                     }
                                                    }
                                                    }
                                                   
                                                 ?>
                                                 </tr>
                                                  <?php
                                                 }
                                                 ?>
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="footer">
                            <div class="container-fluid">
                                <div class="copyright">
                                    ©
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script>
                                    <!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="../assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="../assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="../assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
                
            </div>
        </div>
    </body>
</html>