<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);
$post->page = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');
$limit = 5;
$page = $post->page;
$page1=($page*$limit)-$limit;
//Blog post query
$result = $post->read();
//Get row count
$num = $result->rowCount();

//Check if any posts
if($num > 0 ){
   
    $offset=$num/$limit;
    $offset=ceil($offset);
    // Post array
    $res = $post->readwithpaging($limit,$page1);
    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $res->fetch(PDO::FETCH_ASSOC))
    {
       
        extract($row);
            $post_item = array(
            'profileid'=>$profileid,
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'phone' => $phone,
            'address1'=>$address1,
            'address2'=>$address2,
            'location'=>$location,
            'sublocality'=>$sublocality,
            'landmark'=>$landmark,
            'city'=>$city,
            'district'=>$district,
            'state'=>$state,
            'offset'=>$offset
        );
        array_push($post_arr['data'], $post_item);
    }

    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
    echo json_encode(
        array('message'=>'No records')
    );
}



?>