<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));

$adminid = $data->adminid;
$workerid = $data->workerid;
$newresult = $post->getworkerservices($workerid);
$num = $newresult->rowCount();

if($num>0)
{
    
    foreach($data->deletecategory as $value)
    {
        $delete = $post->deleteworkercategory($workerid,$value);
      
        if($delete=="1")
        {
            $deleteworkerservices = $post->deleteworkerservices($workerid,$value);
        }
    }
   
    //}
    if($delete=="1")
    {
        return true;
    }
}

else
{
    
}

//selected services


?>