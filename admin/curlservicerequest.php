<?php
session_start();
include 'urlrewrite.php';
$userid = $_POST["userid"];
$catid = $_POST["catid"];
$usermessage = $_POST["usermessage"];
$service = $_POST["servicename"];
$servicedistrict = $_POST["servicedistrict"];
$address = $_POST["address"];
//$amount = $_POST["amount"];
$date = $_POST["servicedate"];
$time = $_POST["servicetime"];
$payment_type = $_POST["payment_type"];

$newdata = array("userid"=>"$userid","categoryid"=>"$catid","service"=>$service, "usermessage" =>"$usermessage", "servicedistrict" =>"$servicedistrict","servicedate"=>"$date","time"=>$time,"createdby"=>"$userid","address"=>$address,"payment_type"=>$payment_type);

//Option 1: Convert data array to json if you want to send data as json
$data = json_encode($newdata);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/addservicerequest.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$servicerequestresult = curl_exec($ch);
$result = json_decode($servicerequestresult);
//close curl connection
curl_close($ch);
//print result
// print_r($result);
if(isset($result))
{
    if($result->message=="Please select a future date")
    {
        $_SESSION["catid"] = $catid;
        $_SESSION["usermessage"] = $usermessage;
        $_SESSION["servicedistrict"] = $servicedistrict;
        $_SESSION["address"]= $address;
        $_SESSION["servicedate"] = $date;
        $_SESSION["servicetime"] = $time;
        $_SESSION["service"]= $service;
        $_SESSION["address"] = $address;
        $_SESSION["payment_type"] = $payment_type;

    }
    $newresult = $result->message;
    $_SESSION["result"] = $newresult;
    header("location:servicerequest.php");
}
?>