<?php
//date_default_timezone_set('Asia/Kolkata');
class Sendemail {

        //DB params
        public $email;
        public $workeremail;
        public $subject;
        public $service_location;
        public $servicedistrict;
        public $message;
        public $servicedate;
        public $serdate;
        public $workername;
        public $adminemail;
        public $adminname;


        //DB Conn

        public function send($email,$servicelocation,$servicedistrict,$servicedate,$assignedworker,$admemail)
        {
            $this->workername = $assignedworker;
            $this->workeremail = $email;
            $this->subject = "A new work has assigned to you";
            $this->servicelocation = $servicelocation;
            $this->servicedistrict = $servicedistrict;
            $this->serdate = $servicedate;
            $this->adminemail = $admemail;
            $this->adminname = "HI-FI Admin";
            $this->servicedate = date("d-M-Y",strtotime($this->serdate));
            $this->message = "$this->subject. at $this->servicelocation,$this->servicedistrict district.on $this->servicedate";

            $data = '{"personalizations":[{"to":[{"email":"'.$this->workeremail.'","name":"'.$this->workername.'"}],"subject":"A new service request ,for you"}],"from":{"email":"'.$this->adminemail.'","name":"'.$this->adminname.'"},"reply_to":{"email":"'.$this->adminemail.'","name":"'.$this->workername.'"},"content":[{"type":"text/plain","value":"'.$this->message.'"}]}';
            //$newdata = array("personalizations"=>array("to"=>"raphaelfrancis2002@gmail.com","name"=>"Raphael Francis"),"subject"=>"Hello World");
            //Option 1: Convert data array to json if you want to send data as json

            $ch = curl_init();
            // $data = json_encode($newdata);

            // URL to be called
            curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
            //set post TRUE to do a regular HTTP POST
            curl_setopt($ch, CURLOPT_POST, 1);
            //set http headers - if you are sending as json data (i.e. option 1) else comment this 
            //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Bearer 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'
                ));
            //send post data
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            //return as output instead of printing it
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //execute curl request
            $result = curl_exec($ch);
            $newresult = json_decode($result);
            //close curl connection
            curl_close($ch);
            return  $this->adminemail;
            

            
        }
}

?>