<?php
session_start();
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Hifive
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>

<body class="">
<?php
 include 'urlrewrite.php';
$id = $_GET["id"];
$data = array("id" =>'$id');
//Option 1: Convert data array to json if you want to send data as json
//$data = json_encode($data);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/read_single.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$newdata = json_decode($result);

//$newdata = json_decode($result);
//close curl connection
curl_close($ch);

?>

<script>

//document.write(x);
</script>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
     
    <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="../hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px" >
                  </a>
            </div>
     <?php include 'adminmenu.php';?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
     <?php include 'header.php';?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profile</h4>
                 
                  <p class="card-category">Complete your profile</p>
                </div>
                <?php
                 if(isset($_SESSION["message"]))
            {
                if($_SESSION["message"]=="Successfully Updated")
                {
                    $result = $_SESSION["message"];
                    ?>
                    <div style="color:green;text-align:center;font-size:20px;" id="testdiv"><?php echo $result;?></div>
                    <script type="text/javascript">
                       $(function()
                       {
                               $("#testdiv").delay(8000).fadeOut();
                       });
                   </script>
                    <?php
                    unset($_SESSION["message"]);
                    unset($result);
                }
                else
                {
                    $result = $_SESSION["message"];
                    ?>
                    <div style="color:red;" id="testdiv"><?php echo $result;?></div>
                    <script type="text/javascript">
                       $(function()
                       {
                               $("#testdiv").delay(8000).fadeOut();
                       });
                   </script>
                    <?php
                    unset($_SESSION["message"]);
                    unset($result);
                }
            }

                  ?>
                  <br>
                  <br>
                <?php
               
                $data=array();
                
                foreach($newdata as $key)
                {
                    $data[] = $key;
                }
                $gender = $data[7];
                $age = $data[6];
                $phone = $data[10];
                $imagelocation = $data[20];
                $imageurl = $data[21];
                ?>             
                <div class="card-body">
                  <form method = "post" action = "curledit.php" enctype="multipart/form-data">
                  <?php
                    if(isset($newdata->imagename))
                    {
                       
                        $chk = "true";
                    ?>
                    <div class="row">
                    <div class="col-md-6">
                    <?php
                    if($imagelocation=="L")
                      {
                    ?>     
                            <img src = "../images/<?php echo $newdata->imagename;?>" width = "100" height="70">
                    <?php
                      }
                      else if($imagelocation=="S")
                      {
                    ?>
                        <img src = "../images/<?php echo $imageurl;?>" width = "100" height="70">

                    <?php
                      }
                      else
                      {

                      }
                      ?>
                             
                    </div>
                    <div class="col-md-6">
                           <div class="file-field">
                              <div class="btn btn-primary btn-md">
                                   <input type="file" name="userfile">
                              </div>
                            </div>
                        </div>
                    </div>
                   <?php
                    }
                    else
                    {
                        $chk="false";
                    if($newdata->gender == "F")
                    {
                    ?>
                    <div class="row">
                    <div class="col-md-6">
                           <div class="file-field">
                           <img src = "../images/avathar1.jpg" width = "100" height="100">

                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="file-field">
                              <div class="btn btn-primary btn-md">
                                   <input type="file" name="userfile">
                              </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                  else
                    {
                    
                    ?>
                    <div class="row">
                    <div class="col-md-6">
                           <div class="file-field">
                           <img src = "../images/avatar.png" width = "100" height="100">

                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="file-field">
                              <div class="btn btn-primary btn-md">
                                   <input type="file" name="userfile">
                              </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                }
                    ?>
                  <div class ="row">

                                                    <div class="form-group">
                                                        <div class="col-md-12 col-md-offset-3">
                                                             <div id="messages">
                                                             <div id = "username_result"></div>
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>
                      <div class="row">
                          <div class="col-md-5">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Username
                                  </label>
                                  <input class="form-control" type="text" name ="username" id="username" value = "<?php print_r($newdata->username);?>" id="username">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-3">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Firstname
                                  </label>
                                  <input class="form-control" type="text" name="firstname" id="firstname" value = "<?php print_r($newdata->firstname);?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Lastname
                                  </label>
                                  <input class="form-control" type="text" name="lastname" id="lastname" value = "<?php print_r($newdata->lastname);?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Email
                                  </label>
                                  <input class="form-control" type="email" name ="email" id="email" value = "<?php print_r($newdata->email);?>">
                                  </input>
                              </div>
                          </div>
                          
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Age
                                  </label>
                                  <input class="form-control" type="text" name="age" id="age" value = "<?php echo $newdata->age;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                         
                          <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioOptions" value = "M " <?php if($newdata->gender=='M') echo 'checked="checked"';?>> Male
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                       
                                          
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadioOptions" value = "F" <?php if($newdata->gender=='F') echo 'checked="checked"';?>>Female
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Phone
                                  </label>
                                  <input class="form-control" type="text" name="phone" id="phone" value = "<?php echo $newdata->phone;?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Address1
                                  </label>
                                  <input class="form-control" type="text" name ="address1" id="address1" value="<?php print_r($newdata->address1);?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Address2
                                  </label>
                                  <input class="form-control" type="text" name = "address2" id="address2" value="<?php print_r($newdata->address2);?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      location
                                  </label>
                                  <input class="form-control" type="text" name="location" id="location" value = "<?php print_r($newdata->location);?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          sub locality
                                      </label>
                                      <input class="form-control" type="text" name="sublocality" id="sublocality" value = "<?php print_r($newdata->sublocality);?>">
                                      </input>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          Landmark
                                      </label>
                                      <input class="form-control" type="text" name="landmark" id="landmark" value = "<?php print_r($newdata->landmark);?>" >
                                      </input>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          city
                                      </label>
                                      <input class="form-control" type="text" name="city" id="city" value = "<?php print_r($newdata->city);?>">
                                      </input>
                                  </div>
                              </div>
                          </div>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      District
                                  </label>
                                  <select name="district" id="district" class="browser-default custom-select">
                                                      <option   <?php if ($newdata->district == "Thiruvanathapuram" ) echo 'selected';?> value="Thiruvanathapuram">Thiruvanathapuram</option>
                                                      <option   <?php if ($newdata->district == "Kollam" ) echo 'selected' ; ?> value="Kollam">Kollam</option>
                                                      <option   <?php if ($newdata->district == "Alappuzha" ) echo 'selected' ; ?> value="Alappuzha">Alappuzha</option>
                                                      <option   <?php if ($newdata->district == "Pathanamthitta" ) echo 'selected' ; ?> value="Pathanamthitta">Pathanamthitta</option>
                                                      <option   <?php if ($newdata->district == "Kottayam" ) echo 'selected' ; ?> value="Kottayam">Kottayam</option>
                                                      <option   <?php if ($newdata->district == "Idukki" ) echo 'selected' ; ?> value="Idukki">Idukki</option>
                                                      <option   <?php if ($newdata->district == "Ernakulam" ) echo 'selected' ; ?> value="Ernakulam">Ernakulam</option>
                                                      <option   <?php if ($newdata->district == "Thrissur" ) echo 'selected' ; ?> value="Thrissur">Thrissur</option>
                                                      <option   <?php if ($newdata->district == "Malappuram" ) echo 'selected' ; ?> value="Malappuram">Malappuram</option>
                                                      <option   <?php if ($newdata->district == "Kozhikode" ) echo 'selected' ; ?> value="Kozhikode">Kozhikode</option>
                                                      <option   <?php if ($newdata->district == "Palakkad" ) echo 'selected' ; ?> value="Palakkad">Palakkad</option>
                                                      <option   <?php if ($newdata->district == "Kannur" ) echo 'selected' ; ?> value="Kannur">Kannur</option>
                                                      <option   <?php if ($newdata->district == "Wayanad" ) echo 'selected' ; ?> value="Wayanad">Wayanad</option>
                                                      <option   <?php if ($newdata->district == "Kasaragod" ) echo 'selected' ; ?> value="Kasaragod">Kasaragod</option>
                                                      </select>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     State
                                  </label>
                                  <input class="form-control" type="text" id="state" name="state" value = "<?php print_r($newdata->state);?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                    
                   
                      <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                          Latitude
                                      </label>
                                      <input class="form-control" type="text">
                                      </input>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="bmd-label-floating">
                                         Longitude
                                      </label>
                                      <input class="form-control" type="text">
                                      </input>
                                  </div>
                              </div>
                          </div>
                    <input type = "hidden" name="chk" value="<?php echo $chk;?>">
                    <input type = "hidden" name = "oldimage" value = "<?php if(isset($newdata->imagename)){echo $newdata->imagename;}?>">
                    <input type = "hidden" name = "idimage" value = "<?php if(isset($newdata->idimage)){echo $newdata->idimage;}?>">
                    <input type="hidden" name ="idprofile" value= "<?php print_r($newdata->idprofiles);?>">
                    <button type="submit" id="submit" class="btn btn-primary pull-right">Update Profile</button>
                    <div class="clearfix"></div>
                  </form>
                 
                </div>
                

              </div>
            </div>
                     
            </div> 
          </div>
          

            <?php include 'footer.php';?>


        </div>
      </div>
      <script src="../assets/js/adduser.js">
</script>
              <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
              <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
              <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
              <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
              <!--  Google Maps Plugin    -->
              <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
              <!-- Chartist JS -->
              <script src="../assets/js/plugins/chartist.min.js"></script>
              <!--  Notifications Plugin    -->
              <script src="../assets/js/plugins/bootstrap-notify.js"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
             
                </div>
              </div>
  <!--   Core JS Files   -->
  <script type="text/javascript">
 $(document).ready(function(){
  $('#username').keyup(function(){
    var username = $('#username').val();
    var jsonData = {username:username};
    $.ajax({
     type: "POST",
     url: "checkusername.php",
     data: JSON.stringify(jsonData),
     dataType: "json",
     success: function(data)
     {
         
        $('#username').val("");
        $('#username_result').html('This person has already registered');
        $('#username_result').css('color','red');
     }
      
    }); //ajax ends
   
  });//user name change function ends
 }); //document ready function ends
</script> 
 
</body>

</html>