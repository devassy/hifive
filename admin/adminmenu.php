
   
      <div class="sidebar-wrapper">
        <ul class="nav">
                        
                        <li class="nav-item  ">
                            <a class="nav-link" href="addcategory.php">
                                <i class="material-icons">
                                    supervised_user_circle
                                </i>
                                <p>
                                   Add Categories
                                </p>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a class="nav-link" href="viewcategories.php">
                                <i class="material-icons">
                                    view_module
                                </i>
                                <p>
                                   View Categories
                                </p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="addservices.php">
                                <i class="material-icons">
                                    addservices
                                </i>
                                <p>
                                Add services
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="viewservices.php">
                                <i class="material-icons">
                                   Add services
                                </i>
                                <p>
                                View Services
                                </p>
                            </a>
                        </li>
                       
                        <li class="nav-item  ">
                            <a class="nav-link" href="adduser.php">
                                <i class="material-icons">
                                    +person
                                </i>
                                <p>
                                    Add Users
                                </p>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a class="nav-link" href="listusers.php">
                                <i class="material-icons">
                                    person
                                </i>
                                <p>
                                List Users
                                </p>
                            </a>
                        </li>
                       
                        <li class="nav-item  ">
                            <a class="nav-link" href="servicerequest.php">
                                <i class="material-icons">
                                group_add
                                </i>
                                <p>
                            Add service request
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="listrequest.php">
                                <i class="material-icons">
                                List
                                </i>
                                <p>
                                View Service Request
                                </p>
                            </a>
                        </li>
                        
                        <li class="nav-item  ">
                            <a class="nav-link" href="viewworkerservices.php">
                                <i class="material-icons">
                                    work
                                </i>
                                <p>
                            view Worker services
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="viewtransaction.php">
                            <i class="material-icons">
                            attach_money
                            </i>
                        
                                <p>
                            View Transaction
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="addrewards.php">
                            <i class="material-icons">
                            +
                            </i>
                                <p>
                            Rewards
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="viewrewards.php">
                            <i class="material-icons">
                            View
                            </i>
                                <p>
                            Rewards
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="summary.php">
                            <i class="material-icons">
                            Summary
                            </i>
                                <p>
                            Summary
                                </p>
                            </a>
                        </li>
                       
                        <li class="nav-item ">
                            <a class="nav-link" href="settings.php">
                                <i class="material-icons">
                                    settings
                                </i>
                                <p>
                                    Settings
                                </p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="logout.php">
                                <i class="material-icons">
                                    Logout
                                </i>
                                <p>
                                    Logout
                                </p>
                            </a>
                        </li>
                    </ul>
      </div>
    
