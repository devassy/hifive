$(document).ready(function() {
$('#submit').click(function(e) {
// Initializing Variables With Form Element Values
var service_name = $('#servicename').val();
var cat_name = $('#cat_name').val();

// Initializing Variables With Regular Expressions
var name_regex = /^[a-zA-Z]+$/;
var add_regex = /[A-Za-z0-9'\.\-\s\,]/;

// To Check Empty Form Fields.

if( !$('#cat_name').val() ) { 
$('#messages').html("<p>* Please fill the mandatory fields *</p>"); 
$("#cat_name").focus();
return false;
}



// Validating service name Field.
else if (!service_name.match(add_regex) || service_name.length == 0) {
$('#messages').html("<p>* Please provide service name name - contain alphabets only *</p>"); 
$("#servicename").focus();
return false;
}

else {

return true;
}
});
});