$(document).ready(function() {
$('#submit').click(function(e) {

// Initializing Variables With Form Element Values
var currentpassword = $('#currentpassword').val();
var newpassword = $('#newpassword').val();
var retypenewpassword = $('#retypenewpassword').val();


// Validating current password Field.
if ( currentpassword.length == 0) {
$('#messages').html("<p>* Please enter current password *</p>"); 
$("#currentpassword").focus();
return false;
}

// Validating new password Field.
else if (newpassword.length == 0) {
$('#messages').html("<p>* Please enter new password *</p>"); 
$("#newpassword").focus();
return false;
}


// Validating retype password Field.
else if (retypenewpassword.length == 0) {

$('#messages').html("<p>* Please re-enter new password *</p>");
$("#retypenewpassword").focus();
return false;
}

//validating confirm password field.

else if(newpassword!= retypenewpassword) {
	$('#messages').html("<p>* Passwords does not match *</p>");
	$("#retypenewpassword").focus();
	return false;	
}


else {
//alert("Form Submitted Successfuly!");
//return true;
}
});
});
//document ready function ends