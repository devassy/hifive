$(document).ready(function() {
$('#submit').click(function(e) {
// Initializing Variables With Form Element Values
var type = $('#type').val();
var amount = $('#amount').val();

// Initializing Variables With Regular Expressions
var name_regex = /^[0-9]+$/;



// Validating payment type.
if( !$('#type').val() ) { 
$('#messages').html("<p>* Please provide a payment type *</p>"); 
$("#cat_name").focus();
return false;
}

// Validating type Field.
else if (!amount.match(name_regex) || type.length == 0) {
$('#messages').html("<p>* Please provide payment amount*</p>"); 
return false;
}
else {
//alert("Form Submitted Successfuly!");
return true;
}
});
});