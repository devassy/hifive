$(document).ready(function() {
$('#submit').click(function(e) {
// Initializing Variables With Form Element Values
var service_name = $('#servicename').val();

// Initializing Variables With Regular Expressions
var add_regex = /[A-Za-z0-9'\.\-\s\,]/;




// Validating service name Field.
if (!service_name.match(add_regex) || service_name.length == 0) {
$('#messages').html("<p>* Please provide service name name - contain alphabets only *</p>"); 
$("#servicename").focus();
return false;
}

else {

return true;
}
});
});