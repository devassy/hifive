$(document).ready(function() {
$('#submit').click(function(e) {

// Initializing Variables With Form Element Values
var catid = $('#catid').val();
var servicename = $('#servicename').val();
var message = $('#message').val();
var location = $('#location').val();
var address = $('#address').val();
var amount = $('#amount').val();
var type = $('#type').val();
var date = $('#date').val();
var time = $('#time').val();
// Initializing Variables With Regular Expressions
var name_regex = /^[a-zA-Z]+$/;
// var add_regex = /^[0-9a-zA-Z]+$/;
var add_regex = /^.{1,150}$/;


var phone_regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
// var msg_regex = /^[a-zA-Z]+[a-zA-Z0-9._]+$/;
//var msg_regex =/^[-@./#&+\w\s]*$/;

var amt_regex =/^\$?[0-9]?((\.[0-9]+)|([0-9]+(\.[0-9]+)?))$/;
// To Check Empty Form Fields.
if( !$('#catid').val()||$('#catid').val() == "Please select a Category" ) { 
	$('#messages').html("<p>* Please fill the mandatory fields *</p>"); 
$("#cat_id").focus();
return false;
}
// Validating service Field.
else if( !$('#servicename').val() ) { 
$('#messages').html("<p>* Uservices field is empty*</p>"); 
return false;
}
// Validating message Field.

else if (!$.trim($("#message").val())) {
		
$('#messages').html("<p>* Please provide a valid message *</p>");
$("#message").focus();
return false;
}
// Validating location Field.
else if (!location.match(name_regex) || location.length == 0) {
$('#messages').html("<p>* Please provide location*</p>"); 
$("#location").focus();
return false;
}

// Validating address Field.
else if (!$.trim($("#address").val())) {
		
	$('#messages').html("<p>* Please provide a valid message *</p>");
	$("#address").focus();
	return false;
	}


// Validating type Field.
else if (!type.match(name_regex) || type.length == 0) {
$('#messages').html("<p>* Please enter type of amount *</p>"); 
$("#type").focus();
return false;
}

// Validating amount Field.
else if (!amount.match(amt_regex) || amount.length == 0) {

$('#messages').html("<p>* Please enter amount*</p>");
$("#amount").focus();
return false;
} 



// Validating date Field.
else if (date.length == 0) {
$('#messages').html("<p>* Please enter a valid date*</p>"); 
$("#date").focus();
return false;
}

// Validating time Field.
else if ( time.length == 0) {
$('#messages').html("<p>* Please enter valid time*</p>"); 
$("#time").focus();
return false;
}

else {
//alert("Form Submitted Successfuly!");
//return true;
}
});
});
//document ready function ends