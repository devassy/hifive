$(document).ready(function() {
$('#submit').click(function(e) {
// Initializing Variables With Form Element Values
var cat_name = $('#cat_name').val();
// Initializing Variables With Regular Expressions
var name_regex = /^[a-zA-Z]+$/;



// Validating category name Field.
if (!cat_name.match(name_regex) || cat_name.length == 0) {
$('#messages').html("<p>* Please provide category name - contain alphabets only *</p>"); 
$("#cat_name").focus();
return false;
}

else {

return true;
}
});
});