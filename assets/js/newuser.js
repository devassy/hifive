$(document).ready(function() {
$('#submit').click(function(e) {

// Initializing Variables With Form Element Values
var username = $('#username').val();
var email = $('#useremail').val();
var password = $('#password').val();
var confirm_password = $('#confirmpassword').val();
var phone = $('#phone').val();

// Initializing Variables With Regular Expressions
var name_regex = /^[a-zA-Z]+$/;
var email_regex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
var phone_regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
// To Check Empty Form Fields.
if (username.length == 0) {
$('#messages').html("<p>* Please fill all the mandatory fields *</p>"); 
$("#username").focus();
return false;
}
// Validating Username Field.
else if (!username.match(name_regex) || username.length == 0) {
$('#messages').html("<p>* Username can only contail alphabets and numbers *</p>"); 
return false;
}

// Validating password Field.
else if (!(password.length >= 6 )|| password.length == 0) {
$('#messages').html("<p>* Please enter Password - should be atleast 6 characters length *</p>"); 
$("#password").focus();
return false;
}

//validating confirm password field.

else if(!confirm_password.length>=6&&confirm_password.length<=8 || confirm_password.length == 0) {
	$('#messages').html("<p>*please enter confirm password*</p>");
	$("#confirm_password").focus();
	return false;

}
else if(password!= confirm_password) {
	$('#messages').html("<p>*Passwords does not match*</p>");
	$("#confirm_password").focus();
	return false;	
}


// Validating Email Field.
else if (!email.match(email_regex) || email.length == 0) {
$('#messages').html("<p>* Please enter a valid email address *</p>"); 
$("#useremail").focus();
return false;
}


// Validating phone Field.
else if (!phone.match(phone_regex) || phone.length == 0) {

$('#messages').html("<p>* Please enter a valid phone number*</p>");
$("#phone").focus();
return false;
} 


else {

return true;
}
});
});
//document ready function ends