$(document).ready(function() {
$('#submit').click(function(e) {

// Initializing Variables With Form Element Values
var catid = $('#catid').val();
var servicename = $('#servicename').val();
var workerid = $('#workerid').val();
var message = $('#message').val();
var location = $('#location').val();
var address = $('#address').val();
var amount = $('#amount').val();
var payment_type = $('#payment_type').val();
var date = $('#servicedate').val();
var time = $('#servicetime').val();
// Initializing Variables With Regular Expressions
var name_regex = /^[a-zA-Z]+$/;
var add_regex = /^[0-9a-zA-Z]+$/;
var phone_regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
var msg_regex = /^[a-zA-Z]+[a-zA-Z0-9._]+$/;
var amt_regex =/^\$?[0-9]?((\.[0-9]+)|([0-9]+(\.[0-9]+)?))$/;
// To Check Empty Form Fields.
if( $('#catid').val() == "Please select a Category" ) { 
	$('#messages').html("<p>* Please fill the mandatory fields *</p>"); 
$("#catid").focus();
return false;
}
// Validating service Field.
else if( !$('#servicename').val() ) { 
$('#messages').html("<p>* Services field is empty*</p>"); 
return false;
}
//validating workername field
else if( $('#workerid').val() == "Please select a Worker" ) { 
	$('#messages').html("<p>* Please select worker name *</p>"); 
$("#workerid").focus();
return false;
}
// Validating message Field.
else if (!$.trim($("#message").val())) {
$('#messages').html("<p>* Please provide a valid message *</p>");
$("#message").focus();
return false;
}
// Validating location Field.
else if (!location.match(name_regex) || location.length == 0) {
$('#messages').html("<p>* Please provide location*</p>"); 
$("#location").focus();
return false;
}

// Validating address Field.
else if (!$.trim($("#message").val())) {
	$('#messages').html("<p>* Please provide a valid message *</p>");
	$("#message").focus();
	return false;
	}


// Validating amount Field.
else if (!amount.match(amt_regex) || amount.length == 0) {

$('#messages').html("<p>* Please enter amount*</p>");
$("#amount").focus();
return false;
} 



// Validating date Field.
else if (date.length == 0) {
$('#messages').html("<p>* Please enter a valid date*</p>"); 
$("#servicedate").focus();
return false;
}

// Validating time Field.
else if ( time.length == 0) {
$('#messages').html("<p>* Please enter valid time*</p>"); 
$("#servicetime").focus();
return false;
}

else {
//alert("Form Submitted Successfuly!");
//return true;
}
});
});
//document ready function ends