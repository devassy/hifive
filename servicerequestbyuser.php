<?php
session_start();
if(($_SESSION["type"]=='U')&&($_SESSION["idprofiles"]))
{
include 'urlrewrite.php';
$userid = $_SESSION["userid"];

if(isset($_SESSION["singleworkerid"]))
{
$singleworkerid = $_SESSION["singleworkerid"];
$data = array("id"=>$singleworkerid);
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/read_singleworker.php?id=$singleworkerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$singleworkerlist = curl_exec($ch);
$singleworkerdata = json_decode($singleworkerlist);

//$newdata = json_decode($result);
//close curl connection
curl_close($ch);

}
//if the request from login user search
else if(isset($_GET["id"]))
{ 
$singleworkerid = $_GET["id"];
$data = array("id"=>$singleworkerid);
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/read_singleworker.php?id=$singleworkerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$singleworkerlist = curl_exec($ch);
$singleworkerdata = json_decode($singleworkerlist);

//$newdata = json_decode($result);
//close curl connection
curl_close($ch);

}
else
{

$data = array("id"=>1);
//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/worker.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$workerlist = curl_exec($ch);
$workerdata = json_decode($workerlist);

//$newdata = json_decode($result);
//close curl connection
curl_close($ch);
}
if(isset($_SESSION["singleworkerid"]))
{
$workerid = $_SESSION["singleworkerid"];
$ch = curl_init();
$data = array("idcategory","1");
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getselectedworker_categories.php?id=$workerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
//close curl connection
curl_close($ch);
$getselectedworkercategory = json_decode($result);

}


else if(isset($_GET["id"]))
{
    $workerid = $_GET["id"];
$ch = curl_init();
$data = array("idcategory","1");
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getselectedworker_categories.php?id=$workerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
//close curl connection
curl_close($ch);
$getselectedworkercategory = json_decode($result);

}
else
{
    $ch = curl_init();
    $data = array("idcategory","1");
    // URL to be called
    curl_setopt($ch, CURLOPT_URL, "$url/getallcategories.php");
    //set post TRUE to do a regular HTTP POST
    curl_setopt($ch, CURLOPT_POST, 1);
    //set http headers - if you are sending as json data (i.e. option 1) else comment this 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //send post data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //return as output instead of printing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute curl request
    $result = curl_exec($ch);
    //close curl connection
    curl_close($ch);
    $getcategory = json_decode($result);
}
//print result
?>




<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
            </link>
        </link>
    </head>
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <div class="logo">
                   <a class="simple-text logo-normal" href="welcomeuser.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
                <div class="sidebar-wrapper">
                     <?php include 'usermenu.php';?>
                        
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title">
                                            Add Service Request
                                        </h4>
                                        
                                    </div>
                                    <?php
                                        if(isset($_SESSION["message"]))
                                        {

                                             $result = $_SESSION["message"];
                                            if($result=="Service Request Sent Successfully")
                                            {
                                                
                                            ?>
                                            <div style="color:green;" id = "testdiv"><?php echo $result;?></div>
                                            <script type="text/javascript">
                                               $(function()
                                               {
                                                       $("#testdiv").delay(7000).fadeOut();
                                               });
                                           </script>
                                            <?php
                                            unset($_SESSION["message"]);
                                            unset($result);
                                            }
                                            else if($result=="Request Not Submitted")
                                            {
                                               
                                                ?>
                                                <div style="color:red;" id = "testdiv"><?php echo $result;?></div>
                                                <script type="text/javascript">
                                                   $(function()
                                                   {
                                                           $("#testdiv").delay(10000).fadeOut();
                                                   });
                                               </script>
                                                <?php
                                                unset($_SESSION["message"]);
                                                unset($result);
                                            }
                                            else if($result=="Please select a future date")
                                            {
                                                $servicedistrict = $_SESSION["servicedistrict"];
                                                $servicetime = $_SESSION["servicetime"];
                                                $usermessage = $_SESSION["usermessage"];
                                                $catid = $_SESSION["catid"];
                                                $service_location = $_SESSION["service_location"];
                                                $payment_type = $_SESSION["payment_type"];
                                                $servicedate =  $_SESSION["servicedate"];
                                                $servicename = $_SESSION["servicename"];
                                               
                                                ?>
                                                <span style = "padding-right:10px;"></span><div style="color:red;" id = "testdiv"><?php echo $result;?></div>
                                                <script type="text/javascript">
                                                   $(function()
                                                   {
                                                           $("#testdiv").delay(10000).fadeOut();
                                                   });
                                               </script>
                                                <?php
                                                unset($_SESSION["message"]);
                                                unset($result);
                                            }
                                            else
                                            {
                                                
                                            }
                                           
                                        }
                                        else
                                        {


                                        }
                                        ?>
                                    
                                    <div class="card-body">
                                   
                                        <form name = "f1" method="post" action="curlservicerequest.php">


                                            <div class ="row">

                                                    <div class="form-group">
                                                        <div class="col-md-12 col-md-offset-3">
                                                             <div id="messages">
                                                                 
                                                             </div>
                                                         </div>
                                                    </div>
                                                </div>


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                            Category name
                                                        </label>
                                                      <select name="catid" id="catid" class="browser-default custom-select">
                                                      <option>Please select a Category</option>
                                                           <?php
                                                           if (isset($getcategory))
                                                           {
                                                            foreach($getcategory as $value)
                                                            {
                                                              foreach($value as $data)
                                                              {
                                                              ?>
                                                           
                                                           <option <?php if(isset($catid)){if($catid=="$data->idcategory"){echo 'selected';}}?> value ="<?php echo $data->idcategory;?>"><?php echo $data->categoryname;?></option>
                                                             <?php
                                                              }
                                                            }
                                                           }
                                                           else if(isset($getselectedworkercategory))
                                                           {
                                                            foreach($getselectedworkercategory as $data)
                                                              {
                                                              ?>
                                                           
                                                           <option <?php if(isset($catid)){if($catid=="$data->idcategory"){echo 'selected';}}?> value ="<?php echo $data->idcategory;?>"><?php echo $data->categoryname;?></option>
                                                             <?php
                                                              }

                                                           }
                                                           else
                                                           {

                                                           }
                                                           ?>
                                                       </select>
                                                    </div>
                                                </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Service 
                                                        </label>
                                                        <select class="browser-default custom-select" name="servicename" id = "servicename">
                                                           <option value='serviceid'></option>
                                                        </select>
                                                    
                                                        <!-- <input class="form-control" type="text" id="service_name" name="service_name">
                                                        </input>
                                                    </div> -->
                                                </div>
                                                </div>
                                            </div>

                                            
                                            
                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                                message
                                                            </label>
                                                        <textarea class="form-control" rows = "4" cols = "90" name="usermessage" id="message"><?php if(isset($usermessage)){echo $usermessage;}?></textarea> 
                                                        <!-- <input class="form-control" type="text" name="cat_name" id="cat_name">
                                                        </input> -->
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="bmd-label-floating">
                                                            District
                                                            </label>
                                                            <select name="servicedistrict" id="payment_type" class="browser-default custom-select">
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Thiruvananthapuram"){echo 'selected';}}?> value ="Thiruvananthapuram">Thiruvananthapuram</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Kollam"){echo 'selected';}}?> value ="	Kollam">Kollam</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Alappuzha"){echo 'selected';}}?> value ="Alappuzha">Alappuzha</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Pathanamthitta"){echo 'selected';}}?> value ="Pathanamthitta">Pathanamthitta</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Kottayam"){echo 'selected';}}?> value = "Kottayam">Kottayam</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Idukki"){echo 'selected';}}?> value ="Idukki">Idukki</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Ernakulam"){echo 'selected';}}?> value ="Ernakulam">Ernakulam</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Thrissur"){echo 'selected';}}?> value ="Thrissur">Thrissur</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Palakkad"){echo 'selected';}}?> value ="Palakkad">Palakkad</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Malappuram"){echo 'selected';}}?> value ="Malappuram">Malappuram</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Kozhikode"){echo 'selected';}}?> value ="Kozhikode">Kozhikode</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Kannur"){echo 'selected';}}?> value ="Kannur">Kannur</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Wayanad"){echo 'selected';}}?> value ="Wayanad">Wayanad</option>
                                                      <option  <?php if(isset($servicedistrict)){if($servicedistrict=="Kasaragod"){echo 'selected';}}?> value ="Kasaragod">Kasaragod</option>
                                                      </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="bmd-label-floating">
                                                                Address
                                                            </label>
                                                            <textarea class="form-control" rows = "2" cols = "90" name="service_location" id="address"><?php if(isset($service_location)){echo $service_location;}?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="bmd-label-floating">
                                                                Payment type
                                                            </label>
                                                            <select name="payment_type" id="payment_type" class="browser-default custom-select">
                                                      <option  <?php if(isset($payment_type)){if($payment_type=="C"){echo 'selected';}}?> value ="C">Cash</option>
                                                      <option   <?php if(isset($payment_type)){if($payment_type=="O"){echo 'selected';}}?> value ="O">Online</option>
                                                      </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                        
                                                            
                                                            <input class="form-control" type="date" name="servicedate" id="servicedate" value="<?php if(isset($servicedate)){echo $servicedate;}?>">
                                                            </input>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            
                                                        <input placeholder="Time" class="form-control textbox-n" type="text" onfocus="(this.type='time')"  id="servicetime" name="servicetime" value="<?php if(isset($servicetime)){echo $servicetime;}?>">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            <input type = "hidden" name = "singleworkerid" value = "<?php if(isset($singleworkerid)){echo $singleworkerid;}?>">
                                            <input type = "hidden" name = "userid" value = "<?php echo $userid;?>">
                                            <button class="btn btn-primary pull-right" type="submit" id="submit" name="submit">
                                                Request
                                            </button>
                                                    <table id= "mytable">
                                                    </table>    
                                            <div class="clearfix">
                                            </div>
                                        </form>
                                        <?php
                                        if(isset($_SESSION["message"]))
                                        {

                                             $result = $_SESSION["message"];
                                            if($result=="Service Request Sent Successfully")
                                            {
                                                
                                            ?>
                                            <div style="color:green;" id = "testdiv"><?php echo $result;?></div>
                                            <script type="text/javascript">
                                               $(function()
                                               {
                                                       $("#testdiv").delay(7000).fadeOut();
                                               });
                                           </script>
                                            <?php
                                            unset($_SESSION["message"]);
                                            unset($result);
                                            }
                                            else if($result=="Request Not Submitted")
                                            {
                                               
                                                ?>
                                                <div style="color:red;" id = "testdiv"><?php echo $result;?></div>
                                                <script type="text/javascript">
                                                   $(function()
                                                   {
                                                           $("#testdiv").delay(10000).fadeOut();
                                                   });
                                               </script>
                                                <?php
                                                unset($_SESSION["message"]);
                                                unset($result);
                                            }
                                            else if($result=="Please select a future date")
                                            {
                                               
                                                ?>
                                                <div style="color:red;" id = "testdiv"><?php echo $result;?></div>
                                                <script type="text/javascript">
                                                   $(function()
                                                   {
                                                           $("#testdiv").delay(10000).fadeOut();
                                                   });
                                               </script>
                                                <?php
                                                unset($_SESSION["message"]);
                                                unset($result);
                                            }
                                            else
                                            {
                                                
                                            }
                                           
                                        }
                                        else
                                        {


                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>

                   <?php include 'footer.php';?>

                </div>
            </div>
            <!--   Core JS Files   -->
<script src="assets/js/core/jquery.min.js" type="text/javascript">
</script>
<script src="assets/js/core/popper.min.js" type="text/javascript">
</script>
<script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
</script>
<script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
</script>

<!-- add services vaidation -->
<script src="asset/js/addworkermessage.js"></script>

<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
</script>
<!-- Chartist JS -->
<script src="assets/js/plugins/chartist.min.js">
</script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js">
</script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
</script>


        </div>
    </body>
    <script type="text/javascript">
  $(document).ready(function() {

      $('#catid').change(function(e){
        var categoryid = $("#catid").val();
        var jsonData = {categoryid:categoryid};
        $.ajax({
            type: "POST",
            data: JSON.stringify(jsonData),
            url: 'getservice.php',
            dataType: "json",
            success : function(data){
                if (data)
                {
                    $('#servicename').children().remove();
                    var person = JSON.stringify(data['data'][0]);
                    for(var i=0;i<person.length;i++)
                    {
                        var stddata = JSON.stringify(data['data'][i]);
                        var json = JSON.parse(stddata);
                        var servicetype = json["servicename"];
                        var serviceid = json["idservices"];
                        $('#servicename').append($('<option>').text(servicetype).attr('value',serviceid));
                        
                    }
                    
                    
                }
                else {
                    alert("error");
                }
            }
        });


      });
  });
</script>
<script type="text/javascript">
   $(document).ready(function() {

   
        var categoryid = $('select').find('option:selected').val();

  var jsonData = {categoryid:categoryid};
  $.ajax({
      type: "POST",
      data: JSON.stringify(jsonData),
      url: 'getservice.php',
      dataType: "json",
      success : function(data){
          if (data)
          {
              $('#servicename').children().remove();
              var person = JSON.stringify(data['data'][0]);
              for(var i=0;i<person.length;i++)
              {
                  var stddata = JSON.stringify(data['data'][i]);
                  var json = JSON.parse(stddata);
                  var servicetype = json["servicename"];
                  var serviceid = json["idservices"];
                  $('#servicename').append($('<option>').text(servicetype).attr('value',serviceid));
                  
              }
              
              
          }
          else {
              alert("error");
          }
      }
  });


});

  
</script>
</html>     
<?php
}
else
{
    header("userlogin.php");
}
?>