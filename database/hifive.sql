-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2018 at 01:48 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hifive`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `idapplication` int(11) NOT NULL,
  `applicationname` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - Active, ''0''- Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`idapplication`, `applicationname`, `status`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(123456, 'hifive', '1', '2018-10-04 00:00:00', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `application_members`
--

CREATE TABLE `application_members` (
  `idapplication_member` bigint(20) NOT NULL,
  `datafieldid` bigint(20) NOT NULL,
  `applicationid` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `application_settings`
--

CREATE TABLE `application_settings` (
  `idapplication_setting` bigint(20) NOT NULL,
  `applicationid` int(11) NOT NULL,
  `apiprivatekey` varchar(255) DEFAULT NULL,
  `apipublickey` varchar(255) DEFAULT NULL,
  `smsgayteway_token` varchar(255) DEFAULT NULL,
  `paypalkey` varchar(255) DEFAULT NULL,
  `emailkey` varchar(255) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - Active, ''0''- Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `idcategory` bigint(20) NOT NULL,
  `applicationid` int(11) NOT NULL,
  `categoryname` varchar(255) NOT NULL,
  `other_category` varchar(255) DEFAULT NULL,
  `parentcategory` bigint(20) NOT NULL DEFAULT '0' COMMENT '''0'' - Parent Category',
  `type` varchar(50) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1' COMMENT '''1'' - ACTIVE, ''0'' - INACTIVE',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`idcategory`, `applicationid`, `categoryname`, `other_category`, `parentcategory`, `type`, `status`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(2704138695, 123456, 'Civil', NULL, 0, NULL, '1', '2018-11-27 03:31:07', 2031958674, NULL, NULL),
(2718360945, 123456, 'Plumbing', NULL, 0, NULL, '1', '2018-11-27 07:22:39', 2031958674, NULL, NULL),
(2834591076, 123456, 'Mechanical', NULL, 0, NULL, '1', '2018-11-27 02:56:59', 2031958674, '2018-11-27 03:30:36', 2031958674),
(3180756249, 123456, 'Electronics', NULL, 0, NULL, '1', '2018-11-27 07:14:13', 2031958674, NULL, NULL),
(5034162897, 123456, 'IT', NULL, 0, NULL, '1', '2018-11-27 07:17:20', 2031958674, NULL, NULL),
(7642189305, 123456, 'Electrical', NULL, 0, NULL, '1', '2018-11-27 07:12:50', 2031958674, NULL, NULL),
(8390157264, 123456, 'Chemical', NULL, 0, NULL, '1', '2018-11-27 07:15:52', 2031958674, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_fields`
--

CREATE TABLE `data_fields` (
  `iddata_field` bigint(20) NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `status` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `field_application`
--

CREATE TABLE `field_application` (
  `idfield_application` bigint(20) NOT NULL,
  `applicationmemberid` bigint(20) NOT NULL,
  `is_visible_user` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_editable_user` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_addable_user` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_visible_admin` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_editable_admin` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_addable_admin` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_visible_worker` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_editable_worker` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `is_addable_worker` enum('1','0') DEFAULT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `idimage` bigint(200) NOT NULL,
  `idprofile` bigint(20) NOT NULL,
  `idprofile_addressid` bigint(20) DEFAULT NULL,
  `imagename` varchar(255) NOT NULL,
  `imagetype` varchar(25) NOT NULL,
  `imagelocation` enum('L','S','O') NOT NULL COMMENT '''L'' - local, ''S'' - s3 bucket, ''O'' - Other location',
  `imageurl` text,
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '''1'' - Yes, ''0'' - No',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - ACTIVE, ''0'' - INACTIVE',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`idimage`, `idprofile`, `idprofile_addressid`, `imagename`, `imagetype`, `imagelocation`, `imageurl`, `deleted`, `status`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(28405310679, 1740695328, NULL, 'ewqor5hChrysanthemum.jpg', '', 'L', NULL, '0', '1', '2018-11-20 18:50:48', 1740695328, '2018-12-05 17:38:15', 1740695328),
(56840319027, 2031958674, NULL, 'j1768cuTulips.jpg', '', 'L', NULL, '0', '1', '2018-11-22 16:44:53', 2031958674, '2018-11-22 16:45:05', NULL),
(65084027319, 7943652018, NULL, 'dimpz03Lighthouse.jpg', '', 'L', NULL, '0', '1', '2018-11-27 14:25:42', 7943652018, NULL, NULL),
(71234060859, 8527610493, NULL, '693qy1vJellyfish.jpg', '', 'L', NULL, '0', '1', '2018-12-11 09:39:05', 8527610493, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `idmessage` bigint(20) NOT NULL,
  `senderid` bigint(20) NOT NULL,
  `servicrequestid` bigint(20) NOT NULL,
  `recieverid` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `subject` varchar(255) NOT NULL,
  `forwarded` bigint(20) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - ACTIVE, ''0'' - INACTIVE',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `other_categories`
--

CREATE TABLE `other_categories` (
  `idother_category` bigint(20) NOT NULL,
  `categoryname` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `idplace` bigint(20) NOT NULL,
  `idplace_duplicate` bigint(20) DEFAULT NULL,
  `placename` varchar(255) NOT NULL,
  `city` varchar(150) NOT NULL,
  `district` varchar(150) NOT NULL,
  `state` varchar(150) NOT NULL,
  `status` enum('1','0') DEFAULT '1' COMMENT '''1'' - ACTIVE, ''0'' - INACTIVE',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`idplace`, `idplace_duplicate`, `placename`, `city`, `district`, `state`, `status`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(125125, NULL, 'ollur', 'Thrissur', 'Thrissur', 'Kerala', '1', '2018-10-05 00:00:00', 0, NULL, NULL),
(126126, NULL, 'Thrissur', 'Thrissur', 'Thrissur', 'Kerala', '1', '2018-10-09 00:00:00', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `idprofiles` bigint(20) NOT NULL,
  `type` enum('U','A','W') NOT NULL DEFAULT 'U' COMMENT '''U'' - User, ''A'' - Admin, ''W'' - Worker',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `paymenttype` enum('C','O','B') DEFAULT 'C' COMMENT '''C'' - Cash, ''O'' - Online, ''B'' - Both ',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - Active, ''0'' - Inactive',
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '''1'' - Yes, ''0'' - No',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`idprofiles`, `type`, `username`, `password`, `email`, `phone`, `paymenttype`, `status`, `deleted`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(1207589436, 'U', 'vinu', 'e99a18c428cb38d5f260853678922e03', 'vinu123@gmail.com', '12343454567', 'C', '1', '0', '2018-11-28 11:13:58', 1207589436, NULL, NULL),
(1307862549, 'W', 'allan', 'e99a18c428cb38d5f260853678922e03', 'raphael@bromlays.com', '12343457777', 'C', '1', '0', '2018-11-27 07:04:17', 0, NULL, NULL),
(1740695328, 'W', 'rijo', 'e99a18c428cb38d5f260853678922e03', 'rijo123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-15 03:33:26', 0, '2018-12-11 06:04:17', NULL),
(2031958674, 'A', 'raphael86', 'e99a18c428cb38d5f260853678922e03', 'raphaelfrancis2002@gmail.com', '1234567890', 'C', '1', '0', '2018-10-20 03:32:40', 0, '2018-11-22 04:45:05', NULL),
(3159842076, 'U', 'willy', 'e99a18c428cb38d5f260853678922e03', 'willy123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-28 11:55:04', 3159842076, NULL, NULL),
(4519367802, 'W', 'lijo', 'e99a18c428cb38d5f260853678922e03', 'lijo123@gmail.com', '1234567890', 'C', '1', '0', '2018-12-11 05:49:38', 0, NULL, NULL),
(4607259381, 'W', 'binu', 'e99a18c428cb38d5f260853678922e03', 'binu123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-29 11:15:38', 0, NULL, NULL),
(4860793521, 'A', 'Vimal', 'b24331b1a138cde62aa1f679164fc62f', 'amal123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-26 03:59:49', 0, NULL, NULL),
(5104973286, 'A', 'abc123', 'e99a18c428cb38d5f260853678922e03', 'bromlays@gmail.com', '1234567890', 'C', '1', '0', '2018-11-26 02:22:54', 0, NULL, NULL),
(6091735284, 'W', 'leon', 'e99a18c428cb38d5f260853678922e03', 'leon123@gmail.com', '1234567890', 'C', '1', '0', '2018-12-11 05:47:01', 0, NULL, NULL),
(6719325480, 'U', 'Bijo', 'e99a18c428cb38d5f260853678922e03', 'bijo123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-27 10:13:22', 0, NULL, NULL),
(7801642539, 'A', 'Riny', 'e99a18c428cb38d5f260853678922e03', 'riny23@gmail.com', '1234567890', 'C', '1', '0', '2018-11-26 02:27:28', 0, NULL, NULL),
(7943652018, 'U', 'Vincy', 'e99a18c428cb38d5f260853678922e03', 'vincy123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-26 04:10:55', 0, '2018-11-27 02:29:13', NULL),
(8023461795, 'W', 'anathu', 'e99a18c428cb38d5f260853678922e03', 'anathu123@gmail.com', '1234567890', 'C', '1', '0', '2018-12-11 05:52:14', 0, NULL, NULL),
(8470952316, 'U', 'rinoy', 'e99a18c428cb38d5f260853678922e03', 'rinoy123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-28 11:41:13', 8470952316, '2018-12-11 05:04:13', NULL),
(8527610493, 'U', 'don', 'e99a18c428cb38d5f260853678922e03', 'bromlays@gmail.com', '123434522331', 'C', '1', '0', '2018-11-26 02:39:30', 0, '0000-00-00 00:00:00', NULL),
(8743965102, 'A', 'Tijo', 'fd663b8b08b461adb8d06525b395b2a0', 'rfc234@gmail.com', '1234567890', 'C', '1', '0', '2018-11-26 05:30:34', 0, NULL, NULL),
(8920314765, 'W', 'tom', 'e99a18c428cb38d5f260853678922e03', 'tom123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-29 11:09:48', 0, NULL, NULL),
(9146287305, 'U', 'Rino', 'e99a18c428cb38d5f260853678922e03', 'allan123@gmail.com', '1234567890', 'C', '1', '0', '2018-11-27 10:11:26', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile_address`
--

CREATE TABLE `profile_address` (
  `idprofile_address` bigint(20) NOT NULL,
  `profileid` bigint(20) NOT NULL,
  `addressname` varchar(255) NOT NULL,
  `addresstype` enum('P','S') NOT NULL DEFAULT 'P' COMMENT '''P'' - primary, ''S'' - secondary',
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `sublocality` varchar(255) DEFAULT NULL,
  `landmark` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `district` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `lat` text,
  `lon` text,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - Active, ''0''- Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_address`
--

INSERT INTO `profile_address` (`idprofile_address`, `profileid`, `addressname`, `addresstype`, `address1`, `address2`, `location`, `sublocality`, `landmark`, `city`, `district`, `state`, `lat`, `lon`, `status`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(1307469258, 2031958674, '', 'P', 'bromlaystechventures', 'Ollur', 'Thrissur', 'Thrissur', 'Kerala', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, '2018-11-22 04:45:05', NULL),
(1903648275, 6091735284, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'Kl', 'Thrissur', 'Ernakulam', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(3465027819, 6719325480, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'AdamBazar', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(3694207581, 8023461795, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'Kl', 'Thrissur', 'Pathanamthitta', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(4206978315, 5104973286, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'AdamBazar', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(5034726891, 7801642539, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'Kerala', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(5071429863, 4607259381, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'AdamBazar', 'Thrissur', 'Idukki', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(5237694018, 1307862549, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'AdamBazar', 'Thrissur', 'Palakkad', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(5348102769, 4860793521, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'AdamBazar', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(5463719820, 8743965102, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'Kerala', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(6079145382, 8920314765, '', 'P', 'ollur', 'Thrissur', 'Thrissur', 'Thrissur', 'Kerala', 'Thrissur', 'Ernakulam', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(6531478209, 4519367802, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'Kl', 'Thrissur', 'Thiruvananthapuram', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(6759802314, 9146287305, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'Kerala', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, NULL, NULL),
(7583962014, 1740695328, '', 'P', 'bromlays', 'Thrissur', 'Thrissur', 'Thrissur', 'AdamBazar', 'Thrissur', 'Palakkad', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, '2018-12-11 06:04:17', NULL),
(8397415602, 1207589436, '', 'P', '', NULL, '', NULL, '', '', '', '', NULL, NULL, '1', '2018-11-28 11:13:58', 1207589436, NULL, NULL),
(9241876503, 8527610493, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'AdamBazar', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', NULL),
(9245317068, 3159842076, '', 'P', '', NULL, '', NULL, '', '', '', '', NULL, NULL, '1', '2018-11-28 11:55:04', 3159842076, NULL, NULL),
(9285470613, 7943652018, '', 'P', 'bromlaystechventures', 'Thrissur', 'Thrissur', 'Thrissur', 'Kl', 'Thrissur', 'Thrissur', 'KERALA', NULL, NULL, '1', '0000-00-00 00:00:00', 0, '2018-11-27 02:29:13', NULL),
(9610257384, 8470952316, '', 'P', 'ollur', 'center', 'Thrissur', 'AdamBazar', 'Bright Towers', 'Kalamassery', 'Ernakulam', 'Kerala', NULL, NULL, '1', '2018-11-28 11:41:13', 8470952316, '2018-12-11 05:04:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile_application`
--

CREATE TABLE `profile_application` (
  `idprofile_application` bigint(20) NOT NULL,
  `profileid` bigint(20) NOT NULL,
  `applicationid` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - Active, ''0''- Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_details`
--

CREATE TABLE `profile_details` (
  `idprofile_detail` bigint(20) NOT NULL,
  `profileid` bigint(20) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `gender` enum('M','F','O') DEFAULT NULL COMMENT '''M'' - MALE, ''F''- FEMALE, ''O'' - OTHER',
  `age` int(4) DEFAULT NULL,
  `years_of_experience` int(4) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '''1'' - Active, ''0''- Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_details`
--

INSERT INTO `profile_details` (`idprofile_detail`, `profileid`, `firstname`, `lastname`, `gender`, `age`, `years_of_experience`, `status`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(314628975, 8920314765, 'Raphael', 'Francis', 'M', 30, NULL, '1', '2018-11-29 11:09:48', 0, NULL, NULL),
(386497125, 5104973286, 'Francis', 'paul', 'M', 31, NULL, '1', '2018-11-26 02:22:54', 0, NULL, NULL),
(538274916, 1207589436, '', '', NULL, NULL, NULL, '1', '2018-11-28 11:13:58', 538274916, NULL, NULL),
(593648217, 3159842076, '', '', NULL, NULL, NULL, '1', '2018-11-28 11:55:04', 593648217, NULL, NULL),
(761253894, 8470952316, 'Rijo', 'Paul', 'M', 25, NULL, '1', '2018-11-28 11:41:13', 761253894, '2018-12-11 05:04:13', NULL),
(917523468, 1307862549, 'Allan', 'Francis', 'M', 30, NULL, '1', '2018-11-27 07:04:17', 0, NULL, NULL),
(1302978645, 4519367802, 'Alex', 'paul', 'M', 31, NULL, '1', '2018-12-11 05:49:38', 0, NULL, NULL),
(1650473928, 8527610493, 'alexander', 'paul', 'M', 31, NULL, '1', '2018-11-26 02:39:30', 0, '0000-00-00 00:00:00', NULL),
(1926748053, 6719325480, 'Raphael', 'Francis', 'M', 30, NULL, '1', '2018-11-27 10:13:22', 0, NULL, NULL),
(4235189670, 2031958674, 'Raphael', 'Francis', 'M', 30, NULL, '1', '2018-10-20 03:32:40', 0, '2018-11-22 04:45:05', NULL),
(4592136780, 7801642539, 'Babu', 'paul', 'M', 30, NULL, '1', '2018-11-26 02:27:28', 0, NULL, NULL),
(4795063821, 4860793521, 'Amal', 'John', 'M', 31, NULL, '1', '2018-11-26 03:59:49', 0, NULL, NULL),
(4853160927, 7943652018, 'Vincy', 'Luke', 'F', 26, NULL, '1', '2018-11-26 04:10:55', 0, '2018-11-27 02:29:13', NULL),
(5830296417, 4607259381, 'unni', 'balu', 'M', 28, NULL, '1', '2018-11-29 11:15:38', 0, NULL, NULL),
(6450897312, 8023461795, 'appu', 'Francis', 'M', 30, NULL, '1', '2018-12-11 05:52:14', 0, NULL, NULL),
(7201953468, 9146287305, 'allan', 'tom', 'M', 30, NULL, '1', '2018-11-27 10:11:26', 0, NULL, NULL),
(7215093468, 8743965102, 'Francis', 'Francis', 'M', 0, NULL, '1', '2018-11-26 05:30:34', 0, NULL, NULL),
(7895016432, 1740695328, 'Rijo', 'Joseph', 'M', 34, NULL, '1', '2018-11-15 03:33:26', 0, '2018-12-11 06:04:17', NULL),
(9071583642, 6091735284, 'Leon', 'Francis', 'M', 30, NULL, '1', '2018-12-11 05:47:01', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rewards`
--

CREATE TABLE `rewards` (
  `idreward` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL,
  `rewardname` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL COMMENT '''1'' - ''Active'', ''0'' - Inactive',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `idservices` bigint(20) NOT NULL,
  `servicename` varchar(255) NOT NULL,
  `categoryid` bigint(20) DEFAULT NULL,
  `other_categoryid` bigint(20) DEFAULT NULL,
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '''1'' - Yes, ''0'' - No',
  `status` enum('1','0') NOT NULL COMMENT '''1'' - Active, ''0'' - Inactive',
  `createdby` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `updatedby` bigint(20) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`idservices`, `servicename`, `categoryid`, `other_categoryid`, `deleted`, `status`, `createdby`, `created`, `updatedby`, `updated`) VALUES
(1879235604, 'panelboardwiring', 3180756249, NULL, '0', '1', 2031958674, '2018-11-27 07:18:31', NULL, NULL),
(2376108945, 'Tileworker', 2704138695, NULL, '0', '1', 0, '2018-11-27 03:58:22', NULL, '2018-11-27 18:53:34'),
(3510624978, 'Tigwelding', 2704138695, NULL, '0', '1', 0, '2018-11-27 03:55:43', NULL, '2018-11-27 18:46:01'),
(4582179306, 'motormechanic', 7642189305, NULL, '0', '1', 2031958674, '2018-11-27 07:26:15', NULL, NULL),
(4973568120, 'TesingFuels', 8390157264, NULL, '0', '1', 2031958674, '2018-11-27 07:18:01', NULL, NULL),
(7418320956, 'Welding', 2834591076, NULL, '0', '1', 2031958674, '2018-11-27 04:06:03', NULL, '2018-11-27 18:58:02'),
(7613842590, 'Floortileworker', 2704138695, NULL, '0', '1', 0, '2018-11-27 03:59:11', NULL, NULL),
(7961082345, 'Brick', 2704138695, NULL, '0', '1', 0, '2018-11-27 03:58:40', NULL, '2018-11-27 18:59:31'),
(8304795162, 'plastering', 2704138695, NULL, '0', '1', 2031958674, '2018-11-27 07:23:10', NULL, NULL),
(9340716285, 'hardware', 5034162897, NULL, '0', '1', 2031958674, '2018-11-27 07:18:40', NULL, '2018-11-27 19:19:32'),
(9347056182, 'House', 2718360945, NULL, '0', '1', 2031958674, '2018-11-27 07:24:02', NULL, NULL),
(9817532064, 'wiring', 7642189305, NULL, '0', '1', 2031958674, '2018-11-27 07:25:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_requests`
--

CREATE TABLE `service_requests` (
  `idservice_request` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL,
  `workerid` bigint(20) DEFAULT NULL,
  `service` bigint(20) NOT NULL,
  `categoryid` bigint(20) DEFAULT NULL,
  `usermessage` text,
  `servicedistrict` varchar(100) DEFAULT NULL,
  `service_location` varchar(255) NOT NULL,
  `service_location_address` text,
  `payment_type` enum('C','O') DEFAULT NULL COMMENT '''C'' - Cash, ''O'' - Online',
  `payment_status` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `servicedate` date NOT NULL,
  `servicetime` time(4) DEFAULT NULL,
  `service_status` enum('N','S','C','Ca','P') NOT NULL DEFAULT 'N' COMMENT '''N'' - Not started, ''S'' - Started, ''C'' - Completed, ''Ca'' - cancelled,''P''-progress',
  `worker_status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '''1''-assigned ''0''-unassigned',
  `is_email` enum('1','0') NOT NULL DEFAULT '0' COMMENT '''1''-active ''0''-deactive',
  `deleted_status` enum('1','0') NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_requests`
--

INSERT INTO `service_requests` (`idservice_request`, `userid`, `workerid`, `service`, `categoryid`, `usermessage`, `servicedistrict`, `service_location`, `service_location_address`, `payment_type`, `payment_status`, `amount`, `servicedate`, `servicetime`, `service_status`, `worker_status`, `is_email`, `deleted_status`, `createdon`, `createdby`, `updated`, `updatedby`) VALUES
(4259017836, 9146287305, 1740695328, 9340716285, 5034162897, 'Need a hardware technician', 'Idukki', 'bromlays techventures', NULL, 'C', 1, 0, '2108-12-12', '20:50:00.0000', 'N', '', '', '0', '2018-12-11 13:21:02', 9146287305, '2018-12-11 14:35:01', NULL),
(5819743602, 9146287305, 1307862549, 7418320956, 2834591076, 'Need a welder and latheworker', 'Palakkad', 'Bromlays Tech Ventures,Adam bazar', NULL, 'C', 1, 0, '2019-01-02', '21:50:00.0000', 'N', '', '', '0', '2018-12-11 10:06:18', 9146287305, '2018-12-11 14:16:38', 9146287305),
(6405927183, 3159842076, NULL, 9817532064, 7642189305, 'Need A House Wireman', 'Kottayam', 'ollur', NULL, 'C', 1, 0, '2018-12-12', '10:40:00.0000', 'N', '0', '0', '0', '2018-12-11 15:36:30', 3159842076, NULL, NULL),
(8245760139, 3159842076, NULL, 9817532064, 7642189305, 'Need A Tileworker as well', 'Idukki', 'cochin', NULL, 'C', 1, 0, '0000-00-00', '15:50:00.0000', 'N', '0', '0', '0', '2018-12-11 15:15:44', 3159842076, '2018-12-11 16:02:03', 3159842076),
(9365804271, 3159842076, NULL, 9340716285, 5034162897, 'Need a hardware technician', 'Kottayam', 'bromlays techventures', NULL, 'C', 1, 0, '2018-12-12', '20:50:00.0000', 'N', '0', '0', '0', '2018-12-11 15:13:15', 3159842076, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `idtransaction` bigint(20) NOT NULL,
  `requestid` bigint(20) NOT NULL,
  `type` varchar(45) NOT NULL,
  `transaction_provider` varchar(255) DEFAULT NULL,
  `transactionstatus` enum('N','I','C','U') DEFAULT 'N' COMMENT '''N'' - Not Started, ''I'' - Inprogress, ''C''- Canceled, ''U''-Unsuccess',
  `transactionkey` varchar(255) NOT NULL,
  `transactionresponse_message` varchar(255) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `serviceid` bigint(20) NOT NULL,
  `categoryid` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_categories`
--

CREATE TABLE `worker_categories` (
  `idworker_category` bigint(20) NOT NULL,
  `profileid` bigint(20) NOT NULL,
  `categoryid` bigint(20) NOT NULL,
  `parentcategoryid` bigint(20) NOT NULL COMMENT '''0'' - parent',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `worker_categories`
--

INSERT INTO `worker_categories` (`idworker_category`, `profileid`, `categoryid`, `parentcategoryid`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(814379265, 6091735284, 7642189305, 0, '2018-12-11 17:47:50', 6091735284, NULL, NULL),
(845126973, 1740695328, 2704138695, 0, '2018-11-27 12:16:22', 1740695328, NULL, NULL),
(1278093465, 6091735284, 2718360945, 0, '2018-12-11 17:47:40', 6091735284, NULL, NULL),
(1734026859, 6091735284, 2834591076, 0, '2018-12-11 17:47:43', 6091735284, NULL, NULL),
(1798450632, 8920314765, 2834591076, 0, '2018-11-29 11:10:11', 2031958674, NULL, NULL),
(1843029657, 1740695328, 7642189305, 0, '2018-11-27 19:24:37', 2031958674, NULL, NULL),
(2519608437, 4607259381, 2718360945, 0, '2018-11-29 11:16:07', 2031958674, NULL, NULL),
(3257094816, 4607259381, 3180756249, 0, '2018-11-29 11:16:00', 2031958674, NULL, NULL),
(3521409786, 8920314765, 8390157264, 0, '2018-11-29 11:10:14', 2031958674, NULL, NULL),
(3674189025, 8023461795, 3180756249, 0, '2018-12-11 17:53:00', 8023461795, NULL, NULL),
(3746051892, 8920314765, 3180756249, 0, '2018-11-29 11:10:06', 2031958674, NULL, NULL),
(3790456182, 8023461795, 8390157264, 0, '2018-12-11 17:52:57', 8023461795, NULL, NULL),
(3907258641, 8920314765, 7642189305, 0, '2018-11-29 11:10:03', 2031958674, NULL, NULL),
(4378012965, 1307862549, 5034162897, 0, '2018-12-05 07:26:34', 1307862549, NULL, NULL),
(7138592406, 1307862549, 2718360945, 0, '2018-11-29 04:45:26', 1307862549, NULL, NULL),
(7184935062, 1740695328, 2834591076, 0, '2018-11-27 12:16:44', 1740695328, NULL, NULL),
(8371265049, 1307862549, 2834591076, 0, '2018-11-27 14:34:50', 1307862549, NULL, NULL),
(8514397206, 6091735284, 5034162897, 0, '2018-12-11 17:47:46', 6091735284, NULL, NULL),
(9310564872, 4607259381, 7642189305, 0, '2018-11-29 11:15:57', 2031958674, NULL, NULL),
(9312648075, 1740695328, 3180756249, 0, '2018-11-27 19:24:39', 2031958674, NULL, NULL),
(9673801542, 8023461795, 7642189305, 0, '2018-12-11 17:52:55', 8023461795, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `worker_locations`
--

CREATE TABLE `worker_locations` (
  `idworker_location` bigint(20) NOT NULL,
  `profileid` bigint(20) NOT NULL,
  `placeid` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `worker_services`
--

CREATE TABLE `worker_services` (
  `idworkservice` bigint(20) NOT NULL,
  `idprofile` bigint(20) NOT NULL,
  `categoryid` bigint(20) DEFAULT NULL,
  `serviceid` bigint(20) NOT NULL,
  `amount` int(11) NOT NULL,
  `servicename` varchar(255) NOT NULL,
  `status` enum('1','0') DEFAULT '1' COMMENT '''1'' - ACTIVE, ''0'' - INACTIVE',
  `created` datetime NOT NULL,
  `createdby` bigint(20) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updatedby` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `worker_services`
--

INSERT INTO `worker_services` (`idworkservice`, `idprofile`, `categoryid`, `serviceid`, `amount`, `servicename`, `status`, `created`, `createdby`, `updated`, `updatedby`) VALUES
(264839517, 1740695328, 3180756249, 1879235604, 500, 'panelboardwiring', '1', '2018-11-27 19:24:42', 2031958674, NULL, NULL),
(1647980352, 4607259381, 3180756249, 1879235604, 500, 'panelboardwiring', '1', '2018-11-29 11:16:12', 2031958674, NULL, NULL),
(2073184965, 6091735284, 7642189305, 4582179306, 500, 'motormechanic', '1', '2018-12-11 17:47:55', 6091735284, NULL, NULL),
(2410368579, 1740695328, 2704138695, 3510624978, 500, 'plastering', '1', '2018-11-27 12:16:27', 1740695328, NULL, NULL),
(3405168279, 6091735284, 5034162897, 9340716285, 500, 'hardware', '1', '2018-12-11 17:47:59', 6091735284, NULL, NULL),
(4639082175, 8023461795, 7642189305, 9817532064, 500, 'wiring', '1', '2018-12-11 17:53:06', 8023461795, NULL, NULL),
(5096841723, 8920314765, 7642189305, 9817532064, 500, 'wiring', '1', '2018-11-29 11:10:27', 2031958674, NULL, NULL),
(6147398250, 8920314765, 8390157264, 4973568120, 500, 'TesingFuels', '1', '2018-11-29 11:10:25', 2031958674, NULL, NULL),
(6802951473, 1740695328, 2834591076, 7418320956, 500, 'Welding', '1', '2018-11-27 12:16:46', 1740695328, NULL, NULL),
(6987145203, 1740695328, 2704138695, 2376108945, 500, 'Tilework', '1', '2018-11-27 12:16:26', 1740695328, NULL, NULL),
(7236580149, 8023461795, 3180756249, 1879235604, 500, 'panelboardwiring', '1', '2018-12-11 17:53:04', 8023461795, NULL, NULL),
(7358496210, 1307862549, 2834591076, 7418320956, 500, 'Welding', '1', '2018-11-27 14:34:55', 1307862549, NULL, NULL),
(7842501963, 8920314765, 7642189305, 4582179306, 500, 'motormechanic', '1', '2018-11-29 11:10:18', 2031958674, NULL, NULL),
(8215940673, 8920314765, 3180756249, 1879235604, 500, 'panelboardwiring', '1', '2018-11-29 11:10:17', 2031958674, NULL, NULL),
(8704235619, 6091735284, 2718360945, 9347056182, 500, 'House', '1', '2018-12-11 17:47:57', 6091735284, NULL, NULL),
(8734165092, 4607259381, 7642189305, 9817532064, 500, 'wiring', '1', '2018-11-29 11:16:10', 2031958674, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`idapplication`),
  ADD UNIQUE KEY `idapplication_UNIQUE` (`idapplication`);

--
-- Indexes for table `application_members`
--
ALTER TABLE `application_members`
  ADD PRIMARY KEY (`idapplication_member`),
  ADD UNIQUE KEY `idapplication_member_UNIQUE` (`idapplication_member`),
  ADD KEY `fk_application_idx` (`applicationid`),
  ADD KEY `fk_application_datafield_idx` (`datafieldid`);

--
-- Indexes for table `application_settings`
--
ALTER TABLE `application_settings`
  ADD PRIMARY KEY (`idapplication_setting`),
  ADD UNIQUE KEY `idapplication_setting_UNIQUE` (`idapplication_setting`),
  ADD KEY `fk_application_settings_application_idx` (`applicationid`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`idcategory`),
  ADD UNIQUE KEY `idcategory_UNIQUE` (`idcategory`),
  ADD UNIQUE KEY `categoryname_UNIQUE` (`categoryname`),
  ADD KEY `fk_categories_application_idx` (`applicationid`);

--
-- Indexes for table `data_fields`
--
ALTER TABLE `data_fields`
  ADD PRIMARY KEY (`iddata_field`),
  ADD UNIQUE KEY `iddata_field_UNIQUE` (`iddata_field`);

--
-- Indexes for table `field_application`
--
ALTER TABLE `field_application`
  ADD PRIMARY KEY (`idfield_application`),
  ADD KEY `fk_field_application_member_idx` (`applicationmemberid`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`idimage`,`imagelocation`),
  ADD UNIQUE KEY `idimage_UNIQUE` (`idimage`),
  ADD UNIQUE KEY `imagename_UNIQUE` (`imagename`),
  ADD KEY `fk_images_profile_idx` (`idprofile`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`idmessage`),
  ADD UNIQUE KEY `idmessages_UNIQUE` (`idmessage`),
  ADD KEY `fk_messages_sender_idx` (`senderid`),
  ADD KEY `fk_messages_reciever_idx` (`recieverid`),
  ADD KEY `fk_messages_service_request_idx` (`servicrequestid`);

--
-- Indexes for table `other_categories`
--
ALTER TABLE `other_categories`
  ADD PRIMARY KEY (`idother_category`),
  ADD UNIQUE KEY `idother_service_UNIQUE` (`idother_category`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`idplace`),
  ADD UNIQUE KEY `idplaces_UNIQUE` (`idplace`),
  ADD UNIQUE KEY `name_UNIQUE` (`placename`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`idprofiles`),
  ADD UNIQUE KEY `idprofiles_UNIQUE` (`idprofiles`);

--
-- Indexes for table `profile_address`
--
ALTER TABLE `profile_address`
  ADD PRIMARY KEY (`idprofile_address`),
  ADD UNIQUE KEY `idprofile_address_UNIQUE` (`idprofile_address`),
  ADD KEY `fk_profile_address_idx` (`profileid`);

--
-- Indexes for table `profile_application`
--
ALTER TABLE `profile_application`
  ADD PRIMARY KEY (`idprofile_application`),
  ADD UNIQUE KEY `idprofile_application_UNIQUE` (`idprofile_application`),
  ADD KEY `fk_profile_application_profile_idx` (`profileid`),
  ADD KEY `fk_profile_application_application_idx` (`applicationid`);

--
-- Indexes for table `profile_details`
--
ALTER TABLE `profile_details`
  ADD PRIMARY KEY (`idprofile_detail`),
  ADD UNIQUE KEY `idprofile_details_UNIQUE` (`idprofile_detail`),
  ADD KEY `fk_profile_details_idx` (`profileid`);

--
-- Indexes for table `rewards`
--
ALTER TABLE `rewards`
  ADD PRIMARY KEY (`idreward`),
  ADD UNIQUE KEY `idreward_UNIQUE` (`idreward`),
  ADD KEY `fk_rewards_user_idx` (`userid`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`idservices`),
  ADD UNIQUE KEY `idservices_UNIQUE` (`idservices`),
  ADD UNIQUE KEY `servicename` (`servicename`);

--
-- Indexes for table `service_requests`
--
ALTER TABLE `service_requests`
  ADD PRIMARY KEY (`idservice_request`),
  ADD UNIQUE KEY `idservice_request_UNIQUE` (`idservice_request`),
  ADD KEY `fk_service_requests_user_idx` (`userid`),
  ADD KEY `fk_service_requests_worker_idx` (`workerid`),
  ADD KEY `fk_service_requests_service_idx` (`service`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`idtransaction`),
  ADD UNIQUE KEY `idtransaction_UNIQUE` (`idtransaction`),
  ADD UNIQUE KEY `transactionkey_UNIQUE` (`transactionkey`),
  ADD KEY `fk_transactions_service_idx` (`serviceid`),
  ADD KEY `fk_transactions_service_request_idx` (`requestid`);

--
-- Indexes for table `worker_categories`
--
ALTER TABLE `worker_categories`
  ADD PRIMARY KEY (`idworker_category`),
  ADD UNIQUE KEY `idworker_category_UNIQUE` (`idworker_category`),
  ADD KEY `fk_worker_categories_profile_idx` (`profileid`),
  ADD KEY `fk_worker_category_idx` (`categoryid`);

--
-- Indexes for table `worker_locations`
--
ALTER TABLE `worker_locations`
  ADD PRIMARY KEY (`idworker_location`),
  ADD UNIQUE KEY `idworker_location_UNIQUE` (`idworker_location`),
  ADD KEY `fk_worker_locations_profile_idx` (`profileid`),
  ADD KEY `fk_worker_location_idx` (`placeid`);

--
-- Indexes for table `worker_services`
--
ALTER TABLE `worker_services`
  ADD PRIMARY KEY (`idworkservice`),
  ADD UNIQUE KEY `user_services_UNIQUE` (`idworkservice`),
  ADD KEY `fk_worker_services_profile_idx` (`idprofile`),
  ADD KEY `fk_worker_services_category_idx` (`categoryid`),
  ADD KEY `fk_worker_services_service_idx` (`serviceid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `application_members`
--
ALTER TABLE `application_members`
  MODIFY `idapplication_member` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_fields`
--
ALTER TABLE `data_fields`
  MODIFY `iddata_field` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `field_application`
--
ALTER TABLE `field_application`
  MODIFY `idfield_application` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `idmessage` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_address`
--
ALTER TABLE `profile_address`
  MODIFY `idprofile_address` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;
--
-- AUTO_INCREMENT for table `profile_details`
--
ALTER TABLE `profile_details`
  MODIFY `idprofile_detail` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;
--
-- AUTO_INCREMENT for table `rewards`
--
ALTER TABLE `rewards`
  MODIFY `idreward` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_requests`
--
ALTER TABLE `service_requests`
  MODIFY `idservice_request` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `idtransaction` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `worker_services`
--
ALTER TABLE `worker_services`
  MODIFY `idworkservice` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `application_members`
--
ALTER TABLE `application_members`
  ADD CONSTRAINT `fk_application` FOREIGN KEY (`applicationid`) REFERENCES `applications` (`idapplication`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_application_datafield` FOREIGN KEY (`datafieldid`) REFERENCES `data_fields` (`iddata_field`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `application_settings`
--
ALTER TABLE `application_settings`
  ADD CONSTRAINT `fk_application_settings_application` FOREIGN KEY (`applicationid`) REFERENCES `applications` (`idapplication`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `fk_categories_application` FOREIGN KEY (`applicationid`) REFERENCES `applications` (`idapplication`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `field_application`
--
ALTER TABLE `field_application`
  ADD CONSTRAINT `fk_field_application_member` FOREIGN KEY (`applicationmemberid`) REFERENCES `application_members` (`idapplication_member`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `fk_images_profile` FOREIGN KEY (`idprofile`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_reciever` FOREIGN KEY (`recieverid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_messages_sender` FOREIGN KEY (`senderid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_messages_service_request` FOREIGN KEY (`servicrequestid`) REFERENCES `service_requests` (`idservice_request`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile_address`
--
ALTER TABLE `profile_address`
  ADD CONSTRAINT `fk_profile_address` FOREIGN KEY (`profileid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile_application`
--
ALTER TABLE `profile_application`
  ADD CONSTRAINT `fk_profile_application_application` FOREIGN KEY (`applicationid`) REFERENCES `applications` (`idapplication`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_profile_application_profile` FOREIGN KEY (`profileid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile_details`
--
ALTER TABLE `profile_details`
  ADD CONSTRAINT `fk_profile_details` FOREIGN KEY (`profileid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rewards`
--
ALTER TABLE `rewards`
  ADD CONSTRAINT `fk_rewards_user` FOREIGN KEY (`userid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_requests`
--
ALTER TABLE `service_requests`
  ADD CONSTRAINT `fk_service_requests_service` FOREIGN KEY (`service`) REFERENCES `services` (`idservices`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_service_requests_user` FOREIGN KEY (`userid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_service_requests_worker` FOREIGN KEY (`workerid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `fk_transactions_service` FOREIGN KEY (`serviceid`) REFERENCES `services` (`idservices`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transactions_service_request` FOREIGN KEY (`requestid`) REFERENCES `service_requests` (`idservice_request`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `worker_categories`
--
ALTER TABLE `worker_categories`
  ADD CONSTRAINT `fk_worker_categories_profile` FOREIGN KEY (`profileid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_worker_category` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`idcategory`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `worker_locations`
--
ALTER TABLE `worker_locations`
  ADD CONSTRAINT `fk_worker_location` FOREIGN KEY (`placeid`) REFERENCES `places` (`idplace`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_worker_locations_profile` FOREIGN KEY (`profileid`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `worker_services`
--
ALTER TABLE `worker_services`
  ADD CONSTRAINT `fk_worker_services_category` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`idcategory`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_worker_services_profile` FOREIGN KEY (`idprofile`) REFERENCES `profiles` (`idprofiles`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_worker_services_service` FOREIGN KEY (`serviceid`) REFERENCES `services` (`idservices`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
