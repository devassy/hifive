<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);


// Get ID
$post->senderid = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');


//Get post

$result = $post->getsendmessage();
//Get row count

$num = $result->rowCount();

//Check if any posts
if($num > 0 ){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $senderid = $row["senderid"];
        
        $idprofiles = $row["idprofiles"];
        
        $receiverid = $row["recieverid"];
        $type = $row["type"];
        if(($senderid=="$idprofiles")&&($type=='U'||$type=='W'))
        {
                $fname = $row["firstname"];
                $lname = $row["lastname"];
                $space = "  ";
                $from = $fname.$space.$lname;
                $post_item = array(
                'resultedmessage'=>"success",
                'idmessage'=>$row["idmessage"],
                'senderid'=>$row["senderid"],
                'recieverid'=>$row["recieverid"],
                'message'=>$row["message"],
                'servicerequestid'=>$row["servicrequestid"],
                'subject'=>$row["subject"],
                'usermessage'=>$row["usermessage"],
                'created'=>$row["created"],
                "From"=>"$from",
                "To"=>"Admin",
                'firstname'=>$row["firstname"],
                'lastname'=>$row["lastname"],
                "type"=>$row["type"],
                "idprofiles"=>$row["idprofiles"]
            );
            array_push($post_arr['data'],$post_item);
        }
        else
        {
            $error_arr = array();
            $post_item = array("message"=>"failed to login");
            array_push($error_arr[],$post_item);
            echo json_encode($error_arr);
            return true;
        }
        
       
    }
   
    
   
    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
        $error_arr = array();
        $error_arr['data']= array();
        $post_item = array("message"=>"No such Records Found");
        array_push($error_arr['data'],$post_item);
        echo json_encode($error_arr);
        return true;
}

?>
