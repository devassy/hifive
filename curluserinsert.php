<?php
include 'urlrewrite.php';
$username = $_POST["username"];
$password = $_POST["password"];
$confirmpassword = $_POST["confirmpassword"];
$email = $_POST["useremail"];
$phone = $_POST["phone"];
$userdata = array("username"=>$username,"password"=>$password,"confirmpassword"=>$confirmpassword,"email"=>"$email","phone"=>"$phone");


//Option 1: Convert data array to json if you want to send data as json
$data = json_encode($userdata);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/addnewuser.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$newresult = json_decode($result);

//close curl connection
curl_close($ch);
//print result
// print_r($result);
if($newresult)
{
    $message = $newresult->message;
    header("location:registernewuser.php?message=$message");
}
?>