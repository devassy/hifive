<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));

$post->idservice_request = $data->idservice_request;

//$post->workerid = $data->workerid
$post->userid = $data->userid;
//$post->amount = $data->amount;
$post->servicedate = $data->servicedate;
$post->servicetime = $data->servicetime;
$post->servicedistrict = $data->servicedistrict;
$post->service_location = $data->service_location;
$post->usermessage = $data->usermessage;
$post->payment_type = $data->payment_type;
$post->payment_status = $data->payment_status;
if(isset($data->categoryid))
{
$post->categoryid = $data->categoryid;
}
else
{
   
    $getcategory = $post->getcategoryservicefromrequest();
   
    if($row = $getcategory->fetch(PDO::FETCH_ASSOC))
    {
        $post->categoryid = $row["categoryid"];
        $post->service = $row["service"];
    }

}
if(isset($data->service))
{
$post->service = $data->service;
}
else
{
    $getcategoryservice = $post->getcategoryservicefromrequest();
    if($row = $getcategoryservice->fetch(PDO::FETCH_ASSOC))
    {
        $post->categoryid = $row["categoryid"];
        $post->service = $row["service"];
    }
   
}

if($result = $post->updateservicerequestbyuser()){
    
    if($result=="1")
    {
        echo json_encode(array('message' => 'service request updated successfully'));
        return true;
    }
    
}else{
    echo json_encode(array('message' => 'Failed to  update Service Request'));
    return true;
}