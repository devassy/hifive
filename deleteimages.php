<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

$post = new Post($db);

$data = json_decode(file_get_contents("php://input"));

$post->idimage = $data->idimage;
$newresult = $post->deleteimages();
$num = $newresult->rowCount();
if($num>0)
{
    $error_arr = array();
    $error_arr['data']= array();
    $post_item = array("message"=>"Image Deleted Successfully");
    array_push($error_arr['data'],$post_item);
    echo json_encode($error_arr);
    return true;
}

else
{
    $error_arr = array();
    $error_arr['data']= array();
    $post_item = array("message"=>"Failed to delete image");
    array_push($error_arr['data'],$post_item);
    echo json_encode($error_arr);
    return true;
    
}

//selected services


?>