<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';
include_once 'Sendemail.php';
date_default_timezone_set('Asia/Kolkata');
//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();
$send = new Sendemail();

// Instatiate blog post object

$post = new Post($db);
$data = json_decode(file_get_contents("php://input"));
$string = "0123456789";
$post->idservice_request = str_shuffle($string);
$post->userid = $data->userid; 

$post->service = $data->idservices;
$post->service_location = $data->service_location;
$post->payment_type = $data->payment_type;
$post->payment_status = "0";
$post->servicedistrict = $data->servicedistrict;
$post->service_location = $data->service_location;
$post->servicedate = $data->servicedate;
$post->servicetime = $data->servicetime;
$post->categoryid = $data->categoryid;
$post->usermessage = $data->usermessage;
$post->createdon = date('Y-m-d H:i:s');
$post->createdby = $data->createdby;
$servdate = $post->servicedate;
$currentdate = date("Y-m-d");
if($currentdate > $servdate)
{
    echo json_encode(array('message' => 'Please select a future date'));
    return true;
}


//$post->type = $data->type;




if($lastid = $post->addservicerequest())
{
    $admindata = $post->getadminid();
    while($row = $admindata->fetch(PDO::FETCH_ASSOC))
    {
        $email = $row["email"];
    }
    $userdata = $post->getuseremail();
    while($userrow = $userdata->fetch(PDO::FETCH_ASSOC))
    {
        $useremail = $userrow["email"];
        $username = $userrow["username"];
    }
    $re = $send->send($email,$post->usermessage,$useremail,$username);
    if($re)
    {
        echo json_encode(array('message' =>'Service Request Sent Successfully'));
        return true;
    }
}
else{
    echo json_encode(array('message' => 'Request Not Submitted'));
    return false;
}
?>