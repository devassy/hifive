<?php
session_start();
include 'urlrewrite.php';
if(isset($_POST["getworker"]))
{
if($_POST["search"] && $_POST["location"])
{
$category = $_POST["search"];
$location = $_POST["location"];
$_SESSION["category"] = $category;
$_SESSION["location"] = $location;
$search = array("servicename"=>$category,"location"=>$location);
$ch = curl_init();
$data = json_encode($search);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getworkerslist.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$searchresult = curl_exec($ch);
$newresult = stripslashes($searchresult);
$workerresult = json_decode($newresult);

//close curl connection
curl_close($ch);
//print result
// print_r($result);
}
else
{
    
}
}
if(isset($_GET["id"]))
{
    $category = $_SESSION["category"];
    $location = $_SESSION["location"];
    $search = array("servicename"=>$category,"location"=>$location);
    $ch = curl_init();
    $data = json_encode($search);
    // URL to be called
    curl_setopt($ch, CURLOPT_URL, "$url/getworkerslist.php");
    //set post TRUE to do a regular HTTP POST
    curl_setopt($ch, CURLOPT_POST, 1);
    //set http headers - if you are sending as json data (i.e. option 1) else comment this 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //send post data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //return as output instead of printing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute curl request
    $searchresult = curl_exec($ch);

    $newresult = stripslashes($searchresult);
    $workerresult = json_decode($newresult);
    //close curl connection
    curl_close($ch);
   
}

?>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="colorlib.com" name="author">
            
                <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet"/>
                <link href="css/main2.css" rel="stylesheet"/>
                <!-- <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet"> -->
                <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
                </link>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            </meta>
        </meta>
    </head>
    <body>
        
        <div class="s01" style="background: linear-gradient(to bottom , #54cff8, #23ce8b)">
            <div class="nav">
                <img alt="hifivelogo" class="logo" src="./hifiveimages/hifi.jpg">
                    <div class="login" id="myTopnav">
                        <a href="userlogin.php">
                            Login
                        </a>
                        <a href="registernewuser.php">
                            Signup
                        </a>
                        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                        <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <!-- Modal HTML -->
                    <!-- <div class="modal fade" id="myModal">
                        <div class="modal-dialog modal-login">
                            <div class="modal-content">
                                <form action="#" method="post">
                                    <div class="modal-header">
                                        <h4 class="modal-title">
                                            Login
                                        </h4>
                                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                                            ×
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>
                                                Email
                                            </label>
                                            <input class="form-control" required="required" type="email">
                                            </input>
                                        </div>
                                        <div class="form-group">
                                            <div class="clearfix">
                                                <label>
                                                    Password
                                                </label>
                                                <a class="pull-right text-muted" href="#">
                                                    <small>
                                                        Forgot?
                                                    </small>
                                                </a>
                                            </div>
                                            <input class="form-control" required="required" type="password">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <label class="checkbox-inline pull-left">
                                            <input type="checkbox">
                                                Remember me
                                            </input>
                                        </label>
                                        <input class="btn pull-right" type="submit" value="Login">
                                        </input>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> -->
                </img>
            </div>
           
            <form action="<?=$_SERVER['PHP_SELF'];?>" method="post" name="f1">
                <fieldset>
                    <legend>
                        The Complete Service Supermarket
                    </legend>
                   
                </fieldset>
                <?php
                //it is done if the request is from mobile
                if(isset($_GET["m"]))
                {
                $appid = $_GET["m"];
                ?>
                <button id="btn_link" class="btn-search" style = "height:60px;width:247px;"><a href="userlogin.php?appid=<?php echo $appid;?>">Create Service Request</a></button>
                <?php
                }
                ?>
                <br>
                <br>
                <?php
                if(!isset($_GET["m"]))
                {
                ?>
                <div class="inner-form">
                    <div class="input-field first-wrap">
                        <input id="search" name="search" placeholder="What service are you looking for?" type="text" value = "<?php if(isset($category)){echo $category;}?>"/>
                    </div>
                    <div class="input-field second-wrap">
                        <input id="location" name="location" placeholder="location" type="text" value = "<?php if(isset($location)){echo $location;}?>"/>
                    </div>
                    <div class="input-field third-wrap">
                        <button class="btn-search" name="getworker" type="submit">
                            Search
                        </button>
                    </div>
                </div>
            </form>
            <?php
                }
            ?>

            <!--listing for more than 3 search results-->
            <?php
            if(isset($workerresult))
            {
                foreach($workerresult as $data)
                {
                    foreach($data as $value)
                    {
            ?>
            <div class="row"> 
            <div class="col-sm-12" id="details">
                    <div class="col1">
                    <?php
                    if(isset($value->imagename))
                    {
                    ?>
                    <img class="serach_res"  src="images/<?php echo $value->imagename;?>" width="150px" height="150px">
                        </img>
                    <?php
                    }
                    else 
                    {
                        ?>
                        <img class="serach_res"  src= "./images/avatar.png" width="150px" height="150px">
                            </img>
                        <?php

                    }
                    ?>
                    </div>
                    <div class="col1">
                        <table class="search_details" style="width:195 !important">
                            <tr>
                                <td>
                                    Name:
                                </td>
                                <td>
                                <?php echo $value->firstname;?>   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Location:
                                </td>
                                <td>
                                <?php echo $value->location;?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Service:
                                </td>
                                <td>
                                <?php echo $value->servicename;?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Category:
                                </td>
                                <td>
                                <?php echo $value->categoryname;?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Gender:
                                </td>
                                <td>
                                <?php
                                if($value->gender=='M')
                                {
                                $gender ="Male";
                                echo $gender;
                                }
                                else
                                {
                                $gender ="Female";
                                echo $gender;
                                }
                                ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    City:
                                </td>
                                <td>
                                <?php echo $value->city;?>
                                </td>
                            </tr>
                            
                           
                        </table>
                        <div style="padding-top:15px">
                        <a href = "viewworkerdetailsbyuser.php?id=<?php echo $value->idprofiles;?>">
                                    <button class="btn-search" style="width:81px">
                                        Profile <br>Details
                                    </button>
                                    </a>
                              
                                <a href = "userlogin.php?id=<?php echo $value->idprofiles;?>">
                                    <button class="btn-search" style="width:81px ">
                                        Book <br>Now
                                    </button>
                                </a>
                            </div>
                    </div>
                </div>
            </div>
            <?php
            }
        }
    }
            ?>
</div>
</div>







            <!--listing for one search result-->

            <!-- <div class="row">
                            
                <div class= "col-sm-4 col-md-offset-4" id="details" >
                    <div class="col1">
                        <img class="serach_res" height="150px" src="images/worker1.jpg" width="150px">
                        </img>
                    </div>
                    <div class="col1">
                        <table class="search_details">
                            <tr>
                                <td>
                                    Name
                                </td>
                                <td>
                                    joey
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    location
                                </td>
                                <td>
                                    abc
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    service
                                </td>
                                <td>
                                    IT
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    category
                                </td>
                                <td>
                                    DM
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    Registered on :
                                </td>
                                <td>
                                    12/12/2018
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button>
                                        Profile <br>details
                                    </button>
                                </td>
                                <td>
                                    <button>
                                        Book <br>now
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div> -->
              
                
            </div>
        
        
           
        </div>
        <div class="footer">
    
        </div>
    </body>
    <!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
<script type="text/javascript">
    
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "login") {
        x.className += " responsive";
    } else {
        x.className = "login";
    }
} 


</script>
