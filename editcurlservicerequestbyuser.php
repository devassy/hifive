<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Hifive
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>

<body class="">
<?php
include 'urlrewrite.php';
$ch = curl_init();
$data = array("idcategory","1");
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getallcategories.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
//close curl connection
curl_close($ch);
$getcategory = json_decode($result);

?>
<?php
 include 'urlrewrite.php';
$id = $_GET["id"];
$userid = $_SESSION["idprofiles"];
$data = array("id" =>'$id');
//Option 1: Convert data array to json if you want to send data as json

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getservicerequestbyuser.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);

$newdata = json_decode($result);

foreach($newdata as $val)
{
    foreach($val as $value)
    {
        $district =  $value->servicedistrict;
        $service_status = $value->service_status;
        $servicename = $value->servicename;
        $usermessage = $value->usermessage;
        $servicelocation = $value->service_location;
        $amount = $value->amount;
        $servicedate = $value->servicedate;
        $servicetime = $value->servicetime;
        $service_location = $value->service_location;
        $workerid = $value->workerid;
        $worker_status = $value->worker_status;
        $service_status = $value->service_status;
        $payment_status = $value->payment_status;
        $payment_type = $value->payment_type;
        $idservice_request = $value->idservice_request;
        $workerfirstname = $value->firstname;
        $workerlastname = $value->lastname;
        $workeremail = $value->workeremail;
        $workerphone = $value->workerphone;
        $userfirstname = $value->userfirstname;
        $userlastname = $value->userlastname;
        $useremail = $value->useremail;
        $userphone = $value->userphone;
        $categoryid = $value->categoryid;
        $idservices = $value->idservices;
        
    }
}

//$newdata = json_decode($result);
//close curl connection
curl_close($ch);
?>

<script>

//document.write(x);
</script>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
     
    <div class="logo">
                   <a class="simple-text logo-normal" href="welcomeuser.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
      <div class="sidebar-wrapper">
       <?php include 'usermenu.php';?>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php include 'header.php';?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit service request by user</h4>
                  
                </div>
                           
                <div class="card-body">
                Worker name:<?php if(isset($workerfirstname)){echo $workerfirstname;}?><span style="padding-right:3px;"></span><?php if(isset($workerlastname)){echo $workerlastname;}?><span style="padding-right:20px;"></span>
                Email:<?php echo $workeremail;?><span style="padding-right:20px;"></span>Phone:<?php if(isset($workerphone)){echo $workerphone;}?><span style="padding-right:20px;"></span>Service Name:<?php if(isset($servicename)){echo $servicename;}?>
                  <form method = "post" action = "updatecurluserrequestbyuser.php">
                  <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                            Category name
                                                        </label>
                                                      <select name="catid" id="catid" class="browser-default custom-select">
                                                      <option>Please select a Category</option>
                                                           <?php
                                                           if (isset($getcategory))
                                                           {
                                                            foreach($getcategory as $value)
                                                            {
                                                              foreach($value as $data)
                                                              {
                                                              ?>
                                                           
                                                            <option <?php if(isset($categoryid)){if($categoryid=="$data->idcategory"){echo 'selected';}}?> value ="<?php echo $data->idcategory;?>"><?php echo $data->categoryname;?></option>
                                                             <?php
                                                              }
                                                            }
                                                           }
                                                           else if(isset($getselectedworkercategory))
                                                           {
                                                            foreach($getselectedworkercategory as $data)
                                                              {
                                                              ?>
                                                           
                                                           <option value ="<?php echo $data->idcategory;?>"><?php echo $data->categoryname;?></option>
                                                             <?php
                                                              }

                                                           }
                                                           else
                                                           {

                                                           }
                                                           ?>
                                                       </select>
                                                    </div>
                                                </div>
                                                    <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                            Service 
                                                        </label>
                                                        <select class="browser-default custom-select" name="servicename" id = "servicename">
                                                           <option <?php if(isset($idservices)){echo 'selected';}?> value='<?php echo $idservices;?>'><?php echo $servicename;?></option>
                                                        </select>
                                                    
                                                        <!-- <input class="form-control" type="text" id="service_name" name="service_name">
                                                        </input>
                                                    </div> -->
                                                </div>
                                                </div>
                                            </div>

                      <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                                message
                                                            </label>
                                                        <textarea class="form-control" rows = "4" cols = "90" name="usermessage"><?php echo $usermessage;?></textarea> 
                                                        <!-- <input class="form-control" type="text" name="cat_name" id="cat_name">
                                                        </input> -->
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="bmd-label-floating">
                                                            District
                                                            </label>
                                                            <select name="servicedistrict" id="payment_type" class="browser-default custom-select">
                                                      <option   <?php if ($district == "Thiruvanathapuram" ) echo 'selected';?> value="Thiruvanathapuram">Thiruvanathapuram</option>
                                                      <option   <?php if ($district == "Kollam" ) echo 'selected' ; ?> value="Kollam">Kollam</option>
                                                      <option   <?php if ($district == "Alappuzha" ) echo 'selected' ; ?> value="Alappuzha">Alappuzha</option>
                                                      <option   <?php if ($district == "Pathanamthitta" ) echo 'selected' ; ?> value="Pathanamthitta">Pathanamthitta</option>
                                                      <option   <?php if ($district == "Kottayam" ) echo 'selected' ; ?> value="Kottayam">Kottayam</option>
                                                      <option   <?php if ($district == "Idukki" ) echo 'selected' ; ?> value="Idukki">Idukki</option>
                                                      <option   <?php if ($district == "Ernakulam" ) echo 'selected' ; ?> value="Ernakulam">Ernakulam</option>
                                                      <option   <?php if ($district == "Thrissur" ) echo 'selected' ; ?> value="Thrissur">Thrissur</option>
                                                      <option   <?php if ($district == "Malappuram" ) echo 'selected' ; ?> value="Malappuram">Malappuram</option>
                                                      <option   <?php if ($district == "Kozhikode" ) echo 'selected' ; ?> value="Kozhikode">Kozhikode</option>
                                                      <option   <?php if ($district == "Palakkad" ) echo 'selected' ; ?> value="Palakkad">Palakkad</option>
                                                      <option   <?php if ($district == "Kannur" ) echo 'selected' ; ?> value="Kannur">Kannur</option>
                                                      <option   <?php if ($district == "Wayanad" ) echo 'selected' ; ?> value="Wayanad">Wayanad</option>
                                                      <option   <?php if ($district == "Kasaragod" ) echo 'selected' ; ?> value="Kasaragod">Kasaragod</option>
                                                      </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
					   <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Servicelocation
                                  </label>
                                  <input class="form-control" type="text" name="service_location" value = "<?php echo $service_location;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Service amount
                                  </label>
                                  <input class="form-control" type="text" name="amount" value = "<?php echo $amount;?>" readonly>
                                  </input>
                              </div>
                          </div>
                          
                      </div>
					   <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                 
                                  <input class="form-control" type="date" name="servicedate" value = "<?php echo $servicedate;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                              <label class="bmd-label-floating">
                                     Service Time
                                  </label>
                                  <?php
                                  $number = rtrim($servicetime, "0");
                                  ?>
                                  
                                  <input class="form-control" type="text" name="servicetime" value = "<?php echo $number;?>">
                                  </input>
                              </div>
                          </div>
                      </div>
                      
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                           Worker Status
                                                       
                                                      </label>
                                                      <select name="worker_status" id="worker_status" class="browser-default custom-select" readonly>
                                                      <?php
                                                      if($worker_status=="1")
                                                      {
                                                      ?>
                                                       <option <?php if ($worker_status == "1"){echo 'selected';}?> value ="1">Assigned</option>
                                                    <?php
                                                      }
                                                      else
                                                      {
                                                       ?>
                                                 <option <?php if ($worker_status == "0"){echo 'selected';}?> value ="0">Assigned</option>

                                                    <?php
                                                      }

                                                      
                                                      ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                           Service Status
                                                       
                                                      </label>
                                                      <select name="service_status" id="service_status" class="browser-default custom-select" disabled>
                                                      <option <?php if ($service_status == "C"){echo 'selected';}?> value ="C">Completed</option>
                                                      <option <?php if ($service_status == "Ca"){echo 'selected';}?> value ="Ca">Cancelled</option>
                                                      <option <?php if ($service_status == "P"){echo 'selected';}?> value ="C">Progress</option>
                                                      <option <?php if ($service_status == "S"){echo 'selected';}?> value ="S">Started</option>
                                                      <option <?php if ($service_status == "N"){echo 'selected';}?> value ="C">Not Started</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                           Payment Status
                                                        </label>
                                                        
                                                      </label>
                                                      <select name="payment_status" id="payment_status" class="browser-default custom-select" readonly>
                                                      <?php
                                                      if($worker_status=="1")
                                                      {
                                                      ?>
                                                       <option <?php if ($payment_status == "1"){echo 'selected';}?> value ="1">Paid</option>
                                                    <?php
                                                      }
                                                      else
                                                      {
                                                       ?>
                                                 <option <?php if ($payment_status == "0"){echo 'selected';}?> value ="0">Not Paid</option>

                                                    <?php
                                                      }

                                                      
                                                      ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                        Workers
                                                      </label>
                                                      <select name="workerid" id="workerid" class="browser-default custom-select" disabled>
                                                     
                                                        <option value = "<?php echo $workerid;?>"><?php echo $workerfirstname;?>  <?php echo $workerlastname;?></option>
                                                        
                                                      
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                        Payment Type
                                                      </label>
                                                      
                                                      </label>
                                                      <select name="payment_type" id="payment_type" class="browser-default custom-select">
                                                      <option value="<?php echo $payment_type;?>"><?php if($payment_type=="C"){echo "cash";}else if($payment_type=="O"){echo "online";}else{}?></option>
                                                      <option value = "C">Cash</option>
                                                      <option value = "O">Online</option>
                                                      </select>
                                                     
                                                    </div>
                                                </div>
                                                
                                            </div>
                    <input type="hidden" name ="userid" value= "<?php echo $userid;?>">
                    <input type="hidden" name ="idservice_request" value= "<?php echo $idservice_request;?>">
                    <?php
                    if(empty($workerid))
                    {
                    ?>
                <button type="submit" class="btn btn-primary pull-right">Update Service Request</button>
                    <?php
                    }
                    else
                    {
                    ?>
                    <button type="submit" class="btn btn-primary pull-right" disabled>Update Service Request</button>
                    <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                  </form>
                </div>
                <?php
                   if(isset($_SESSION["result"]))
                   {
                      $resultmessage = $_SESSION["result"];
                      if($resultmessage=="No data to display")
                      {
                     ?>
                     <div style="color:red;" id = "testdiv"><?php echo $resultmessage;?></div>
                     <script type="text/javascript">
                        $(function()
                        {
                                $("#testdiv").delay(7000).fadeOut();
                        });
                    </script>
                     <?php
                     unset($resultmessage);
                     unset($_SESSION["result"]);
                      }
                     
                      else
                      {
                        ?>
                        <div style="color:green;" id = "testdiv"><?php echo $resultmessage;?></div>
                        <script type="text/javascript">
                           $(function()
                           {
                                   $("#testdiv").delay(7000).fadeOut();
                           });
                       </script>
                        <?php
                       
                        unset($resultmessage);
                        unset($_SESSION["result"]);
                       
                      }
                      

                   }
                
                  ?>
              </div>
            </div>

            </div> 
          </div>

            <footer class="footer">
        <div class="container-fluid">
         
          </nav> 
          <div class="copyright">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script><!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
          </div>
        </div>
      </footer>


        </div>
      </div>
              <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
              <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
              <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
              <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
              <!--  Google Maps Plugin    -->
              <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
              <!-- Chartist JS -->
              <script src="assets/js/plugins/chartist.min.js"></script>
              <!--  Notifications Plugin    -->
              <script src="assets/js/plugins/bootstrap-notify.js"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
              <script type="text/javascript">
  $(document).ready(function() {

      $('#catid').change(function(e){
        var categoryid = $("#catid").val();
        var jsonData = {categoryid:categoryid};
        $.ajax({
            type: "POST",
            data: JSON.stringify(jsonData),
            url: 'getservice.php',
            dataType: "json",
            success : function(data){
                if (data)
                {
                    $('#servicename').children().remove();
                    var person = JSON.stringify(data['data'][0]);
                    for(var i=0;i<person.length;i++)
                    {
                        var stddata = JSON.stringify(data['data'][i]);
                        var json = JSON.parse(stddata);
                        var servicetype = json["servicename"];
                        var serviceid = json["idservices"];
                        $('#servicename').append($('<option>').text(servicetype).attr('value',serviceid));
                        
                    }
                    
                    
                }
                else {
                    alert("error");
                }
            }
        });


      });
  });
</script>
              
                </div>
              </div>
  <!--   Core JS Files   -->
  
 
</body>

</html>