<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

    $post = new Post($db);

    $data = json_decode(file_get_contents("php://input"));

    $string = "0123456789";
    $post->idtransaction = str_shuffle($string);
    $post->payment_status = $data->payment_status;
    $post->idservice_request = $data->idservice_request;
    $post->userid = $data->userid;
    $post->type = $data->type;
    $post->transaction_provider = $data->transaction_provider;
    if($post->transaction_provider!=null)
    {
        $post->transaction_provider = $data->transaction_provider;
    }
    else
    {
        $post->transaction_provider = "paypal";
    }
    $post->transactionkey = $data->transactionkey;
    $post->transactionresponse_message = $data->transactionresponse_message;
    if($post->transactionresponse_message!=null)
    {
        $post->transactionresponse_message = $data->transactionresponse_message;
    }
    else
    {
        $post->transactionresponse_message = "Transaction Completed Successfully";
    }
    $post->transactionstatus = $data->transactionstatus;
    if($post->transactionstatus!=null)
    {
        $post->transactionstatus = $data->transactionstatus;
    }
    else
    {
        $post->transactionstatus = "C";
    }
    $post->amount = $data->amount;
    $post->serviceid = $data->serviceid;
    $post->categoryid = $data->categoryid;
    

if($result = $post->updateuserpaymentstatus()){
   if($result=="1") 
     {
       
        $addtransaction = $post->addtransactiondetails();
        if($addtransaction=="1")
        {
            echo json_encode(array('message' => 'payment status updated successfully'));
            return true;
        }
     }
        
    
}
else{
    echo json_encode(array('message' => 'Failed to  update Service Request'));
    return true;
}
?>