<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);


// Get ID
$post->workerid = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');


//Get post

if($post->getappworkerrequest()){
    
    
    $result = $post->getappworkerrequest();
    $post_arr = array();
    $post_arr['data'] =  array();
   
//Create array
while($row = $result->fetch(PDO::FETCH_ASSOC))
{
        
        $post_item = array(
        'idservice_request'=>$row["idservice_request"],
        'servicename'=>$row["servicename"],
        'usermessage' =>$row["usermessage"],
        'service_location'=>$row["service_location"],
        'servicedate'=>$row["servicedate"],
        'service_status'=>$row["service_status"],
        'servicetime'=>$row["servicetime"],
        'amount'=>$row["amount"],
        'createdon'=>$row["createdon"],
        'firstname'=>$row["firstname"],
        'lastname'=>$row["lastname"],
        'service_status'=>$row["service_status"],
        'servicename'=>$row["servicename"],
        'payment_status'=>$row["payment_status"],
        'worker_status'=>$row["worker_status"]);
         array_push($post_arr['data'], $post_item);
}
//Json output

    if(count($row)>0)
    {
       
        echo json_encode($post_arr);
        return true;
    }
    else
    {
        $error_arr = array();
        $error_arr['data']= array();
        $post_item = array("message"=>"No Records Found");
        array_push($error_arr['data'],$post_item);
        echo json_encode($error_arr);
        return true;
    }
}
else
    {
        $error_arr = array();
        $error_arr['data']= array();
        $error_item = array("message"=>"No Records Found");
        array_push($error_arr['data'],$error_item);
        echo json_encode($error_arr);
        return true;
    }

?>