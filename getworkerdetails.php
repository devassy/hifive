<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once './config/Database.php';
include_once './models/Post.php';

//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();

// Instatiate blog post object

$post = new Post($db);

//$data = json_decode(file_get_contents("php://input"));
$post->idservice_request = isset($_GET['id']) ? $_GET['id'] : die('could not get the value');

//Blog post query
$result = $post->getassignedworkerdetails();
//Get row count

$num = $result->rowCount();

//Check if any posts
if($num >0 ){
    // Post array

    $post_arr = array();
    $post_arr['data'] =  array();
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        
            $post_item = array(
            'resultedmessage'=>'success',
            'firstname'=>$row["firstname"],
            'lastname' =>$row["lastname"]
        );
        array_push($post_arr['data'], $post_item);
    }
   
    //Json output

    echo json_encode($post_arr);
    return true;
} else {
    //No posts
    echo json_encode(
        array('resultedmessage' => 'No records')
    );
}



?>