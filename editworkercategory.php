<?php
session_start();
include 'urlrewrite.php';
if(($_SESSION["idprofiles"])&&($_SESSION["type"]=='W'))
{
    $workerid = $_SESSION["idprofiles"];
    
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Hifive
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
</head>

<?php
$ch = curl_init();
$data = array("id"=>$workerid);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getselectedworker_categories.php?id=$workerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$selectedresult = json_decode($result);

//close curl connection
curl_close($ch);

?>
<?php
$ch = curl_init();
$newdata = array("id"=>$workerid);
$data = json_encode($newdata);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getcategories.php?id=$workerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$categoryresult = json_decode($result);

//close curl connection
curl_close($ch);

?>
<?php
$ch = curl_init();
$data = array("id"=>$workerid);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getservices.php?id=$workerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$serviceresult = json_decode($result);

//close curl connection
curl_close($ch);

?>
<?php
$ch = curl_init();
$data = array("id"=>$workerid);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getworkerservices.php?id=$workerid");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$logworkerservices = curl_exec($ch);
$workerservices = json_decode($logworkerservices);

//close curl connection
curl_close($ch);

?>
<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    
    <div class="logo">
                   <a class="simple-text logo-normal" href="viewworkerdetails.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
      <?php
      $type = $_SESSION["type"];
      if($type=="U")
      {
      include 'usermenu.php';
      }
      else
      {
        include 'workermenu.php';
      }
      ?>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <?php include 'header.php';?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit workercategory and Services</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <form name="f1" method="post" action="curlupdateworkercategory.php">
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Category
                                  </label>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="col-md-5">
                              <div class="form-group">
                                  <div class="category-box-1">
                                      <select class="form-control" id="lstBox1" name="category[]"  multiple="multiple">
                                      <?php
                                       foreach($categoryresult as $newresult)
                                       {
                                          foreach($newresult as $value)
                                          {
                                           ?>
                                                <option value="<?php echo $value->idcategory;?>"><?php echo $value->categoryname;?></option>
                                                <?php
                                            
                                            }
                                        }
                                        
                                        ?>
                                      </select>

                                  </div>
                              </div>
                          </div>
                          <div class="col-md-2">
                              <div class="form-group">
                                  <div class="arrows text-center">
                                      <input class="btn btn-sm btn-info" id="btnright" name = "btnright" type="submit" value=">">
                                      <br>
                                              <input class="btn btn-sm btn-info" id="btnleft" name="btnleft" type="submit"   value="<">
                                                  
                                              </input>
                                          </input>
                                      </input>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-5">
                              <div class="form-group">
                                  <div class="category-box-2">
                                      <select class="form-control" id="lstBox2" name = "multicategory[]" multiple="multiple">
                                      <?php
                                       
                                           foreach($selectedresult as $data)
                                           {
                                           
                                            ?>
                                            <option value="<?php echo $data->idcategory;?>"><?php echo $data->categoryname;?></option>
                                            <?php
                                            }     
                                           
                                        ?>
                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                      Services
                                  </label>
                              </div>
                          </div>
                      </div>


                      <div class="row">
                          <div class="col-md-5">
                              <div class="form-group">
                                  <div class="service-box-1">
                                      <select class="form-control" id="lstBox3" multiple="multiple" name = "services[]">
                                      <?php
                                       foreach($serviceresult as $newresult)
                                       {
                                          foreach($newresult as $value)
                                          {
                                           foreach($selectedresult as $data)
                                           {
                                            if($data->idcategory=="$value->categoryid")
                                            {
                                            ?>
                                            <option value="<?php echo $value->idservices;?>"><?php echo $value->servicename;?></option>
                                            <?php
                                            }     
                                            }
                                            }
                                        }
                                        ?>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-2">
                              <div class="form-group">
                                  <div class="arrows text-center">
                                      <input class="btn btn-sm btn-info" id="btnright1" name = "btnright1" type="submit" value=">">
                                         <br>
                                              <input class="btn btn-sm btn-info" id="btnleft1" type="submit" name = "btnleft1" value="<">
                                                 
                                              </input>
                                          </input>
                                      </input>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-5">
                              <div class="form-group">
                                  <div class="service-box-2">
                                      <select class="form-control" id="lstBox4" multiple="multiple" name="services[]">
                                      <?php
                                       foreach($workerservices as $newresult)
                                       {
                                          foreach($newresult as $value)
                                          {
                                            ?>
                                            <option value="<?php echo $value->serviceid;?>"><?php echo $value->servicename;?></option>
                                            <?php
                                           } 
                                        }
                                        ?>
                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>

                       
                    <input type = "hidden" name="idprofiles" value="<?php echo $workerid;?>">
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
           
            </div>
          </div>
            <footer class="footer">
        <div class="container-fluid">
         
          </nav> 
          <div class="copyright">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
          </div>
        </div>
      </footer>


        </div>
      </div>
      


  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
  <script src = "assets/js/service.js" type="text/javascript"></script>
  <script src = "assets/js/services.js" type="text/javascript"></script>

  
    </div>
  </div>
  <!--   Core JS Files   -->
 
</body>

</html>
<script type="text/javascript">

  
    // $('#lstBox2').on('change',function(e)
    // {
    // //     var categoryid = $('#lstBox2').val();
    // //     var profileid = "<?php// echo $workerid;?>";
    // //     $('#btnleft').on('click',function(e)
    // //         {
    // //         var jsonData = {categoryid:categoryid,profileid:profileid};
    // //         alert(JSON.stringify(jsonData));
    // //         $.ajax({
    // //         type: "POST",
    // //         data: JSON.stringify(jsonData),
    // //         url: "deleteworkercategory.php",
    // //         dataType: "json",
    // //         success : function(data){
    // //             if (data)
    // //             {
    // //                 alert(data);
    // //                 // $('#servicename').children().remove();
    // //                 // var person = JSON.stringify(data['data'][0]);
    // //                 // for(var i=0;i<person.length;i++)
    // //                 // {
    // //                 //     var stddata = JSON.stringify(data['data'][i]);
    // //                 //     var json = JSON.parse(stddata);
    // //                 //     var servicetype = json["servicename"];
    // //                 //     var serviceid = json["idservices"];
    // //                 //     $('#servicename').append($('<option>').text(servicetype).attr('value',serviceid));
                        
    // //                 // }
                    
                    
    // //             }
    // //             else {
    // //                 alert("error");
    // //             }
    // //         }//success
    // //     });//ajax

    // //    });//click
    // //   }); //change
</script>

<?php
}
else
{
   header("location:userlogin.php");
}
?>