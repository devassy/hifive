<?php
session_start();
if($_SESSION["idprofiles"]&&$_SESSION["type"]=='W')
{
    $workerid = $_SESSION["idprofiles"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                   
            </link>
        </link>
    </head>
   
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
               
            <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
                <?php include 'workermenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title ">
                                           View Profile
                                        </h4>
                                        <p class="card-category">
                                            
                                        </p>
                                    </div>
                                    <?php
                                    $username = $_SESSION["username"];
                                    $idprofiles = $_SESSION["idprofiles"];
                                    $type = $_SESSION["type"];
                                    $email = $_SESSION["email"];
                                    $phone = $_SESSION["phone"];
                                    ?>
                                    <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      
                        <tr>
                       
                        <td>
                           
                          </td>
                      </tr>
                      <tr>
                        <th>
                          Username
                        </th>
                        <td>
                          <?php
                        echo $username;
                        ?>
                          </td>
                      </tr>
                     
                        <tr>
                        <th>
                          Email
                        </th>
                        <td>
                        <?php
                       echo $email;
                        ?>
                          </td>
                      </tr>
                     
                       
                     
                      <tr>
                        <th>
                          Phone
                        </th>
                        <td>
                        <?php
                        echo $phone;
                        ?>
                          </td>
                      </tr>
                      
                     
                                         
                       
                       
                          
                          
                          
                         
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          
                          <td class = "text-left">
                         
               <a href= "editworker.php?id=<?php echo $idprofiles;?>"><button type="button" rel="tooltip" class="btn btn-success">
                    <i class="material-icons">edit</i>
                </button></a>
                <button type="button" rel="tooltip" class="btn btn-danger">
                    <i class="material-icons">close</i>
                </button>
                          </td>


                        </tr>
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                                </div>
                            </div>
                        </div>
                        <?php include 'footer.php';?>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="../assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="../assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
                
            </div>
        </div>
    </body>
</html>
<?php
}
else
{
    header("location:userlogin.php");
}
?>
