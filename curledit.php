<?php
session_start();
include 'urlrewrite.php';

if(isset($_POST["submit"]))
{
    $username = $_POST["username"];
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $age = $_POST["age"];
    $email = $_POST["email"];
    $phone = $_POST["phone"];
    $gender = $_POST["inlineRadioOptions"];
    $address1 = $_POST["address1"];
    $address2 = $_POST["address2"];
    $location = $_POST["location"];
    $sublocality = $_POST["sublocality"];
    $landmark = $_POST["landmark"];
    $city = $_POST["city"];
    $district = $_POST["district"];
    $state = $_POST["state"];
    $idprofiles = $_POST["idprofiles"];
    
   
if($_POST["chk"]=="false")
{
   
if($_FILES["userfile"]["name"])
{
    $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
    $filename = $_FILES["userfile"]["name"];
    $tmp_name = $_FILES["userfile"]["tmp_name"];
    $filename = $random.$filename;
    $imagename = './images/'.$filename;
    
if(move_uploaded_file($tmp_name,$imagename))
{
    $newdata = array("username"=>"$username","phone"=>"$phone","created"=>date("Y-m-d"),"address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender","imagename"=>"$filename");

    //Option 1: Convert data array to json if you want to send data as json
    $data = json_encode($newdata);

    //Option 2: else send data as post array.
    //$data = urldecode(http_build_query($data));
    /****** curl code ****/
    //init curl
    $ch = curl_init();
    // URL to be called
    curl_setopt($ch, CURLOPT_URL, "$url/updateworkerwithimage.php");
    //set post TRUE to do a regular HTTP POST
    curl_setopt($ch, CURLOPT_POST, 1);
    //set http headers - if you are sending as json data (i.e. option 1) else comment this 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //send post data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //return as output instead of printing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute curl request
    $result = curl_exec($ch);
    //close curl connection
    curl_close($ch);
    if($result)
    {
        
        $_SESSION["message"] = $result->message;
        header("location:editworker.php?id=$idprofiles");
       
    }
}//moveuploaded file ends
}//file checking ends
else{          //update without image
    $username = $_POST["username"];
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $age = $_POST["age"];
    $email = $_POST["email"];
    $phone = $_POST["phone"];
    $gender = $_POST["inlineRadioOptions"];
    $address1 = $_POST["address1"];
    $address2 = $_POST["address2"];
    $location = $_POST["location"];
    $sublocality = $_POST["sublocality"];
    $landmark = $_POST["landmark"];
    $city = $_POST["city"];
    $district = $_POST["district"];
    $state = $_POST["state"];
    $idprofiles = $_POST["idprofiles"];

    $newdata = array("username"=>"$username","phone"=>"$phone","created"=>date("Y-m-d"),"address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender");

    //Option 1: Convert data array to json if you want to send data as json
    $data = json_encode($newdata);

    //Option 2: else send data as post array.
    //$data = urldecode(http_build_query($data));
    /****** curl code ****/
    //init curl
    $ch = curl_init();
    // URL to be called
    curl_setopt($ch, CURLOPT_URL, "$url/updatedetails.php");
    //set post TRUE to do a regular HTTP POST
    curl_setopt($ch, CURLOPT_POST, 1);
    //set http headers - if you are sending as json data (i.e. option 1) else comment this 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //send post data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //return as output instead of printing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute curl request
    $result = curl_exec($ch);
    $resultmessage = json_decode($result);
    //close curl connection
    curl_close($ch);
    if($result)
    {
        $_SESSION["message"] = $resultmessage->message;
        header("location:editworker.php?id=$idprofiles");
        
    }

}//update with out image ends
}// checking checkbox ends
else if($_POST["oldimage"]) 
{
    $oldimage = $_POST["oldimage"];
    $idimage = $_POST["idimage"];
    $random = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,7);
    $filename = $_FILES["userfile"]["name"];
    $tmp_name = $_FILES["userfile"]["tmp_name"];
    $filename = $random.$filename;
    $imagename = './images/'.$filename;
    
        if(move_uploaded_file($tmp_name,$imagename))
        {
          $imagelocation = 'L';
        $newdata = array("username"=>"$username","phone"=>"$phone","created"=>date("Y-m-d"),"address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender","imagename"=>"$filename","idprofiles"=>"$idprofiles","idimage"=>"$idimage","imagelocation"=>"$imagelocation");
         
        //Option 1: Convert data array to json if you want to send data as json
        $data = json_encode($newdata);

        //Option 2: else send data as post array.
        //$data = urldecode(http_build_query($data));
        /****** curl code ****/
        //init curl
        $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/updateworker.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $result = curl_exec($ch);
       
        //close curl connection
        curl_close($ch);
        if($result)
        {
        $oldimage = "images/".$oldimage;
        unlink($oldimage);
        $_SESSION["message"] = "Successfully Updated";
        header("location:editworker.php?id=$idprofiles");
        }
    }
    
    else  
    {
        $imagelocation = $_POST["imagelocation"];
        $newdata = array("username"=>"$username","phone"=>"$phone","created"=>date("Y-m-d"),"address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender","imagename"=>"$oldimage","idprofiles"=>"$idprofiles","idimage"=>"$idimage","imagelocation"=>"$imagelocation");

        //Option 1: Convert data array to json if you want to send data as json
        $data = json_encode($newdata);

        //Option 2: else send data as post array.
        //$data = urldecode(http_build_query($data));
        /****** curl code ****/
        //init curl
        $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/updateworker.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $newresult = curl_exec($ch);
        $result = json_decode($newresult);
       
        //close curl connection
        curl_close($ch);
        if($result)
        {
            $_SESSION["message"] = "Successfully Updated";
            header("location:editworker.php?id=$idprofiles");
        }

    }
    


}//else if ends
else
{
    $username = $_POST["username"];
    $firstname = $_POST["firstname"];
    $lastname = $_POST["lastname"];
    $age = $_POST["age"];
    $email = $_POST["email"];
    $phone = $_POST["phone"];
    $gender = $_POST["inlineRadioOptions"];
    $address1 = $_POST["address1"];
    $address2 = $_POST["address2"];
    $location = $_POST["location"];
    $sublocality = $_POST["sublocality"];
    $landmark = $_POST["landmark"];
    $city = $_POST["city"];
    $district = $_POST["district"];
    $state = $_POST["state"];
    $idprofiles = $_POST["idprofiles"];

    $newdata = array("username"=>"$username","phone"=>"$phone","created"=>date("Y-m-d"),"address1"=>"$address1","address2"=>"$address2","location"=>"$location","sublocality"=>"$sublocality","landmark"=>"$landmark","city"=>"$city","district"=>"$district","state"=>"$state","firstname"=>"$firstname","lastname"=>"$lastname","age"=>"$age","idprofiles"=>"$idprofiles","email"=>"$email","gender"=>"$gender");

    //Option 1: Convert data array to json if you want to send data as json
    $data = json_encode($newdata);

    //Option 2: else send data as post array.
    //$data = urldecode(http_build_query($data));
    /****** curl code ****/
    //init curl
    $ch = curl_init();
    // URL to be called
    curl_setopt($ch, CURLOPT_URL, "$url/updatedetails.php");
    //set post TRUE to do a regular HTTP POST
    curl_setopt($ch, CURLOPT_POST, 1);
    //set http headers - if you are sending as json data (i.e. option 1) else comment this 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //send post data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //return as output instead of printing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute curl request
    $result = curl_exec($ch);
    //close curl connection
    curl_close($ch);
    if($result)
    {
        $_SESSION["message"] = $result->message;
        header("location:editworker.php?id=$idprofiles");
        
    }

}//else  ends
}//submit ends
?>