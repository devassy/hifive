<?php
include 'urlrewrite.php';
session_start();
if($_SESSION["idprofiles"] && $_SESSION['type']=='W')
{
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <link href="assets/css/service_request.css" rel="stylesheet"/>
            </link>
        </link>
    </head>
    <?php
$id = $_SESSION["idprofiles"];
$data = array("id"=>$id);
//Option 1: Convert data array to json if you want to send data as json
//$data = json_encode($data);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getservicerequest.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);
$newdata = json_decode($result);


//$newdata = json_decode($result);
//close curl connection
curl_close($ch);
//print result
?>
   
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
               
            <div class="logo">
                   <a class="simple-text logo-normal" href="viewworkerdetails.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
               <?php include 'workermenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title ">
                                            Service Requests
                                        </h4>
                                        
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class=" text-primary">
                                                    <th>Usermessage</th>
                                                    <th>Servicelocation</th>
                                                    <th>ServiceDate</th>
                                                    <th>Worker_Status</th> 
                                                    <th>Service Requested On</th>
                                                    <th>Action</th>
                                                </thead>
                                                <tbody>
												<?php
                                                
												if($newdata)
												{
                                                    foreach($newdata as $data)
                                                    {
                            
                                                        ?>
                                                   <tr>
                                                   <td><?php echo $data->usermessage;?></td>
                                                   <td><?php echo $data->service_location;?></td>
                                                   <td><?php $servicedate = date("d-M-Y",strtotime($data->servicedate));echo $servicedate;?></td>
                                                   <td><?php if($data->worker_status=="1"){echo "Active";}else{echo "Not Active";}?></td>
                                                   <td><?php $servicecreated = date("d-M-Y",strtotime($data->createdon));echo $servicecreated;?></td>
													<td>
													 <a href="editservicerequest.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-success" rel="tooltip" type="button">
                                                                    <i class="material-icons">
                                                                        edit
                                                                    </i>
                                                                </button>
                                                            </a></td>
                                                            <!--<td><a href="deleterequest.php?id=<?php echo $data->idservice_request;?>">
                                                                <button class="btn btn-sm btn-danger" rel="tooltip" type="button">
                                                                    <i class="material-icons">
                                                                       close
                                                                    </i>
                                                                </button>
                                                            </a>
                                                    </td>-->
                                                    <td>
                                                   
                                                   <a href="addfirstworkermessage.php?id=<?php echo $data->idservice_request;?>">
                                                       <button class="btn btn-sm btn-success" rel="tooltip" type="button">
                                                           <i class="material-icons">
                                                          +message
                                                           </i>
                                                       </button>
                                                   </a>
                                                   <a href="viewworkerservicemessage.php?id=<?php echo $data->idservice_request;?>">
                                                       <button class="btn btn-sm btn-success" rel="tooltip" type="button">
                                                           <i class="material-icons">
                                                          message
                                                           </i>
                                                       </button>
                                                   </a>
                                           </td>

                                                    
                                                    </tr>
                                                </tbody>
												<?php
                                                
                                                }
												}
												else
												{
													echo "No records Found";
                                                }
												?>
                                            </table>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="footer">
                            <div class="container-fluid">
                                <div class="copyright">
                                    ©
                                    <script>
                                        document.write(new Date().getFullYear())
                                    </script>
                                    <!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
                
            </div>
        </div>
    </body>
</html>
<?php
}
else
{
    header("location:userlogin.php");
}
?>
