<?php
include 'urlrewrite.php';
if(isset($_POST["getworker"]))
{
$category = $_POST["search"];
$location = $_POST["location"];
$search = array("servicename"=>$category,"location"=>$location);

$ch = curl_init();
$data = json_encode($search);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getworkerslist.php");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$searchresult = curl_exec($ch);
$newresult = stripslashes($searchresult);
$workerresult = json_decode($newresult);

//close curl connection
curl_close($ch);
//print result
// print_r($result);

}
?>


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="colorlib.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="s01">
      <div class="nav">
        <img class="logo" src="./hifiveimages/logo1.png" alt="hifivelogo">
        <div class="login">
          <a href="userlogin.php">Login</a>
          <a href="registernewuser.php">Signup</a>
        </div>
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog modal-login">
            <div class="modal-content">
              <form action="#" method="post">
                <div class="modal-header">        
                  <h4 class="modal-title">Login</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">        
                  <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control" required="required">
                  </div>
                  <div class="form-group">
                    <div class="clearfix">
                      <label>Password</label>
                      <a href="#" class="pull-right text-muted"><small>Forgot?</small></a>
                    </div>
                    
                    <input type="password" class="form-control" required="required">
                  </div>
                </div>
                <div class="modal-footer">
                  <label class="checkbox-inline pull-left"><input type="checkbox"> Remember me</label>
                  <input type="submit" class="btn pull-right"  value="Login">
                </div>
              </form>
            </div>
          </div>
        </div>     







      </div>
    
      
      <form name="f1" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
        <fieldset>
          <legend>The Complete Service Marketplace</legend>
        </fieldset>
        <div class="inner-form">
          <div class="input-field first-wrap">
            <input id="search" type="text" name = "search" placeholder="What service are you looking for?" />
          </div>
          <div class="input-field second-wrap">
            <input id="location" type="text" name = "location" placeholder="location" />
          </div>
          <div class="input-field third-wrap">
            <button class="btn-search" type="submit" name="getworker">Search</button>
          </div>
        </div>
      </form>
      <div id="details" style="width:100%;background-color:white;">
                <div class="col" style="float:left;" >
                    <img class="serach_res" height="150px" src="images/worker1.jpg" width="150px">
                    </img>
                </div>
                <div class="col" style="float:left;">
                    <table class="search_details">
                        <tr>
                            <td>
                                Name
                            </td>
                            <td>
                                joey
                            </td>
                        </tr>
                        <tr>
                            <td>
                                location
                            </td>
                            <td>
                                abc
                            </td>
                        </tr>
                        <tr>
                            <td>
                                service
                            </td>
                            <td>
                                IT
                            </td>
                        </tr>
                        <tr>
                            <td>
                                category
                            </td>
                            <td>
                                DM
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                joey@gmail.com
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Registered on :
                            </td>
                            <td>
                                12/12/2018
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input onclick="window.location.href='#'" type="button" value="Profile"/>
                            </td>
                            <td>
                                <input onclick="window.location.href='#'" type="button" value="Remove"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


      
  
    </div>
   
  
  </body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
