<?php 

//headers 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Methods, Authorization, X-Requested-With');

include_once './config/Database.php';
include_once './models/Post.php';
date_default_timezone_set('Asia/Kolkata');
//Instantiate DB  & connect 

$database = new Database();
$db = $database->connect();


// Instatiate blog post object

        $post = new Post($db);
        $data = json_decode(file_get_contents("php://input"));
        $string = "0123456789";
        $post->idimage = str_shuffle($string); 
        $post->idprofile = $data->idprofile;
        $post->imagename = $data->imagename;
        $post->imagelocation = $data->imagelocation;
        if($data->imageurl)
        {
            $post->imageurl = $data->imageurl;
        }
        $post->created = date("Y-m-d h:i:sa");
        $check = $post->checkimage();
        $no = $check->rowCount();
    if($no>0)
    {
        while($row = $check->fetch(PDO::FETCH_ASSOC))
        {
            $post->idimage = $row["idimage"];
            $update = $post->updateimages();
        }
        $updateno = $update->rowCount();
        if($updateno>0)
        {
            $update_message = array();
            $update_message['data']= array();
            $post_update = array("message"=>"Image Updated Successfully");
            array_push($update_message['data'],$post_update);
            echo json_encode($update_message);
            return true;
            
        }
    }
    
    else if($result = $post->addimages())
    {
    if($result)
    {
        $error_arr = array();
        $error_arr['data']= array();
        $post_item = array("message"=>"Image Added Successfully");
        array_push($error_arr['data'],$post_item);
        echo json_encode($error_arr);
        return true;
    }
    else
    {
        $error_arr = array();
        $error_arr['data']= array();
        $post_item = array("message"=>"Failed To Add Image");
        array_push($error_arr['data'],$post_item);
        echo json_encode($error_arr);
        return true;
    }
     
}
else
{
        $error_arr = array();
        $error_arr['data']= array();
        $post_item = array("message"=>"Failed To Add Image");
        array_push($error_arr['data'],$post_item);
        echo json_encode($error_arr);
        return true;
}
?>