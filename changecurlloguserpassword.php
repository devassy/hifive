<?php
include 'urlrewrite.php';
    
       $currentpassword =  $_POST["currentpassword"]; 
       $newpassword = $_POST["newpassword"];
       $retypenewpassword = $_POST["retypenewpassword"];
       $username = $_POST["username"];
       $passworddata = array("username"=>$username,"currentpassword"=>$currentpassword,"newpassword"=>$newpassword,"retypenewpassword"=>$retypenewpassword);
       $data = json_encode($passworddata);
       
       $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/changeloguserpassword.php");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $result = curl_exec($ch);
        $newresult = json_decode($result);
        //close curl connection
        curl_close($ch);
        //print result
        // print_r($result);
        if($newresult)
        {
            $message = $newresult->message;
            header("location:changeloginuserpassword.php?message=$message");
        }
    
    ?>