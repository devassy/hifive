<?php
session_start();
include 'urlrewrite.php';

$type = $_SESSION["type"];
$userid = $_SESSION["idprofiles"];

if(($type == 'U')&&($_SESSION["idprofiles"]))
{
$id = $_GET["id"];
$data = array();
//Option 1: Convert data array to json if you want to send data as json


//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getservicemessages.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$resultmessage = curl_exec($ch);
$result = json_decode($resultmessage);
if(isset($result->resultedmessage))
{
   $resultedmessage = $result->resultedmessage;
}
else
{
foreach($result as $value)
    {
        foreach($value as $data)
        {
            $resultedmessage = $data->resultedmessage;
            $idmessage = $data->idmessage;
        }
    }
}

//close curl connection
curl_close($ch);
//print result
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/getworkerdetails.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$workerdetail = curl_exec($ch);
$workerdata = json_decode($workerdetail);

if(isset($workerdata->resultedmessage))
{
    $resultedworkermessage = $workerdata->resultedmessage;
}
else
{
foreach($workerdata as $workdata)
{
    foreach($workdata as $data)
    {
        $resultedworkermessage = $data->resultedmessage;
    }
}
}
?>





<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                   
            </link>
        </link>
    </head>
   
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
               
            <div class="logo">
                   <a class="simple-text logo-normal" href="welcomeuser.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
                <?php include 'usermenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title ">
                                            Service Request Messages
                                        </h4>
                                        
                                    </div>
                                    <b><span style = "padding-right:20px;"></span>Service Name:
                                    <?php
                                  
                                       if($resultedmessage=="No records")
                                       {
                                            echo "No Records Found";
                                       }
                                       else
                                       {
                                    foreach($result as $value)
                                    {
                                        foreach($value as $data)
                                        {
                                           echo $data->servicename;
                                           break;
                                        }
                                    }
                                }
                                    ?>
                                     
                                    <span style = "padding-right:20px;"></span>
                                    Service Request:
                                    <?php
                                   
                                   if($resultedmessage=="No records")
                                       {
                                            echo "No Records Found";
                                       }
                                       else
                                       {
                                    foreach($result as $value)
                                    {
                                        foreach($value as $data)
                                        {
                                           echo $data->usermessage;
                                           break;
                                        }
                                    }
                                }
                                    ?>
                                    <span style = "padding-right:20px;"></span>
                                    Requested User:
                                    <?php
                                    if($resultedmessage=="No records")
                                    {
                                         echo "No Records Found";
                                    }
                                    else
                                    {
                                    foreach($result as $value)
                                    {
                                        foreach($value as $data)
                                        {
                                           echo $data->firstname;
                                           echo "<span style=padding-right:3px;></span>";
                                           echo $data->lastname;
                                           break;
                                        }
                                    }
                                }
                                
                            
                               
                                    ?>
                                    <span style = "padding-right:20px;"></span>
                                    Service Request Id:<span style = "padding-right:3px;"></span><?php echo $id;?>
                                    <span style = "padding-right:96px;"></span>
                                    Workername:
                                    <?php
                                   if($resultedworkermessage=="No records")
                                       {
                                            echo "Worker Not Assigned Yet";
                                       }
                                       else
                                       {
                                        foreach($workerdata as $value)
                                        {
                                        foreach($value as $data)
                                        {
                                                echo $data->firstname;
                                                echo "<span style=padding-right:3px;></span>";
                                                echo $data->lastname;
                                                break;
                                            
                                        }
                                    }       
                                }
                                    ?>
                                    </b>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class=" text-primary">
                                                    <th>Sl No:</th>
                                                    <th>Subject</th>
                                                    <th>User Message</th>
                                                    <th>Sent On</th>
                                                    <th>Sender</th>
                                                    
                                                </thead>
                                                <tbody>
												<?php
                                               
												if(!empty($result))
												{
                                                    $i=1;
                                                    $j=1;
                                                        if($resultedmessage=="No records")
                                                        {
                                                            echo "No Records Found";
                                                        }
                                                    else
                                                    {
												foreach($result as $value)
												{
													foreach($value as $data)
													{
                                                        $receiverid = $data->recieverid;
                                                        if($data->type=="U" && $data->senderid=="$userid")
                                                        {
												        ?>
                                                   <tr>
                                                   <td bgcolor="#EAF2F8"><?php echo $i;?></td>
                                                   <td bgcolor="#EAF2F8"><?php echo $data->subject;?></td>
                                                   <td bgcolor="#EAF2F8"><?php echo $data->message;?></td>
                                                   <td bgcolor="#EAF2F8"><?php echo $data->created;?></td>
                                                   <td bgcolor="#EAF2F8"><?php echo "sent by you";?></td>
                                                  
                                                    </tr>
                                                </tbody>
												<?php
                                                 $i++;
                                                        }
                                                        else if($data->type="A" && $receiverid =="$userid")
                                                        {
                                                        
                                                        ?>
                                                         <tr>
                                                   <td><?php echo $i;?></td>
                                                   <td><?php echo $data->subject;?></td>
                                                   <td><?php echo $data->message;?></td>
                                                   <td><?php echo $data->created;?></td>
                                                   <td><?php echo "sent by Admin";?></td>
                                                   <td>
                                                   <a href="replyadminmessage.php?id=<?php echo $data->idmessage;?>">
                                                                <button class="btn btn-sm btn-success" rel="tooltip" type="button">
                                                                    <i class="material-icons">
                                                                    Reply
                                                                    </i>
                                                                </button>
                                                            </a>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   </td>
                                                    </tr>
                                                </tbody>
                                                        <?php
                                                        $i++;
                                                        }
                                                        else
                                                        {

                                                        }
                                                    } 
                                                    }

                                                }
                                                
												}
												else
												{
													echo "No records Found";
                                                }
												?>
                                            </table>
                                            <?php
                                            if(isset($_GET["result"]))
                                            {
                                                $success = $_GET["result"];
                                                echo "<span style=color:green;>$success</span>";
                                            }
                                            // if(empty($success))
                                            // {
                                            //     echo "<span style=color:red;></span>";
                                            // }
                                           ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include 'footer.php';?>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
                
            </div>
        </div>
    </body>
</html>
<?php
}
else
{
    header("location:userlogin.php");

}
