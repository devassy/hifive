<?php
session_start();
?>
<!DOCTYPE doctype html>
<html lang="en">
   
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      Hifive
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
</head>
  <?php
 include 'urlrewrite.php';
 date_default_timezone_set("Asia/Calcutta");
 $id = $_GET["id"];
 $newdata = array("id"=>$id);
$ch = curl_init();
$data = json_encode($newdata);
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/read_singleworker.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$workerresult = curl_exec($ch);
$result = json_decode($workerresult);
$i=1;

//close curl connection
curl_close($ch);
//print result
?>
    
    <body style="background: linear-gradient(to bottom left, #54cff8, #23ce8b) !important">
        <div class="">
       
            <div class="content">
                <div class="container-fluid">
                    <!-- your content here -->
                    <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo">

                    </a>
                </div>
               
                    
                    <div class="row" style="margin-top:100px">
                        <div class="col-md-8 offset-2">
                           <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Worker Details</h4>
                  <p class="card-category"> </p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" width="400">
                    <?php
                    foreach($result as $value)
                    {
                    ?>
                        <tr>
                        <td>
                           
                          </td>
                      </tr>
                      <tr>
                        <th>
                          Firstname
                        </th>
                        <td>
                          <?php
                    echo $value[0]->firstname;
                        ?>
                          </td>
                      </tr>
                      <tr>
                        <th>
                        Lastname
                        </th>
                        <td>
                        <?php
                        echo $value[0]->lastname;
                        ?>
                          </td>
                      </tr>
                        <th>
                        Age
                        </th>
                        <td>
                        <?php
                        echo $value[0]->age;
                        ?>
                        </td>
                        <tr>
                        <th>
                       Gender
                        </th>
                        <td>
                        <?php
                        if($value[0]->gender=="M")
                        {
                          echo "Male";
                        }
                        else if($value[0]->gender=="F")
                        {
                          echo "Female";
                        }
                        ?>
                          </td>
                      </tr>
                     
                      <tr>
                        <th>
                        Location
                        </th>
                        <td>
                        <?php
                        echo $value[0]->location;
                        ?>
                          </td>
                      </tr>
                     
                      <tr>
                        <th>
                          City
                        </th>
                        <td>
                        <?php
                        echo $value[0]->city;
                        ?>
                          </td>
                      </tr>
                      <tr>
                        <th>
                        District
                        </th>
                        <td>
                        <?php
                        echo $value[0]->district;
                        ?>
                          </td>
                      </tr>
                      <tr>
                        
                      </tr>
                     
                       
                          <tr>
                          <th>Categories</th>
                         
                          <td>
                        <?php
                        $result_names = '';
                        foreach($value[1] as $data)
                        {
                          $result_names.= $data.',';
                        }
                        echo rtrim($result_names, ',');
                        $i++;
                        echo "</td>";
                        }
                        ?>
                        </tr>
                        <tr>
                        <td>
                                <a href = "userlogin.php?id=<?php echo $id;?>">
                                    <button class="btn-search">
                                        Book <br>Now
                                    </button>
                                   
                                </a></td>
                                <?php
                                if(isset($_SESSION["idprofiles"]))
                                {
                                  $id = $_SESSION["idprofiles"];
                                ?>
                                <td><a href="welcomeuser.php?id=<?php echo $id;?>"><button class="btn-search"  style="width:120px;height:70px">
                                        Back
                                        
                                    </button></a>
                                </td>
                                <?php
                                }
                                else
                                {
                                  $id = "1";
                                  ?>
                                  <td><a href="index.php?id=<?php echo $id;?>"><button class="btn-search" style="width:120px;height:70px">
                                          Back
                                      </button></a>
                                  </td>
                                  <?php
                                }
                                ?>

                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
             
      </div>
                       
                    </div>
                

                   
                </div>
            </div>
            <footer class="footer">
        <div class="container-fluid">
         
          <div class="copyright">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script><!-- , made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web. -->
          </div>
        </div>
      </footer>
            <!--   Core JS Files   -->
            <script src="assets/js/core/jquery.min.js" type="text/javascript">
            </script>
            <script src="assets/js/core/popper.min.js" type="text/javascript">
            </script>
            <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
            </script>
            <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
            </script>
            <!--  Google Maps Plugin    -->
            <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
            </script>
            <!-- Chartist JS -->
            <script src="assets/js/plugins/chartist.min.js">
            </script>
            <!--  Notifications Plugin    -->
            <script src="assets/js/plugins/bootstrap-notify.js">
            </script>
            <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
            <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
            </script>
        </div>
    </body>
</html>
