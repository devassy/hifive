<?php
session_start();
?>
<?php
if($_SESSION["type"]=='W')
{
$id = $_SESSION["idprofiles"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifi
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                    <link href="assets/css/userrewards.css" rel="stylesheet"/>
            </link>
        </link>
    </head>
    <?php
    // if(isset($_GET["page"]))
    // {

    //     include 'urlrewrite.php';
    //     $data = array();
    //     $id = $_GET["page"];
      
    //     //Option 1: Convert data array to json if you want to send data as json
        
        
    //     //Option 2: else send data as post array.
    //     //$data = urldecode(http_build_query($data));
    //     /****** curl code ****/
    //     //init curl
    //     $ch = curl_init();
    //     // URL to be called
    //     curl_setopt($ch, CURLOPT_URL, "$url/readwithpagination.php?id=$id");
    //     //set post TRUE to do a regular HTTP POST
    //     curl_setopt($ch, CURLOPT_POST, 1);
    //     //set http headers - if you are sending as json data (i.e. option 1) else comment this 
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    //     //send post data
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //     //return as output instead of printing it
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     //execute curl request
    //     $newdata = curl_exec($ch);
    //     $result = json_decode($newdata);
       
       
    //     //close curl connection
    //     curl_close($ch);
    // //print result
    // }
    // else
    // {
        include 'urlrewrite.php';
        $data = array();
        //Option 1: Convert data array to json if you want to send data as json


        //Option 2: else send data as post array.
        //$data = urldecode(http_build_query($data));
        /****** curl code ****/
        //init curl
        $ch = curl_init();
        // URL to be called
        curl_setopt($ch, CURLOPT_URL, "$url/getrewardsworker.php?id=$id");
        //set post TRUE to do a regular HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //set http headers - if you are sending as json data (i.e. option 1) else comment this 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //send post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //return as output instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute curl request
        $newdata = curl_exec($ch);
        $result = json_decode($newdata);
        //close curl connection
        curl_close($ch);
//print result
//}
?>
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
              
            <div class="logo">
                   <a class="simple-text logo-normal" href="viewworkerdetails.php">
                             <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
                <?php include 'workermenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-primary">
                                        <h4 class="card-title ">
                                            View Your Rewards
                                        </h4>
                                        
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table" id = "mytable">
                                                <thead class=" text-primary">
                                                    <th>
                                                        ID
                                                    </th>
                                                    <th>Reward Name
                                                    </th>
                                                    
                                                        

                                                    
                                                   <!--  <th><p>Username</p> <i class="fa fa-fw fa-sort"></i></th> -->

                                                   <!--  <th>
                                                        Firstname
                                                    </th>
                                                    <th>
                                                        Lastname
                                                    </th> -->
                                                    <th>
                                                    Description
                                                    </th>
                                                   <!--  <th>
                                                        Password
                                                    </th>
                                                    <th>
                                                        Age
                                                    </th>
                                                    <th>
                                                        Gender
                                                    </th> -->
                                                    <th>
                                                        Points
                                                    </th>
                                                    <th>
                                                    Date
                                                    </th>
                                                    
                                                    <!-- <th>
                                                        Active
                                                    </th>
                                                    <th>
                                                        Created date
                                                    </th> -->
                                                </thead>
                                                <tbody>
                                                <?php
                                                 $id = 1;
                                                 if($result)
                                                 {
                                                foreach($result as $value)
                                                {
                                                    foreach($value as $data)
                                                    {
                                                        
                                                     ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $id;?>
                                                        </td>
                                                        <td>
                                                        <?php echo $data->rewardname;?>
                                                        </td>
                                                       
                                                        <!-- <td>
                                                            Rachel
                                                        </td>
                                                        <td>
                                                            Green
                                                        </td> -->
                                                        <td>
                                                        <?php echo $data->description;?></td>
                                                        <!-- <td>
                                                            ******
                                                        </td>
                                                        <td>
                                                            25
                                                        </td>
                                                        <td>
                                                            F
                                                        </td> -->
                                                        <td>
                                                        <?php echo $data->points;?>
                                                        </td>
                                                        <td>
                                                        <?php echo $data->created;?>
                                                        </td>
                                                       
                                                       
                                                       
                                                        <!-- <td>
                                                            1
                                                        </td>
                                                        <td>
                                                            05-05-2018
                                                        </td> -->
                                                       
                                                    </tr>
                                                    <?php
                                                     $id++;
                                                     }
                                                     }
                                                    }
                                                    ?>
                                                    
                                                </tbody>
                                            </table>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include 'footer.php';?>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="../assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="../assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="../assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
               

                
            </div>
        </div>
    </body>
</html>
<?php
}
else
{
header("location:index.php");
}
?>