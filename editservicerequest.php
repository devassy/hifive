<?php
include 'urlrewrite.php';
session_start();
if(($_SESSION["idprofiles"])&&($_SESSION["type"]=='W'))
{
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link href="assets/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76">
            <link href="assets/img/favicon.png" rel="icon" type="image/png">
                <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
                <title>
                    Hifive
                </title>
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport"/>
                <!--     Fonts and icons     -->
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" rel="stylesheet" type="text/css"/>
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

                    <!-- CSS Files -->
                    <link href="assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
                   
            </link>
        </link>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    </head>
    <?php
$id =$_GET["id"];
$data = array("id" =>'$id');
//Option 1: Convert data array to json if you want to send data as json
//$data = json_encode($data);

//Option 2: else send data as post array.
//$data = urldecode(http_build_query($data));
/****** curl code ****/
//init curl
$ch = curl_init();
// URL to be called
curl_setopt($ch, CURLOPT_URL, "$url/editworkerservicerequest.php?id=$id");
//set post TRUE to do a regular HTTP POST
curl_setopt($ch, CURLOPT_POST, 1);
//set http headers - if you are sending as json data (i.e. option 1) else comment this 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//send post data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//return as output instead of printing it
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//execute curl request
$result = curl_exec($ch);

$newdata = json_decode($result);

foreach($newdata as $value)
{
    foreach($value as $data)
    {
        $firstname = $data->firstname;
        $location = $data->location;
        $phone = $data->phone;
        $address1 = $data->address1;
        $usermessage = $data->usermessage;
        $service_location = $data->service_location;
        $address1 = $data->address1;
        $address2 = $data->address2;
        $servicedate = $data->servicedate;
        $servicetime = $data->servicetime;
        $servicename = $data->servicename;
        $is_email = $data->is_email;
        $payment_status = $data->payment_status;
        $payment_type = $data->payment_type;
        $amount = $data->amount;
        $workerid = $data->workerid;
        $idservice_request = $data->idservice_request;
        $service_status = $data->service_status;
        $worker_status = $data->worker_status;
        
    }
}
//close curl connection
curl_close($ch);
//print result
?>
   
    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-background-color="white" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
               
            <div class="logo">
                   <a class="simple-text logo-normal" href="#">
                               <img class="logo" src="./hifiveimages/hifi.jpg" alt="hifivelogo" style="padding-left: 30px">

                    </a>
                </div>
                <?php include 'workermenu.php';?>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <?php include 'header.php';?>
                <!-- End Navbar -->
                <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Service Request</h4>
                  
                </div>
                           
                <div class="card-body">
                Requested user:<?php echo $firstname;?><br>
                <span style="padding-right;50px;"></span>Requested user Location:   <span style="padding-left;50px;"></span><?php echo $location;?><br>
                <span style="padding-right;50px;"></span>Requested user Phone:  <span style="padding-right;50px;"></span><?php echo $phone;?><br>
                <span style="padding-right;50px;"></span>Requested user Address:    <span style="padding-right;50px;"></span><?php echo $address1;?><br>
                <span style="padding-right;50px;"></span>Requested user Id:    <span style="padding-right;50px;"></span><?php echo $id;?><br>

                  <form method = "post" action = "updatecurlservicerequest.php">
                      <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                                message
                                                            </label>
                                                        <textarea class="form-control" rows = "4" cols = "90" name="usermessage"><?php echo $usermessage;?></textarea> 
                                                        <!-- <input class="form-control" type="text" name="cat_name" id="cat_name">
                                                        </input> -->
                                                    </div>
                                                </div>
                                                
                                            </div>
					   <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Servicelocation
                                  </label>
                                  <input class="form-control" type="text" name="service_location" value = "<?php echo $service_location;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Address1
                                  </label>
                                  <input class="form-control" type="text" name="address1" value = "<?php echo $address1;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Address2
                                  </label>
                                  <input class="form-control" type="text" name="address2" value = "<?php echo $address2;?>">
                                  </input>
                              </div>
                          </div>
                         
                          
                      </div>
					   <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Servicedate
                                  </label>
                                  <input class="form-control" type="date" name="servicedate" value = "<?php echo $servicedate;?>">
                                  </input>
                              </div>
                          </div>
                          
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Service Time</label>
                                     <?php
                                     $servicetime = $servicetime;
                                     $servicetime = rtrim("$servicetime",0);
                                     ?>
                                  <input class="form-control" type= "text" name="servicetime" value = "<?php echo $servicetime;?>">
                                  </input>
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group">
                                  <label class="bmd-label-floating">
                                     Type of Service</label>
                                  <input class="form-control" type= "text" name="servicename" value = "<?php echo $servicename;?>" readonly>
                                  </input>
                              </div>
                          </div>
                          
                      </div>
                      
                                            <div class="row">
                                            <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                        Worker Email Status
                                                        <?php
                                                        $email = $is_email;
                                                        ?>
                                                      </label>
                                                      <?php
                                                      if($email=="1")
                                                      {
                                                    ?>
                                                      <select  name="is_email" id="is_email" class="browser-default custom-select" readonly>
                                                      <option <?php if ($email == "1"){echo 'selected';}?> value ="1">sent</option>
                                                      
                                                      </select>

                                                      <?php
                                                      }
                                                      else
                                                      {
                                                      ?>
                                                      <select  name="is_email" id="is_email" class="browser-default custom-select" readonly>
                                                      <option <?php if ($email == "0"){echo 'selected';}?> value ="1">sent</option>
                                                      
                                                      </select>

                                                    <?php
                                                      }
                                                      ?>
                                                    </div>
                                                </div>
                                           
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                           Service Status
                                                           </label>
                                                        <?php
                                                        $service = $service_status;
                            
                                                        ?>
                                                      
                                                      <select name="service_status" id="service_status" class="browser-default custom-select">
                                                      <option <?php if ($service == "N"){echo 'selected';}?> value ="N">Not Started</option>
                                                      <option <?php if ($service == "S"){echo 'selected';}?> value ="S">Started</option>
                                                      <option <?php if ($service == "C"){echo 'selected';}?> value ="C">Completed</option>
                                                      <option <?php if ($service == "Ca"){echo 'selected';}?> value ="Ca">Cancelled</option>
                                                      <option <?php if ($service == "P"){echo 'selected';}?> value ="P">Progress</option>
                                                      </select>
                                                    </div>
                                                </div>
                                               
                          <div class="col-md-4">
                                                    <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                           Worker Status
                                                        <?php
                                                        $value = $worker_status;
                                                        ?>
                                                      </label>
                                                      <?php
                                                      if($value=="1")
                                                      {
                                                      ?>
                                                       <select  name="worker_status" id="worker_status" class="browser-default custom-select" >
                                                      <option <?php if ($value == "1"){echo 'selected';}?> value ="1" selected="selected">Assigned</option>
                                                     
                                                      </select>
                                                      <?php
                                                      }
                                                      else if($value=="0")
                                                      {
                                                        ?>
                                                        <select  name="worker_status" id="worker_status" class="browser-default custom-select" >
                                                       <option <?php if ($value == "0"){echo 'selected';}?> value ="1" selected="selected">Assigned</option>
                                                      
                                                       </select>
                                                       <?php
                                                          
                                                      }
                                                      else
                                                      {

                                                      }
                                                      ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                            <div class="col-md-4">
                                            <div class="form-group">
                                                    
                                                        <!-- <label class="bmd-label-floating">
                                                            Service 
                                                        </label> -->
                                                        <label class="bmd-label-floating">
                                                           Payment Status
                                                        <?php
                                                        $value = $payment_status;
                                                        ?>
                                                      </label>
                                                      <select name="payment_status" id="payment_status" class="browser-default custom-select">
                                                      <option <?php if ($value == "1"){echo 'selected';}?> value ="1">Paid</option>
                                                      <option <?php if ($value == "0"){echo 'selected';}?> value ="0">Not Paid</option>
                                                      </select>
                                            </div>
                                            
                                            </div>
                                            <div class="col-md-4">
                                                        <div class="form-group">
                                                        <label class="bmd-label-floating">
                                                           Payment Type
                                                      
                                                      </label>
                                                      <select name="payment_type" id="payment_status" class="browser-default custom-select">
                                                      <option <?php if ($payment_type == "C"){echo 'selected';}?> value ="C">Cash</option>
                                                      <option <?php if ($payment_type == "O"){echo 'selected';}?> value ="O">Online</option>
                                                      </select>
                                                        </div>
                                                    </div>
                                            
                                                
                                               
                                            </div>
                                            <div class="row">
                                            <div class="col-md-4">
                                            <div class="form-group">
                                                    <label class="bmd-label-floating">
                                                        Service Amount</label>
                                                    <input class="form-control" type= "text" name="amount" value = "<?php echo $amount;?>">
                                                    </input>
                                                </div>
                                            </div>
                                            </div>
                                            
                    <input type="hidden" name ="userid" value= "<?php if(isset($id)){echo $id;}?>">
                    <input type="hidden" name ="workerid" value= "<?php if(isset($workerid)){echo $workerid;}?>">
                    <input type="hidden" name ="idservice_request" value= "<?php echo $idservice_request;?>">
                    <button type="submit" class="btn btn-primary pull-right">Update Service Request</button>
                    <div class="clearfix"></div>
                  </form>
                  <?php
                   if(isset($_SESSION["result"]))
                   {
                      $resultmessage = $_SESSION["result"];
                      if($resultmessage=="No data to display")
                      {
                     ?>
                     <div style="color:red;" id = "testdiv"><?php echo $resultmessage;?></div>
                     <script type="text/javascript">
                        $(function()
                        {
                                $("#testdiv").delay(7000).fadeOut();
                        });
                    </script>
                     <?php
                     unset($resultmessage);
                     unset($_SESSION["result"]);
                      }
                     
                      else
                      {
                        ?>
                        <div style="color:green;" id = "testdiv"><?php echo $resultmessage;?></div>
                        <script type="text/javascript">
                           $(function()
                           {
                                   $("#testdiv").delay(7000).fadeOut();
                           });
                       </script>
                        <?php
                       
                        unset($resultmessage);
                        unset($_SESSION["result"]);
                       
                      }
                      

                   }
                
                  ?>
                </div>
              </div>
            </div>
          
            </div> 
          </div>
                        <?php include 'footer.php';?>
                    </div>
                </div>
                <!--   Core JS Files   -->
                <script src="assets/js/core/jquery.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/popper.min.js" type="text/javascript">
                </script>
                <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript">
                </script>
                <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js">
                </script>
                <!--  Google Maps Plugin    -->
                <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE">
                </script>
                <!-- Chartist JS -->
                <script src="assets/js/plugins/chartist.min.js">
                </script>
                <!--  Notifications Plugin    -->
                <script src="assets/js/plugins/bootstrap-notify.js">
                </script>
                <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
                <script src="assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript">
                </script>
                
            </div>
        </div>
    </body>
</html>
<?php
}
else
{
    header("location:userlogin.php");
}
?>
